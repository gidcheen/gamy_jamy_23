#include "window.hpp"

#include <stdexcept>

#define GLFW_INCLUDE_NONE
#include <glfw/glfw3.h>

namespace windowing
{
	static GLFWwindow *rootWindow;

	static void Windowposfun(GLFWwindow *window, int xpos, int ypos);
	static void Windowsizefun(GLFWwindow *window, int width, int height);
	static void Windowfocusfun(GLFWwindow *window, int focused);
	static void Windowrefreshfun(GLFWwindow *window);
	static void Windowclosefun(GLFWwindow *window);
	static void Windowcontentscalefun(GLFWwindow *window, float xscale, float yscale);
	static void Windowiconifyfun(GLFWwindow *window, int iconified);
	static void Windowmaximizefun(GLFWwindow *window, int maximized);
	static void Cursorposfun(GLFWwindow *window, double xpos, double ypos);
	static void Cursorenterfun(GLFWwindow *window, int entered);
	static void Mousebuttonfun(GLFWwindow *window, int button, int action, int mods);
	static void Scrollfun(GLFWwindow *window, double xoffset, double yoffset);
	static void Keyfun(GLFWwindow *window, int key, int scancode, int action, int mods);
	static void Charmodsfun(GLFWwindow *window, unsigned int codepoint, int mods);
	static void Dropfun(GLFWwindow *window, int path_count, const char *p_paths[]);

	Window::Window(std::string const &title, int width, int height)
	{
		GLFWwindow *window = glfwCreateWindow(width, height, title.c_str(), nullptr, rootWindow);
		glfwSetWindowUserPointer(window, events.get());

		if (rootWindow == nullptr)
		{
			rootWindow = window;
		}

		impl = (WindowImpl *)window;

		glfwSetWindowPosCallback(window, Windowposfun);
		glfwSetWindowSizeCallback(window, Windowsizefun);
		glfwSetWindowFocusCallback(window, Windowfocusfun);
		glfwSetWindowRefreshCallback(window, Windowrefreshfun);
		glfwSetWindowCloseCallback(window, Windowclosefun);
		glfwSetWindowContentScaleCallback(window, Windowcontentscalefun);
		glfwSetWindowIconifyCallback(window, Windowiconifyfun);
		glfwSetWindowMaximizeCallback(window, Windowmaximizefun);
		glfwSetCursorPosCallback(window, Cursorposfun);
		glfwSetCursorEnterCallback(window, Cursorenterfun);
		glfwSetMouseButtonCallback(window, Mousebuttonfun);
		glfwSetScrollCallback(window, Scrollfun);
		glfwSetKeyCallback(window, Keyfun);
		glfwSetCharModsCallback(window, Charmodsfun);
		glfwSetDropCallback(window, Dropfun);

		glfwMakeContextCurrent(window);
		glfwSwapInterval(0);
		glfwMaximizeWindow(window);
	}

	Window::~Window()
	{
		GLFWwindow *window = (GLFWwindow *)impl;
		glfwDestroyWindow(window);
	}

	void Window::MakeCurrent() const
	{
		GLFWwindow *window = (GLFWwindow *)impl;
		glfwMakeContextCurrent(window);
	}

	bool Window::ShouldClose() const
	{
		GLFWwindow *window = (GLFWwindow *)impl;
		return glfwWindowShouldClose(window);
	}

	void Window::Close()
	{
		GLFWwindow *window = (GLFWwindow *)impl;
		glfwSetWindowShouldClose(window, true);
	}

	void Window::HandleEvents(std::function<void(InputEvent const &)> callback) const
	{
		for (InputEvent &event : *events)
		{
			callback(event);
		}
		events->clear();
	}

	void Window::SwapBuffers() const
	{
		GLFWwindow *window = (GLFWwindow *)impl;
		glfwSwapBuffers(window);
	}

	Window::Loader Window::GetLoader() const
	{
		return &glfwGetProcAddress;
	}

	static std::vector<InputEvent> &GetEvents(GLFWwindow *window)
	{
		std::vector<InputEvent> *events = (std::vector<InputEvent> *)glfwGetWindowUserPointer(window);
		return *events;
	}

	static InputAction ToInputAction(int input);
	static InputModifier ToInputModifier(int input);
	static MouseButton ToMouseButton(int input);
	static KeyboardKey ToKeyboardKey(int input);

	static void Windowposfun(GLFWwindow *window, int xpos, int ypos)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Windowpos,
			.windowpos = {
				.xpos = xpos,
				.ypos = ypos,
			}});
	}

	static void Windowsizefun(GLFWwindow *window, int width, int height)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Windowsize,
			.windowsize = {
				.width = width,
				.height = height,
			}});
	}

	static void Windowfocusfun(GLFWwindow *window, int focused)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Windowfocus,
			.windowfocus = {
				.focused = (bool)focused,
			}});
	}

	static void Windowrefreshfun(GLFWwindow *window)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Windowrefresh,
			.windowrefresh = {}});
	}

	static void Windowclosefun(GLFWwindow *window)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Windowclose,
			.windowclose = {}});
	}

	static void Windowcontentscalefun(GLFWwindow *window, float xscale, float yscale)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Windowcontentscale,
			.windowcontentscale = {
				.xscale = xscale,
				.yscale = yscale,
			}});
	}

	static void Windowiconifyfun(GLFWwindow *window, int iconified)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Windowiconify,
			.windowiconify = {
				.iconified = (bool)iconified,
			}});
	}

	static void Windowmaximizefun(GLFWwindow *window, int maximized)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Windowmaximize,
			.windowmaximize = {
				.maximized = (bool)maximized,
			}});
	}
	static void Cursorposfun(GLFWwindow *window, double xpos, double ypos)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Cursorpos,
			.cursorpos = {
				.xpos = xpos,
				.ypos = ypos,
			}});
	}

	static void Cursorenterfun(GLFWwindow *window, int entered)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Cursorenter,
			.cursorenter = {
				.entered = (bool)entered,
			}});
	}

	static void Mousebuttonfun(GLFWwindow *window, int button, int action, int mods)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Mousebutton,
			.mousebutton = {
				.button = ToMouseButton(button),
				.action = ToInputAction(action),
				.mods = ToInputModifier(mods),
			}});
	}

	static void Scrollfun(GLFWwindow *window, double xoffset, double yoffset)
	{
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Scroll,
			.scroll = {
				.xoffset = xoffset,
				.yoffset = yoffset,
			}});
	}

	static void Keyfun(GLFWwindow *window, int key, int scancode, int action, int mods)
	{
		if (key < 0)
		{
			return;
		}
		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Key,
			.key = {
				.key = ToKeyboardKey(key),
				.action = ToInputAction(action),
				.mods = ToInputModifier(mods),
			}});
	}

	static void Charmodsfun(GLFWwindow *window, unsigned int codepoint, int mods)
	{
		unsigned int codepoints[]{codepoint, 0};
		std::string text;
		text += (char *)codepoints;

		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Text,
			.text = {
				.text = text,
			}});
	}

	static void Dropfun(GLFWwindow *window, int path_count, const char *p_paths[])
	{
		std::vector<std::string> paths;
		for (int i = 0; i < path_count; i++)
		{
			paths.push_back(std::string(p_paths[i]));
		}

		GetEvents(window).push_back(InputEvent{
			.type = InputEventType::Drop,
			.drop = {
				.paths = paths,
			}});
	}

	static InputAction ToInputAction(int action)
	{
		switch (action)
		{
		case GLFW_PRESS:
			return InputAction::Down;
		case GLFW_RELEASE:
			return InputAction::Up;
		case GLFW_REPEAT:
			return InputAction::Repeat;
		}
		throw std::runtime_error("");
	}

	static InputModifier ToInputModifier(int mods)
	{
		return (InputModifier)mods;
	}

	static MouseButton ToMouseButton(int button)
	{
		switch (button)
		{
		case GLFW_MOUSE_BUTTON_LEFT:
			return MouseButton::Left;
		case GLFW_MOUSE_BUTTON_RIGHT:
			return MouseButton::Right;
		case GLFW_MOUSE_BUTTON_MIDDLE:
			return MouseButton::Middle;

		case GLFW_MOUSE_BUTTON_4:
			return MouseButton::Button4;
		case GLFW_MOUSE_BUTTON_5:
			return MouseButton::Button5;
		case GLFW_MOUSE_BUTTON_6:
			return MouseButton::Button6;
		case GLFW_MOUSE_BUTTON_7:
			return MouseButton::Button7;
		case GLFW_MOUSE_BUTTON_8:
			return MouseButton::Button8;
		}
		throw std::runtime_error("");
	}

	static KeyboardKey ToKeyboardKey(int key)
	{
		switch (key)
		{
		case GLFW_KEY_1:
			return KeyboardKey::Key1;
		case GLFW_KEY_2:
			return KeyboardKey::Key2;
		case GLFW_KEY_3:
			return KeyboardKey::Key3;
		case GLFW_KEY_4:
			return KeyboardKey::Key4;
		case GLFW_KEY_5:
			return KeyboardKey::Key5;
		case GLFW_KEY_6:
			return KeyboardKey::Key6;
		case GLFW_KEY_7:
			return KeyboardKey::Key7;
		case GLFW_KEY_8:
			return KeyboardKey::Key8;
		case GLFW_KEY_9:
			return KeyboardKey::Key9;
		case GLFW_KEY_0:
			return KeyboardKey::Key0;

		case GLFW_KEY_A:
			return KeyboardKey::KeyA;
		case GLFW_KEY_B:
			return KeyboardKey::KeyB;
		case GLFW_KEY_C:
			return KeyboardKey::KeyC;
		case GLFW_KEY_D:
			return KeyboardKey::KeyD;
		case GLFW_KEY_E:
			return KeyboardKey::KeyE;
		case GLFW_KEY_F:
			return KeyboardKey::KeyF;
		case GLFW_KEY_G:
			return KeyboardKey::KeyG;
		case GLFW_KEY_H:
			return KeyboardKey::KeyH;
		case GLFW_KEY_I:
			return KeyboardKey::KeyI;
		case GLFW_KEY_J:
			return KeyboardKey::KeyJ;
		case GLFW_KEY_K:
			return KeyboardKey::KeyK;
		case GLFW_KEY_L:
			return KeyboardKey::KeyL;
		case GLFW_KEY_M:
			return KeyboardKey::KeyM;
		case GLFW_KEY_N:
			return KeyboardKey::KeyN;
		case GLFW_KEY_O:
			return KeyboardKey::KeyO;
		case GLFW_KEY_P:
			return KeyboardKey::KeyP;
		case GLFW_KEY_Q:
			return KeyboardKey::KeyQ;
		case GLFW_KEY_R:
			return KeyboardKey::KeyR;
		case GLFW_KEY_S:
			return KeyboardKey::KeyS;
		case GLFW_KEY_T:
			return KeyboardKey::KeyT;
		case GLFW_KEY_U:
			return KeyboardKey::KeyU;
		case GLFW_KEY_V:
			return KeyboardKey::KeyV;
		case GLFW_KEY_W:
			return KeyboardKey::KeyW;
		case GLFW_KEY_X:
			return KeyboardKey::KeyX;
		case GLFW_KEY_Y:
			return KeyboardKey::KeyY;
		case GLFW_KEY_Z:
			return KeyboardKey::KeyZ;

		case GLFW_KEY_SPACE:
			return KeyboardKey::Space;
		case GLFW_KEY_GRAVE_ACCENT:
			return KeyboardKey::GraveAccent;
		case GLFW_KEY_MINUS:
			return KeyboardKey::Minus;
		case GLFW_KEY_EQUAL:
			return KeyboardKey::Equal;
		case GLFW_KEY_LEFT_BRACKET:
			return KeyboardKey::LeftBracket;
		case GLFW_KEY_RIGHT_BRACKET:
			return KeyboardKey::RightBracket;
		case GLFW_KEY_SEMICOLON:
			return KeyboardKey::Semicolon;
		case GLFW_KEY_APOSTROPHE:
			return KeyboardKey::Apostrophe;
		case GLFW_KEY_BACKSLASH:
			return KeyboardKey::BackSlash;
		case GLFW_KEY_COMMA:
			return KeyboardKey::Comma;
		case GLFW_KEY_PERIOD:
			return KeyboardKey::Period;
		case GLFW_KEY_SLASH:
			return KeyboardKey::Slash;

		case GLFW_KEY_LEFT_SHIFT:
			return KeyboardKey::LeftShift;
		case GLFW_KEY_LEFT_CONTROL:
			return KeyboardKey::LeftControl;
		case GLFW_KEY_LEFT_ALT:
			return KeyboardKey::LeftAlt;
		case GLFW_KEY_LEFT_SUPER:
			return KeyboardKey::LeftSuper;

		case GLFW_KEY_RIGHT_SHIFT:
			return KeyboardKey::RightShift;
		case GLFW_KEY_RIGHT_CONTROL:
			return KeyboardKey::RightControl;
		case GLFW_KEY_RIGHT_ALT:
			return KeyboardKey::RightAlt;
		case GLFW_KEY_RIGHT_SUPER:
			return KeyboardKey::RightSuper;

		case GLFW_KEY_PRINT_SCREEN:
			return KeyboardKey::Print;
		case GLFW_KEY_SCROLL_LOCK:
			return KeyboardKey::ScrollLock;
		case GLFW_KEY_PAUSE:
			return KeyboardKey::Pause;

		case GLFW_KEY_TAB:
			return KeyboardKey::Tab;
		case GLFW_KEY_CAPS_LOCK:
			return KeyboardKey::CapsLock;
		case GLFW_KEY_BACKSPACE:
			return KeyboardKey::BackSpace;
		case GLFW_KEY_ENTER:
			return KeyboardKey::Enter;

		case GLFW_KEY_ESCAPE:
			return KeyboardKey::Escape;

		case GLFW_KEY_F1:
			return KeyboardKey::F1;
		case GLFW_KEY_F2:
			return KeyboardKey::F2;
		case GLFW_KEY_F3:
			return KeyboardKey::F3;
		case GLFW_KEY_F4:
			return KeyboardKey::F4;
		case GLFW_KEY_F5:
			return KeyboardKey::F5;
		case GLFW_KEY_F6:
			return KeyboardKey::F6;
		case GLFW_KEY_F7:
			return KeyboardKey::F7;
		case GLFW_KEY_F8:
			return KeyboardKey::F8;
		case GLFW_KEY_F9:
			return KeyboardKey::F9;
		case GLFW_KEY_F10:
			return KeyboardKey::F10;
		case GLFW_KEY_F11:
			return KeyboardKey::F11;
		case GLFW_KEY_F12:
			return KeyboardKey::F12;

		case GLFW_KEY_INSERT:
			return KeyboardKey::Insert;
		case GLFW_KEY_DELETE:
			return KeyboardKey::Delete;
		case GLFW_KEY_HOME:
			return KeyboardKey::Home;
		case GLFW_KEY_END:
			return KeyboardKey::End;
		case GLFW_KEY_PAGE_UP:
			return KeyboardKey::PageUp;
		case GLFW_KEY_PAGE_DOWN:
			return KeyboardKey::PageDown;

		case GLFW_KEY_LEFT:
			return KeyboardKey::Left;
		case GLFW_KEY_RIGHT:
			return KeyboardKey::Right;
		case GLFW_KEY_UP:
			return KeyboardKey::Up;
		case GLFW_KEY_DOWN:
			return KeyboardKey::Down;

		case GLFW_KEY_NUM_LOCK:
			return KeyboardKey::NumLock;
		case GLFW_KEY_KP_0:
			return KeyboardKey::Num0;
		case GLFW_KEY_KP_1:
			return KeyboardKey::Num1;
		case GLFW_KEY_KP_2:
			return KeyboardKey::Num2;
		case GLFW_KEY_KP_3:
			return KeyboardKey::Num3;
		case GLFW_KEY_KP_4:
			return KeyboardKey::Num4;
		case GLFW_KEY_KP_5:
			return KeyboardKey::Num5;
		case GLFW_KEY_KP_6:
			return KeyboardKey::Num6;
		case GLFW_KEY_KP_7:
			return KeyboardKey::Num7;
		case GLFW_KEY_KP_8:
			return KeyboardKey::Num8;
		case GLFW_KEY_KP_9:
			return KeyboardKey::Num9;
		case GLFW_KEY_KP_DIVIDE:
			return KeyboardKey::NumDivide;
		case GLFW_KEY_KP_MULTIPLY:
			return KeyboardKey::NumMultiply;
		case GLFW_KEY_KP_SUBTRACT:
			return KeyboardKey::NumSubtract;
		case GLFW_KEY_KP_ADD:
			return KeyboardKey::NumAdd;
		case GLFW_KEY_KP_DECIMAL:
			return KeyboardKey::NumDecimal;
		case GLFW_KEY_KP_ENTER:
			return KeyboardKey::NumEnter;
		}
		throw std::runtime_error("");
	}
}
