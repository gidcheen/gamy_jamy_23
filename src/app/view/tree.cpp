#include "tree.hpp"

#include "shared_assets.hpp"

namespace view
{
	void RenderBranch(graphics::Renderer &renderer, state::Branch &branch, bool isRoot)
	{
		graphics::MeshSubmit submit{
			.mesh = &SharedAssets::Get().quadMesh,
			.shader = &SharedAssets::Get().defaultShader,
			.material = isRoot ? &SharedAssets::Get().treeMaterial : &SharedAssets::Get().branchMaterial,
			.sprite = nullptr,
		};
		submit.depth = 0.1;
		submit.transform = branch.transform.transform * algebra::Matrix3x3::Scale({0.08, branch.size});
		renderer.Submit(submit);
	}

	TreeView::TreeView(std::shared_ptr<state::Tree> tree)
		: tree(tree)
	{
	}

	void TreeView::Render(graphics::Renderer &renderer)
	{
		isRoot = tree->is_root;

		for (auto &branch : tree->GrowingBranches())
		{
			RenderBranch(renderer, *branch, tree->is_root);
		}
		for (auto &branch : tree->FullGrownBranches())
		{
			RenderBranch(renderer, *branch, tree->is_root);
		}
	}
}
