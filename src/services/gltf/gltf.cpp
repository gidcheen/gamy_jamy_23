#include "gltf.hpp"

#include <json/json.hpp>

#include <fstream>
#include <cassert>

namespace gltf
{
	std::vector<GltfMesh> LoadGltfMesh(std::filesystem::path path)
	{
		assert(path.extension() == ".gltf");
		std::ifstream file(path, std:: ios::binary);
		std::string gltf_string((std::istreambuf_iterator<char>(file)),std:: istreambuf_iterator<char>());
		auto gltf = json::JsonToObject(gltf_string).value();

		auto *gltf_buffers = gltf.GetObjectValue("buffers").value();
		auto *gltf_meshes = gltf.GetObjectValue("meshes").value();
		auto *gltf_buffer_views = gltf.GetObjectValue("bufferViews").value();
		auto *gltf_accessors = gltf.GetObjectValue("accessors").value();

		// bin loading
		std::vector<std::vector<uint8_t>> buffers;
		auto buffer_count = gltf_buffers->GetArrayCount().value();
		for (size_t buffer_index = 0; buffer_index < buffer_count; buffer_index++)
		{
			auto *gltf_buffer = gltf_buffers->GetArrayValue(buffer_index).value();
			auto uri = gltf_buffer->GetObjectValue("uri").value()->AsString().value();
			auto dir = path.parent_path();
			auto buffer_path = dir.append(uri);
			std::ifstream buffer_file(buffer_path, std::ios::binary);
			buffers.emplace_back((std::istreambuf_iterator<char>(buffer_file)), std::istreambuf_iterator<char>());
		}

		// mesh loading
		std::vector<GltfMesh> meshes;
		size_t mesh_count = gltf_meshes->GetArrayCount().value();
		for (size_t mesh_index = 0; mesh_index < mesh_count; mesh_index++)
		{
			auto *gltf_mesh = gltf_meshes->GetArrayValue(mesh_index).value();
			auto *gltf_primitives = gltf_mesh->GetObjectValue("primitives").value();
			auto primitives_count = gltf_primitives->GetArrayCount().value();
			for (size_t primitive_index = 0; primitive_index < primitives_count; primitive_index++)
			{
				auto *gltf_primitive = gltf_primitives->GetArrayValue(primitive_index).value();

				// index

				auto indices_index = gltf_primitive->GetObjectValue("indices").value()->AsInt().value();

				auto *gltf_indices_accessor = gltf_accessors->GetArrayValue(indices_index).value();
				auto *gltf_indices_buffer_view =
					gltf_buffer_views->GetArrayValue(gltf_indices_accessor->GetObjectValue("bufferView").value()->AsInt().value()).value();
				auto index_count = gltf_indices_accessor->GetObjectValue("count").value()->AsInt().value();
				auto index_buffer_index = gltf_indices_buffer_view->GetObjectValue("buffer").value()->AsInt().value();
				auto index_offset = gltf_indices_buffer_view->GetObjectValue("byteOffset").value()->AsInt().value();
				auto index_size = gltf_indices_buffer_view->GetObjectValue("byteLength").value()->AsInt().value();

				std::vector<int32_t> indices;
				indices.reserve(index_count);
				if (index_size / index_count == 2)
				{
					auto index_data = (uint16_t *)&buffers[index_buffer_index][index_offset];
					for (size_t i = 0; i < index_count; i++)
						indices.emplace_back(index_data[i]);
				}
				else if (index_size / index_count == 4)
				{
					auto index_data = (uint32_t *)&buffers[index_buffer_index][index_offset];
					for (size_t i = 0; i < index_count; i++)
						indices.emplace_back(index_data[i]);
				}

				// vertex
				auto *gltf_attributes = gltf_primitive->GetObjectValue("attributes").value();
				auto position_index = gltf_attributes->GetObjectValue("POSITION").value()->AsInt().value();

				// position
				auto *gltf_position_accessor = gltf_accessors->GetArrayValue(position_index).value();
				auto *gltf_position_buffer_view =
					gltf_buffer_views->GetArrayValue(gltf_position_accessor->GetObjectValue("bufferView").value()->AsInt().value()).value();
				auto position_count = gltf_position_accessor->GetObjectValue("count").value()->AsInt().value();
				auto position_buffer_index = gltf_position_buffer_view->GetObjectValue("buffer").value()->AsInt().value();
				auto position_offset = gltf_position_buffer_view->GetObjectValue("byteOffset").value()->AsInt().value();
				auto position_data = (algebra::Vector3 *)&buffers[position_buffer_index][position_offset];

				std::vector<algebra::Vector3> vertices;
				vertices.reserve(position_count);
				for (size_t i = 0; i < position_count; i++)
				{
					vertices.emplace_back(position_data[i]);
				}

				GltfMesh mesh;
				for (int index : indices)
				{
					mesh.vertices.emplace_back(vertices[index].x, vertices[index].z);
				}
				meshes.push_back(mesh);
			}
		}

		return meshes;
	}
}
