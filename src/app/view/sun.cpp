#include "sun.hpp"

#include <gltf/gltf.hpp>
#include "shared_assets.hpp"

namespace view
{
	SunView::SunView(std::shared_ptr<state::Sun> sun)
		: sun(sun)
	{
		std::vector<gltf::GltfMesh> gltfMesh = gltf::LoadGltfMesh("meshes/sun.gltf");
		mesh = std::make_unique<graphics::Mesh>(gltfMesh[0].vertices);
	}

	void SunView::Render(graphics::Renderer &renderer)
	{
		graphics::MeshSubmit submit{
			.mesh = mesh.get(),
			.shader = &SharedAssets::Get().defaultShader,
			.material = &SharedAssets::Get().sunMaterial,
			.sprite = nullptr,
		};
		submit.transform = sun->GetTransform();
		renderer.Submit(submit);
	}
}
