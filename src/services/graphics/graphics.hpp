#pragma once

namespace graphics
{
	class Graphics final
	{
	public:
		using LoaderFn = void (*)();
		using Loader = LoaderFn (*)(char const *);

	private:
		Graphics(Loader loader);

	public:
		static void Init(Loader loader);
	};
}
