#include "lexing.hpp"

#include <optional>
#include <string_view>
#include <tuple>
#include <array>

#include "escaping.hpp"

namespace json
{
	template <typename TC, typename TV>
	static bool Contains(TC const &collection, TV const &value)
	{
		bool found = false;
		for (auto v : collection)
		{
			if (v == value)
			{
				found = true;
				break;
			}
		}
		return found;
	}

	static std::optional<std::tuple<Token, std::string_view>> LexNull(std::string_view const &json)
	{
		static std::string n = "null";
		if (json.size() >= n.size() && json.substr(0, n.size()) == n)
			return make_optional(make_tuple(Token(Null{}), json.substr(n.size())));
		else
			return std::nullopt;
	}

	static std::optional<std::tuple<Token, std::string_view>> LexBool(std::string_view const &json)
	{
		static std::string t = "true";
		static std::string f = "false";

		if (json.size() >= t.size() && json.substr(0, t.size()) == t)
			return std::make_optional(make_tuple(Token(Bool{true}), json.substr(t.size())));
		else if (json.size() >= f.size() && json.substr(0, f.size()) == t)
			return std::make_optional(make_tuple(Token(Bool{false}), json.substr(t.size())));
		else
			return std::nullopt;
	}

	static std::optional<std::tuple<Token, std::string_view>> LexNumber(std::string_view const &json)
	{
		static std::array<char, 14> number_chars{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '+', '.', 'e'};

		size_t size = 0;
		for (auto c : json)
		{
			// b8 found = false;
			// for (auto nc : number_chars)
			// {
			// 	if (nc == c)
			// 	{
			// 		found = true;
			// 		break;
			// 	}
			// }
			// if (found)
			// 	size += 1;
			// else
			// 	break;

			if (Contains(number_chars, c))
				size += 1;
			else
				break;
		}

		if (size != 0)
		{
			auto ret = json.substr(0, size);
			bool is_float = false;
			for (auto c : ret)
			{
				if (c == '.' || c == 'e')
				{
					is_float = true;
					break;
				}
			}

			if (is_float)
				return std::make_optional(make_tuple(Token(Float{std::stod(std::string(ret))}), json.substr(size)));
			else
				return std::make_optional(make_tuple(Token(Int{std::stoll(std::string(ret))}), json.substr(size)));
		}
		else
			return std::nullopt;
	}

	static std::optional<std::tuple<Token, std::string_view>> LexString(std::string_view const &json)
	{
		if (json.size() >= 2 && json[0] == '"')
		{
			bool escaped = false;
			size_t size = 0;
			for (auto c : json.substr(1))
			{
				if (!escaped && c == '"')
					break;
				if (c == '\\')
					escaped = true;
				else
					escaped = false;
				size += 1;
			}
			auto ret = json.substr(1, size);
			return std::make_optional(make_tuple(Token(String{Unescape(std::string(ret))}), json.substr(size + 2)));
		}
		else
		{
			return std::nullopt;
		}
	}

	static std::optional<std::tuple<Token, std::string_view>> LexSyntax(std::string_view const &json)
	{
		if (json.size() >= 1)
		{
			switch (json[0])
			{
			case ',':
				return std::make_optional(make_tuple(Token(Comma{}), json.substr(1)));
			case ':':
				return std::make_optional(make_tuple(Token(Colon{}), json.substr(1)));
			case '[':
				return std::make_optional(make_tuple(Token(LeftBracket{}), json.substr(1)));
			case ']':
				return std::make_optional(make_tuple(Token(RightBracket{}), json.substr(1)));
			case '{':
				return std::make_optional(make_tuple(Token(LeftBrace{}), json.substr(1)));
			case '}':
				return std::make_optional(make_tuple(Token(RightBrace{}), json.substr(1)));
			default:
				return std::nullopt;
			}
		}
		else
			return std::nullopt;
	}

	std::vector<Token> Lex(std::string_view json)
	{
		static std::array<char, 4> white_spaces{' ', '\t', '\n', '\r'};

		std::vector<Token> tokens;
		tokens.reserve(json.size() / 3);

		while (json.size() > 0)
		{
			auto c = json[0];
			if (Contains(white_spaces, c))
			{
				json = json.substr(1);
			}
			else
			{
				static std::array<std::optional<std::tuple<Token, std::string_view>> (*)(std::string_view const &json), 5> lexers{
					LexNull,
					LexBool,
					LexNumber,
					LexString,
					LexSyntax,
				};

				for (auto lexer : lexers)
				{
					if (auto t = lexer(json))
					{
						auto [token, new_json] = t.value();
						tokens.emplace_back(std::move(token));
						json = new_json;
						break;
					}
				}
			}
		}
		return tokens;
	}
}
