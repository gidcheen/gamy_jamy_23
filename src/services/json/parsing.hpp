#pragma once

#include <optional>
#include <tuple>
#include <vector>

#include "object.hpp"
#include "lexing.hpp"

namespace json
{
	std::optional<Object> Parse(std::vector<Token> const &tokens);
}
