#pragma once

#include <algebra/algebra.hpp>

namespace state
{
	class Sun final
	{
	private:
		float angle;
		float distance;
		algebra::Matrix3x3 transform;

	public:
		Sun(float distance) : distance(distance)
		{
			AddAngle(0);
		}

		float GetAngle() const { return angle; }
		float GetDistance() const { return distance; }
		algebra::Matrix3x3 const &GetTransform() { return transform; }

		algebra::Vector2 GetPosition() const
		{
			algebra::Vector3 point = transform * algebra::Vector3{0, 0, 1};
			return {point.x, point.y};
		}

		void AddAngle(float angle)
		{
			this->angle += angle;
			transform = algebra::Matrix3x3::Rotation(this->angle) * algebra::Matrix3x3::Translation({0, distance});
		}
	};
}
