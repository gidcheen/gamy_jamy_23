#pragma once

#include <memory>
#include <algebra/algebra.hpp>
#include <graphics/renderer.hpp>
#include <graphics/mesh.hpp>
#include <state/sun.hpp>

namespace view
{
	class SunView final
	{
	private:
		std::shared_ptr<state::Sun> sun;
		std::unique_ptr<graphics::Mesh> mesh;

	public:
		SunView(std::shared_ptr<state::Sun> sun);

		void Render(graphics::Renderer &renderer);

		std::shared_ptr<state::Sun> const &GetSun() { return sun; };
	};
}
