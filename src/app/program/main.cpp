#include <windowing/windowing.hpp>
#include <windowing/window.hpp>
#include <graphics/graphics.hpp>
#include <graphics/sprite.hpp>
#include <graphics/shader.hpp>
#include <graphics/mesh.hpp>
#include <graphics/material.hpp>
#include <graphics/camera.hpp>
#include <graphics/renderer.hpp>
#include <audio/audio.hpp>

#include <state/world.hpp>
#include <view/world.hpp>

#include <iostream>
#include <chrono>

#include <gltf/gltf.hpp>

namespace gamy_jamy
{
	extern "C" int main()
	{
		windowing::Windowing::Init();
		int width = 500, height = 500;
		windowing::Window window("gamy jamy", width, height);

		audio::Audio audio = audio::Audio();
		audio.Loop("atmo_neutral");

		graphics::Graphics::Init(window.GetLoader());

		graphics::Renderer renderer;

		for (int i = 0; i < 3; i++)
		{
			state::World world(i);
			view::WorldView worldView(world, audio);
			worldView.SetScreenSize({(float)width, (float)height});

			// the work...
			bool moveSunLeft = false;
			bool moveSunRight = false;

			auto last_frame_time = windowing::Windowing::GetTime() - 0.16;
			auto current_frame_time = windowing::Windowing::GetTime();

			while (!window.ShouldClose())
			{
				float dt = current_frame_time - last_frame_time;
				std::cout << 1 / dt << std::endl;
				last_frame_time = current_frame_time;
				current_frame_time = windowing::Windowing::GetTime();

				worldView.Update(dt);

				windowing::Windowing::Poll();
				window.HandleEvents(
					[&](windowing::InputEvent const &event)
					{
						if (event.type == windowing::InputEventType::Key &&
							event.key.action == windowing::InputAction::Down &&
							event.key.key == windowing::KeyboardKey::Escape)
						{
							window.Close();
						}
						else if (event.type == windowing::InputEventType::Key &&
								 event.key.action == windowing::InputAction::Down &&
								 event.key.key == windowing::KeyboardKey::KeyA)
						{
							moveSunLeft = true;
						}
						else if (event.type == windowing::InputEventType::Key &&
								 event.key.action == windowing::InputAction::Up &&
								 event.key.key == windowing::KeyboardKey::KeyA)
						{
							moveSunLeft = false;
						}
						else if (event.type == windowing::InputEventType::Key &&
								 event.key.action == windowing::InputAction::Down &&
								 event.key.key == windowing::KeyboardKey::KeyD)
						{
							moveSunRight = true;
						}
						else if (event.type == windowing::InputEventType::Key &&
								 event.key.action == windowing::InputAction::Up &&
								 event.key.key == windowing::KeyboardKey::KeyD)
						{
							moveSunRight = false;
						}
						else if (event.type == windowing::InputEventType::Mousebutton &&
								 event.mousebutton.action == windowing::InputAction::Up &&
								 event.mousebutton.button == windowing::MouseButton::Left)
						{
							std::cout << "Works!\n";
							worldView.MouseUp();
						}
						else if (event.type == windowing::InputEventType::Windowsize)
						{
							width = event.windowsize.width;
							height = event.windowsize.height;
							worldView.SetScreenSize({(float)width, (float)height});
						}
						else if (event.type == windowing::InputEventType::Cursorpos)
						{
							worldView.MouseMove({(float)event.cursorpos.xpos, (float)event.cursorpos.ypos});
						}
					});

				if (moveSunLeft || moveSunRight)
				{
					float sunMove = 0 + (moveSunLeft ? 1 : 0) + (moveSunRight ? -1 : 0);
					worldView.MoveSun(sunMove, dt);
				}

				worldView.Render(renderer);

				renderer.Flush();
				window.SwapBuffers();

				if (world.Won())
				{
					audio.Oneshot("sounds/atmo_overtones.wav");
					break;
				}
				else if (world.Lost())
				{
					audio.Oneshot("sounds/atmo_dark.wav");
					break;
				}
			}
		}

		float exitTime = windowing::Windowing::GetTime() + 5;

		while (exitTime > windowing::Windowing::GetTime())
		{
			window.SwapBuffers();
			windowing::Windowing::Poll();
			window.HandleEvents([](auto const &event) {});
		}

		return 0;
	}
}
