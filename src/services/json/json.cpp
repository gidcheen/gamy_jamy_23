#include "json.hpp"

#include "escaping.hpp"

#include "lexing.hpp"
#include "parsing.hpp"

namespace json
{
	std::optional<Object> JsonToObject(std::string const &json)
	{
		auto tokens = Lex(json);
		auto value = Parse(tokens);
		return value;
	}

	std::string ObjectToJson(Object const &value)
	{
		if (value.IsNull())
			return "null";
		if (auto b = value.AsBool())
			return std::to_string(b.value());
		if (auto i = value.AsInt())
			return std::to_string(i.value());
		if (auto f = value.AsDouble())
			return std::to_string(f.value());
		if (auto s = value.AsString())
			return "\"" + Escape(s.value()) + "\"";
		if (auto c = value.GetArrayCount())
		{
			std::string json = "[";
			for (size_t i = 0; i < c.value(); i++)
				json += ObjectToJson(*value.GetArrayValue(i).value()) + (i != c.value() - 1 ? "," : "");
			json += "]";
			return json;
		}
		if (auto c = value.GetObjectCount())
		{
			std::string json = "{";
			for (size_t i = 0; i < c.value(); i++)
				json += "\"" + *value.GetObjectName(i).value() + "\"" + ":" + ObjectToJson(*value.GetObjectValue(i).value()) + (i != c.value() - 1 ? "," : "");
			json += "}";
			return json;
		}
		exit(EXIT_FAILURE);
	}
}
