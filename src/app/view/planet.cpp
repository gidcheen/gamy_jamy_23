#include "planet.hpp"

#include <fstream>

#include "shared_assets.hpp"

namespace view
{
	PlanetView::PlanetView(std::shared_ptr<state::Planet> planet)
		: planet(planet),
		  mesh(planet->Vertices()),
		  material(SharedAssets::Get().planetMaterial)
	{
	}

	void PlanetView::Render(graphics::Renderer &renderer)
	{
		material.color[2] = (planet->Water() / (float)state::Planet::MaxWater) * 0.7f;

		graphics::MeshSubmit submit{
			.mesh = &mesh,
			.shader = &SharedAssets::Get().defaultShader,
			.material = &material,
			.sprite = nullptr,
		};
		renderer.Submit(submit);
	}
}
