#include "mesh.hpp"

#include <open_gl/gl.hpp>

namespace graphics
{
	static std::vector<algebra::Vector2> const quad{
		{0.0f, 0.0f},
		{0.0f, 1.0f},
		{1.0f, 1.0f},

		{0.0f, 0.0f},
		{1.0f, 1.0f},
		{1.0f, 0.0f},
	};

	Mesh::Mesh(std::vector<algebra::Vector2> const &vertices)
		: triangleCount(vertices.size() / 3)
	{
		std::vector<algebra::Vector2> verticesPadded;
		verticesPadded.reserve(vertices.size() * 2);
		for (auto &v : vertices)
		{
			verticesPadded.push_back(v);
			verticesPadded.emplace_back();
		}

		glCreateBuffers(1, &vertexBuffer);
		glNamedBufferData(vertexBuffer, verticesPadded.size() * sizeof(algebra::Vector2), verticesPadded.data(), GL_STATIC_DRAW);

		glCreateVertexArrays(1, &vertexArray);
		glVertexArrayVertexBuffer(vertexArray, 0, vertexBuffer, 0, (sizeof(algebra::Vector2) * 2));
		glEnableVertexArrayAttrib(vertexArray, 0);
	}

	Mesh::~Mesh()
	{
		glDeleteBuffers(1, &vertexBuffer);
		glDeleteVertexArrays(1, &vertexArray);
	}

	Mesh Mesh::Quad()
	{
		return Mesh(quad);
	}
}
