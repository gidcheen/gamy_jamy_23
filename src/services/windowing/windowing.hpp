#pragma once

namespace windowing
{
	class Windowing final
	{
	private:
		Windowing();
		~Windowing();

	public:
		static void Init();
		static void Poll();
		static float GetTime();
	};
}
