#include "world.hpp"

#include <gltf/gltf.hpp>

#include "shared_assets.hpp"

namespace view
{
	WorldView::WorldView(state::World &world, audio::Audio &audio)
		: audio(audio),
		  world(world),
		  sun(world.GetSun())
	{
		for (std::shared_ptr<state::Planet> planet : world.GetPlanets())
		{
			planets.emplace_back(std::make_unique<PlanetView>(planet));
		}
	}

	void WorldView::Update(float dt)
	{
		cameraRot += dt * 0.05f;
		cameraTransform = algebra::Matrix3x3::TRS({0, -1}, cameraRot, {10, 10});

		algebra::Vector2 transformedMousePos = mousePosScreenSpace;
		transformedMousePos.x = (((mousePosScreenSpace.x / camera.frameSize.x) - 0.5) * 2) * camera.Aspect();
		transformedMousePos.y = (((mousePosScreenSpace.y / camera.frameSize.y) - 0.5) * 2) * -1;
		mousePosWorldSpace = cameraTransform * algebra::Vector3{transformedMousePos.x, transformedMousePos.y, 1};

		world.Update(dt);

		for (auto &tree : world.GetTrees())
		{
			auto find = [&](std::unique_ptr<TreeView> const &t) -> bool
			{ return t->Tree().get() == tree.get(); };

			if (std::find_if(trees.begin(), trees.end(), find) == trees.end())
			{
				this->trees.push_back(std::make_unique<TreeView>(tree));
			}
		}

		if (trees.size() > treeAmount)
		{
			treeAmount = trees.size();
			audio.Play("kick_one");
			audio.Play("roots_growing");
		}

		size_t rootCounter = 0;
		for (auto &treeView : trees)
		{
			if (treeView->isRoot)
			{
				++rootCounter;
			}
		}

		if (rootCounter > rootAmount)
		{
			rootAmount = rootCounter;
			audio.Play("success_one");
		}
	}

	void WorldView::MouseMove(algebra::Vector2 pos)
	{
		mousePosScreenSpace = pos;
	}

	void WorldView::MoveSun(float amout, float dt)
	{
		sun.GetSun()->AddAngle(amout * dt);
	}

	void WorldView::MouseUp()
	{
		world.DetectSpawnPoint({mousePosWorldSpace.x, mousePosWorldSpace.y});
	}

	void WorldView::Render(graphics::Renderer &renderer)
	{
		graphics::CameraSubmit cameraSubmit{.camera = &camera};
		cameraSubmit.transform = cameraTransform;
		renderer.Submit(cameraSubmit);

		sun.Render(renderer);
		for (std::unique_ptr<PlanetView> &planet : planets)
		{
			planet->Render(renderer);
		}
		for (std::unique_ptr<TreeView> &tree : trees)
		{
			tree->Render(renderer);
		}

		graphics::MeshSubmit submit{
			.mesh = &SharedAssets::Get().quadMesh,
			.shader = &SharedAssets::Get().defaultShader,
			.material = &SharedAssets::Get().defaultMaterial,
			.sprite = nullptr,
		};
		submit.depth = 0.1;
		submit.transform = algebra::Matrix3x3::TRS({mousePosWorldSpace.x, mousePosWorldSpace.y}, 0, {0.1, 0.1});
		renderer.Submit(submit);
	}
}
