#pragma once

#include <algebra/algebra.hpp>
#include <string>
#include <vector>

#include "mesh.hpp"
#include "shader.hpp"
#include "material.hpp"
#include "camera.hpp"
#include "sprite.hpp"

namespace graphics
{
	struct Submit
	{
		float depth = 0;
		algebra::Matrix3x3 transform = algebra::Matrix3x3::Identity();
	};
	struct CameraSubmit final : public Submit
	{
		Camera const *camera;
	};
	struct MeshSubmit final : public Submit
	{
		Mesh const *mesh;
		Shader const *shader;
		Material const *material;
		Sprite const *sprite;
	};

	class Renderer final
	{
	private:
		std::vector<CameraSubmit> cameras;
		std::vector<MeshSubmit> meshes;

	public:
		Renderer(){};
		Renderer(Renderer &) = delete;
		void Submit(CameraSubmit camera) { cameras.push_back(camera); }
		void Submit(MeshSubmit mesh) { meshes.push_back(mesh); }
		void Flush();
	};
}
