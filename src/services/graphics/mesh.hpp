#pragma once

#include <algebra/vector.hpp>
#include <vector>

namespace graphics
{
	class Mesh final
	{
		friend class Renderer;

	private:
		unsigned int vertexBuffer = 0;
		unsigned int vertexArray = 0;

		size_t const triangleCount;

	public:
		Mesh(std::vector<algebra::Vector2> const &vertices);
		Mesh(Mesh const &) = delete;
		~Mesh();

		inline size_t TriangleCount() const { return triangleCount; }
		inline size_t VertexCount() const { return triangleCount * 3; }

		static Mesh Quad();
	};
}
