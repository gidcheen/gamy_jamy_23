#include "gl.hpp"

static void (*proc_glActiveShaderProgram)(GLuint pipeline, GLuint program) = NULL;
static void (*proc_glActiveTexture)(GLenum texture) = NULL;
static void (*proc_glAttachShader)(GLuint program, GLuint shader) = NULL;
static void (*proc_glBeginConditionalRender)(GLuint id, GLenum mode) = NULL;
static void (*proc_glBeginQuery)(GLenum target, GLuint id) = NULL;
static void (*proc_glBeginQueryIndexed)(GLenum target, GLuint index, GLuint id) = NULL;
static void (*proc_glBeginTransformFeedback)(GLenum primitiveMode) = NULL;
static void (*proc_glBindAttribLocation)(GLuint program, GLuint index, const GLchar *name) = NULL;
static void (*proc_glBindBuffer)(GLenum target, GLuint buffer) = NULL;
static void (*proc_glBindBufferBase)(GLenum target, GLuint index, GLuint buffer) = NULL;
static void (*proc_glBindBufferRange)(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size) = NULL;
static void (*proc_glBindBuffersBase)(GLenum target, GLuint first, GLsizei count, const GLuint *buffers) = NULL;
static void (*proc_glBindBuffersRange)(GLenum target, GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizeiptr *sizes) = NULL;
static void (*proc_glBindFragDataLocation)(GLuint program, GLuint color, const GLchar *name) = NULL;
static void (*proc_glBindFragDataLocationIndexed)(GLuint program, GLuint colorNumber, GLuint index, const GLchar *name) = NULL;
static void (*proc_glBindFramebuffer)(GLenum target, GLuint framebuffer) = NULL;
static void (*proc_glBindImageTexture)(GLuint unit, GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum access, GLenum format) = NULL;
static void (*proc_glBindImageTextures)(GLuint first, GLsizei count, const GLuint *textures) = NULL;
static void (*proc_glBindProgramPipeline)(GLuint pipeline) = NULL;
static void (*proc_glBindRenderbuffer)(GLenum target, GLuint renderbuffer) = NULL;
static void (*proc_glBindSampler)(GLuint unit, GLuint sampler) = NULL;
static void (*proc_glBindSamplers)(GLuint first, GLsizei count, const GLuint *samplers) = NULL;
static void (*proc_glBindTexture)(GLenum target, GLuint texture) = NULL;
static void (*proc_glBindTextureUnit)(GLuint unit, GLuint texture) = NULL;
static void (*proc_glBindTextures)(GLuint first, GLsizei count, const GLuint *textures) = NULL;
static void (*proc_glBindTransformFeedback)(GLenum target, GLuint id) = NULL;
static void (*proc_glBindVertexArray)(GLuint array) = NULL;
static void (*proc_glBindVertexBuffer)(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride) = NULL;
static void (*proc_glBindVertexBuffers)(GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizei *strides) = NULL;
static void (*proc_glBlendColor)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) = NULL;
static void (*proc_glBlendEquation)(GLenum mode) = NULL;
static void (*proc_glBlendEquationSeparate)(GLenum modeRGB, GLenum modeAlpha) = NULL;
static void (*proc_glBlendEquationSeparatei)(GLuint buf, GLenum modeRGB, GLenum modeAlpha) = NULL;
static void (*proc_glBlendEquationi)(GLuint buf, GLenum mode) = NULL;
static void (*proc_glBlendFunc)(GLenum sfactor, GLenum dfactor) = NULL;
static void (*proc_glBlendFuncSeparate)(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha) = NULL;
static void (*proc_glBlendFuncSeparatei)(GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha) = NULL;
static void (*proc_glBlendFunci)(GLuint buf, GLenum src, GLenum dst) = NULL;
static void (*proc_glBlitFramebuffer)(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter) = NULL;
static void (*proc_glBlitNamedFramebuffer)(GLuint readFramebuffer, GLuint drawFramebuffer, GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter) = NULL;
static void (*proc_glBufferData)(GLenum target, GLsizeiptr size, const void *data, GLenum usage) = NULL;
static void (*proc_glBufferStorage)(GLenum target, GLsizeiptr size, const void *data, GLbitfield flags) = NULL;
static void (*proc_glBufferSubData)(GLenum target, GLintptr offset, GLsizeiptr size, const void *data) = NULL;
static GLenum (*proc_glCheckFramebufferStatus)(GLenum target) = NULL;
static GLenum (*proc_glCheckNamedFramebufferStatus)(GLuint framebuffer, GLenum target) = NULL;
static void (*proc_glClampColor)(GLenum target, GLenum clamp) = NULL;
static void (*proc_glClear)(GLbitfield mask) = NULL;
static void (*proc_glClearBufferData)(GLenum target, GLenum internalformat, GLenum format, GLenum type, const void *data) = NULL;
static void (*proc_glClearBufferSubData)(GLenum target, GLenum internalformat, GLintptr offset, GLsizeiptr size, GLenum format, GLenum type, const void *data) = NULL;
static void (*proc_glClearBufferfi)(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil) = NULL;
static void (*proc_glClearBufferfv)(GLenum buffer, GLint drawbuffer, const GLfloat *value) = NULL;
static void (*proc_glClearBufferiv)(GLenum buffer, GLint drawbuffer, const GLint *value) = NULL;
static void (*proc_glClearBufferuiv)(GLenum buffer, GLint drawbuffer, const GLuint *value) = NULL;
static void (*proc_glClearColor)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) = NULL;
static void (*proc_glClearDepth)(GLdouble depth) = NULL;
static void (*proc_glClearDepthf)(GLfloat d) = NULL;
static void (*proc_glClearNamedBufferData)(GLuint buffer, GLenum internalformat, GLenum format, GLenum type, const void *data) = NULL;
static void (*proc_glClearNamedBufferSubData)(GLuint buffer, GLenum internalformat, GLintptr offset, GLsizeiptr size, GLenum format, GLenum type, const void *data) = NULL;
static void (*proc_glClearNamedFramebufferfi)(GLuint framebuffer, GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil) = NULL;
static void (*proc_glClearNamedFramebufferfv)(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLfloat *value) = NULL;
static void (*proc_glClearNamedFramebufferiv)(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLint *value) = NULL;
static void (*proc_glClearNamedFramebufferuiv)(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLuint *value) = NULL;
static void (*proc_glClearStencil)(GLint s) = NULL;
static void (*proc_glClearTexImage)(GLuint texture, GLint level, GLenum format, GLenum type, const void *data) = NULL;
static void (*proc_glClearTexSubImage)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *data) = NULL;
static GLenum (*proc_glClientWaitSync)(GLsync sync, GLbitfield flags, GLuint64 timeout) = NULL;
static void (*proc_glClipControl)(GLenum origin, GLenum depth) = NULL;
static void (*proc_glColorMask)(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha) = NULL;
static void (*proc_glColorMaski)(GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a) = NULL;
static void (*proc_glCompileShader)(GLuint shader) = NULL;
static void (*proc_glCompressedTexImage1D)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLint border, GLsizei imageSize, const void *data) = NULL;
static void (*proc_glCompressedTexImage2D)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void *data) = NULL;
static void (*proc_glCompressedTexImage3D)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data) = NULL;
static void (*proc_glCompressedTexSubImage1D)(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, const void *data) = NULL;
static void (*proc_glCompressedTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const void *data) = NULL;
static void (*proc_glCompressedTexSubImage3D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data) = NULL;
static void (*proc_glCompressedTextureSubImage1D)(GLuint texture, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, const void *data) = NULL;
static void (*proc_glCompressedTextureSubImage2D)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const void *data) = NULL;
static void (*proc_glCompressedTextureSubImage3D)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data) = NULL;
static void (*proc_glCopyBufferSubData)(GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size) = NULL;
static void (*proc_glCopyImageSubData)(GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth) = NULL;
static void (*proc_glCopyNamedBufferSubData)(GLuint readBuffer, GLuint writeBuffer, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size) = NULL;
static void (*proc_glCopyTexImage1D)(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLint border) = NULL;
static void (*proc_glCopyTexImage2D)(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border) = NULL;
static void (*proc_glCopyTexSubImage1D)(GLenum target, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width) = NULL;
static void (*proc_glCopyTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height) = NULL;
static void (*proc_glCopyTexSubImage3D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height) = NULL;
static void (*proc_glCopyTextureSubImage1D)(GLuint texture, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width) = NULL;
static void (*proc_glCopyTextureSubImage2D)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height) = NULL;
static void (*proc_glCopyTextureSubImage3D)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height) = NULL;
static void (*proc_glCreateBuffers)(GLsizei n, GLuint *buffers) = NULL;
static void (*proc_glCreateFramebuffers)(GLsizei n, GLuint *framebuffers) = NULL;
static GLuint (*proc_glCreateProgram)() = NULL;
static void (*proc_glCreateProgramPipelines)(GLsizei n, GLuint *pipelines) = NULL;
static void (*proc_glCreateQueries)(GLenum target, GLsizei n, GLuint *ids) = NULL;
static void (*proc_glCreateRenderbuffers)(GLsizei n, GLuint *renderbuffers) = NULL;
static void (*proc_glCreateSamplers)(GLsizei n, GLuint *samplers) = NULL;
static GLuint (*proc_glCreateShader)(GLenum type) = NULL;
static GLuint (*proc_glCreateShaderProgramv)(GLenum type, GLsizei count, const GLchar *const *strings) = NULL;
static void (*proc_glCreateTextures)(GLenum target, GLsizei n, GLuint *textures) = NULL;
static void (*proc_glCreateTransformFeedbacks)(GLsizei n, GLuint *ids) = NULL;
static void (*proc_glCreateVertexArrays)(GLsizei n, GLuint *arrays) = NULL;
static void (*proc_glCullFace)(GLenum mode) = NULL;
static void (*proc_glDebugMessageCallback)(GLDEBUGPROC callback, const void *userParam) = NULL;
static void (*proc_glDebugMessageControl)(GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled) = NULL;
static void (*proc_glDebugMessageInsert)(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *buf) = NULL;
static void (*proc_glDeleteBuffers)(GLsizei n, const GLuint *buffers) = NULL;
static void (*proc_glDeleteFramebuffers)(GLsizei n, const GLuint *framebuffers) = NULL;
static void (*proc_glDeleteProgram)(GLuint program) = NULL;
static void (*proc_glDeleteProgramPipelines)(GLsizei n, const GLuint *pipelines) = NULL;
static void (*proc_glDeleteQueries)(GLsizei n, const GLuint *ids) = NULL;
static void (*proc_glDeleteRenderbuffers)(GLsizei n, const GLuint *renderbuffers) = NULL;
static void (*proc_glDeleteSamplers)(GLsizei count, const GLuint *samplers) = NULL;
static void (*proc_glDeleteShader)(GLuint shader) = NULL;
static void (*proc_glDeleteSync)(GLsync sync) = NULL;
static void (*proc_glDeleteTextures)(GLsizei n, const GLuint *textures) = NULL;
static void (*proc_glDeleteTransformFeedbacks)(GLsizei n, const GLuint *ids) = NULL;
static void (*proc_glDeleteVertexArrays)(GLsizei n, const GLuint *arrays) = NULL;
static void (*proc_glDepthFunc)(GLenum func) = NULL;
static void (*proc_glDepthMask)(GLboolean flag) = NULL;
static void (*proc_glDepthRange)(GLdouble n, GLdouble f) = NULL;
static void (*proc_glDepthRangeArrayv)(GLuint first, GLsizei count, const GLdouble *v) = NULL;
static void (*proc_glDepthRangeIndexed)(GLuint index, GLdouble n, GLdouble f) = NULL;
static void (*proc_glDepthRangef)(GLfloat n, GLfloat f) = NULL;
static void (*proc_glDetachShader)(GLuint program, GLuint shader) = NULL;
static void (*proc_glDisable)(GLenum cap) = NULL;
static void (*proc_glDisableVertexArrayAttrib)(GLuint vaobj, GLuint index) = NULL;
static void (*proc_glDisableVertexAttribArray)(GLuint index) = NULL;
static void (*proc_glDisablei)(GLenum target, GLuint index) = NULL;
static void (*proc_glDispatchCompute)(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z) = NULL;
static void (*proc_glDispatchComputeIndirect)(GLintptr indirect) = NULL;
static void (*proc_glDrawArrays)(GLenum mode, GLint first, GLsizei count) = NULL;
static void (*proc_glDrawArraysIndirect)(GLenum mode, const void *indirect) = NULL;
static void (*proc_glDrawArraysInstanced)(GLenum mode, GLint first, GLsizei count, GLsizei instancecount) = NULL;
static void (*proc_glDrawArraysInstancedBaseInstance)(GLenum mode, GLint first, GLsizei count, GLsizei instancecount, GLuint baseinstance) = NULL;
static void (*proc_glDrawBuffer)(GLenum buf) = NULL;
static void (*proc_glDrawBuffers)(GLsizei n, const GLenum *bufs) = NULL;
static void (*proc_glDrawElements)(GLenum mode, GLsizei count, GLenum type, const void *indices) = NULL;
static void (*proc_glDrawElementsBaseVertex)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLint basevertex) = NULL;
static void (*proc_glDrawElementsIndirect)(GLenum mode, GLenum type, const void *indirect) = NULL;
static void (*proc_glDrawElementsInstanced)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount) = NULL;
static void (*proc_glDrawElementsInstancedBaseInstance)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLuint baseinstance) = NULL;
static void (*proc_glDrawElementsInstancedBaseVertex)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex) = NULL;
static void (*proc_glDrawElementsInstancedBaseVertexBaseInstance)(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex, GLuint baseinstance) = NULL;
static void (*proc_glDrawRangeElements)(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices) = NULL;
static void (*proc_glDrawRangeElementsBaseVertex)(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, GLint basevertex) = NULL;
static void (*proc_glDrawTransformFeedback)(GLenum mode, GLuint id) = NULL;
static void (*proc_glDrawTransformFeedbackInstanced)(GLenum mode, GLuint id, GLsizei instancecount) = NULL;
static void (*proc_glDrawTransformFeedbackStream)(GLenum mode, GLuint id, GLuint stream) = NULL;
static void (*proc_glDrawTransformFeedbackStreamInstanced)(GLenum mode, GLuint id, GLuint stream, GLsizei instancecount) = NULL;
static void (*proc_glEnable)(GLenum cap) = NULL;
static void (*proc_glEnableVertexArrayAttrib)(GLuint vaobj, GLuint index) = NULL;
static void (*proc_glEnableVertexAttribArray)(GLuint index) = NULL;
static void (*proc_glEnablei)(GLenum target, GLuint index) = NULL;
static void (*proc_glEndConditionalRender)() = NULL;
static void (*proc_glEndQuery)(GLenum target) = NULL;
static void (*proc_glEndQueryIndexed)(GLenum target, GLuint index) = NULL;
static void (*proc_glEndTransformFeedback)() = NULL;
static GLsync (*proc_glFenceSync)(GLenum condition, GLbitfield flags) = NULL;
static void (*proc_glFinish)() = NULL;
static void (*proc_glFlush)() = NULL;
static void (*proc_glFlushMappedBufferRange)(GLenum target, GLintptr offset, GLsizeiptr length) = NULL;
static void (*proc_glFlushMappedNamedBufferRange)(GLuint buffer, GLintptr offset, GLsizeiptr length) = NULL;
static void (*proc_glFramebufferParameteri)(GLenum target, GLenum pname, GLint param) = NULL;
static void (*proc_glFramebufferRenderbuffer)(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer) = NULL;
static void (*proc_glFramebufferTexture)(GLenum target, GLenum attachment, GLuint texture, GLint level) = NULL;
static void (*proc_glFramebufferTexture1D)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) = NULL;
static void (*proc_glFramebufferTexture2D)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) = NULL;
static void (*proc_glFramebufferTexture3D)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLint zoffset) = NULL;
static void (*proc_glFramebufferTextureLayer)(GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer) = NULL;
static void (*proc_glFrontFace)(GLenum mode) = NULL;
static void (*proc_glGenBuffers)(GLsizei n, GLuint *buffers) = NULL;
static void (*proc_glGenFramebuffers)(GLsizei n, GLuint *framebuffers) = NULL;
static void (*proc_glGenProgramPipelines)(GLsizei n, GLuint *pipelines) = NULL;
static void (*proc_glGenQueries)(GLsizei n, GLuint *ids) = NULL;
static void (*proc_glGenRenderbuffers)(GLsizei n, GLuint *renderbuffers) = NULL;
static void (*proc_glGenSamplers)(GLsizei count, GLuint *samplers) = NULL;
static void (*proc_glGenTextures)(GLsizei n, GLuint *textures) = NULL;
static void (*proc_glGenTransformFeedbacks)(GLsizei n, GLuint *ids) = NULL;
static void (*proc_glGenVertexArrays)(GLsizei n, GLuint *arrays) = NULL;
static void (*proc_glGenerateMipmap)(GLenum target) = NULL;
static void (*proc_glGenerateTextureMipmap)(GLuint texture) = NULL;
static void (*proc_glGetActiveAtomicCounterBufferiv)(GLuint program, GLuint bufferIndex, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetActiveAttrib)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) = NULL;
static void (*proc_glGetActiveSubroutineName)(GLuint program, GLenum shadertype, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name) = NULL;
static void (*proc_glGetActiveSubroutineUniformName)(GLuint program, GLenum shadertype, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name) = NULL;
static void (*proc_glGetActiveSubroutineUniformiv)(GLuint program, GLenum shadertype, GLuint index, GLenum pname, GLint *values) = NULL;
static void (*proc_glGetActiveUniform)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) = NULL;
static void (*proc_glGetActiveUniformBlockName)(GLuint program, GLuint uniformBlockIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformBlockName) = NULL;
static void (*proc_glGetActiveUniformBlockiv)(GLuint program, GLuint uniformBlockIndex, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetActiveUniformName)(GLuint program, GLuint uniformIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformName) = NULL;
static void (*proc_glGetActiveUniformsiv)(GLuint program, GLsizei uniformCount, const GLuint *uniformIndices, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetAttachedShaders)(GLuint program, GLsizei maxCount, GLsizei *count, GLuint *shaders) = NULL;
static GLint (*proc_glGetAttribLocation)(GLuint program, const GLchar *name) = NULL;
static void (*proc_glGetBooleani_v)(GLenum target, GLuint index, GLboolean *data) = NULL;
static void (*proc_glGetBooleanv)(GLenum pname, GLboolean *data) = NULL;
static void (*proc_glGetBufferParameteri64v)(GLenum target, GLenum pname, GLint64 *params) = NULL;
static void (*proc_glGetBufferParameteriv)(GLenum target, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetBufferPointerv)(GLenum target, GLenum pname, void **params) = NULL;
static void (*proc_glGetBufferSubData)(GLenum target, GLintptr offset, GLsizeiptr size, void *data) = NULL;
static void (*proc_glGetCompressedTexImage)(GLenum target, GLint level, void *img) = NULL;
static void (*proc_glGetCompressedTextureImage)(GLuint texture, GLint level, GLsizei bufSize, void *pixels) = NULL;
static void (*proc_glGetCompressedTextureSubImage)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLsizei bufSize, void *pixels) = NULL;
static GLuint (*proc_glGetDebugMessageLog)(GLuint count, GLsizei bufSize, GLenum *sources, GLenum *types, GLuint *ids, GLenum *severities, GLsizei *lengths, GLchar *messageLog) = NULL;
static void (*proc_glGetDoublei_v)(GLenum target, GLuint index, GLdouble *data) = NULL;
static void (*proc_glGetDoublev)(GLenum pname, GLdouble *data) = NULL;
static GLenum (*proc_glGetError)() = NULL;
static void (*proc_glGetFloati_v)(GLenum target, GLuint index, GLfloat *data) = NULL;
static void (*proc_glGetFloatv)(GLenum pname, GLfloat *data) = NULL;
static GLint (*proc_glGetFragDataIndex)(GLuint program, const GLchar *name) = NULL;
static GLint (*proc_glGetFragDataLocation)(GLuint program, const GLchar *name) = NULL;
static void (*proc_glGetFramebufferAttachmentParameteriv)(GLenum target, GLenum attachment, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetFramebufferParameteriv)(GLenum target, GLenum pname, GLint *params) = NULL;
static GLenum (*proc_glGetGraphicsResetStatus)() = NULL;
static void (*proc_glGetInteger64i_v)(GLenum target, GLuint index, GLint64 *data) = NULL;
static void (*proc_glGetInteger64v)(GLenum pname, GLint64 *data) = NULL;
static void (*proc_glGetIntegeri_v)(GLenum target, GLuint index, GLint *data) = NULL;
static void (*proc_glGetIntegerv)(GLenum pname, GLint *data) = NULL;
static void (*proc_glGetInternalformati64v)(GLenum target, GLenum internalformat, GLenum pname, GLsizei count, GLint64 *params) = NULL;
static void (*proc_glGetInternalformativ)(GLenum target, GLenum internalformat, GLenum pname, GLsizei count, GLint *params) = NULL;
static void (*proc_glGetMultisamplefv)(GLenum pname, GLuint index, GLfloat *val) = NULL;
static void (*proc_glGetNamedBufferParameteri64v)(GLuint buffer, GLenum pname, GLint64 *params) = NULL;
static void (*proc_glGetNamedBufferParameteriv)(GLuint buffer, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetNamedBufferPointerv)(GLuint buffer, GLenum pname, void **params) = NULL;
static void (*proc_glGetNamedBufferSubData)(GLuint buffer, GLintptr offset, GLsizeiptr size, void *data) = NULL;
static void (*proc_glGetNamedFramebufferAttachmentParameteriv)(GLuint framebuffer, GLenum attachment, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetNamedFramebufferParameteriv)(GLuint framebuffer, GLenum pname, GLint *param) = NULL;
static void (*proc_glGetNamedRenderbufferParameteriv)(GLuint renderbuffer, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetObjectLabel)(GLenum identifier, GLuint name, GLsizei bufSize, GLsizei *length, GLchar *label) = NULL;
static void (*proc_glGetObjectPtrLabel)(const void *ptr, GLsizei bufSize, GLsizei *length, GLchar *label) = NULL;
static void (*proc_glGetPointerv)(GLenum pname, void **params) = NULL;
static void (*proc_glGetProgramBinary)(GLuint program, GLsizei bufSize, GLsizei *length, GLenum *binaryFormat, void *binary) = NULL;
static void (*proc_glGetProgramInfoLog)(GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog) = NULL;
static void (*proc_glGetProgramInterfaceiv)(GLuint program, GLenum programInterface, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetProgramPipelineInfoLog)(GLuint pipeline, GLsizei bufSize, GLsizei *length, GLchar *infoLog) = NULL;
static void (*proc_glGetProgramPipelineiv)(GLuint pipeline, GLenum pname, GLint *params) = NULL;
static GLuint (*proc_glGetProgramResourceIndex)(GLuint program, GLenum programInterface, const GLchar *name) = NULL;
static GLint (*proc_glGetProgramResourceLocation)(GLuint program, GLenum programInterface, const GLchar *name) = NULL;
static GLint (*proc_glGetProgramResourceLocationIndex)(GLuint program, GLenum programInterface, const GLchar *name) = NULL;
static void (*proc_glGetProgramResourceName)(GLuint program, GLenum programInterface, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name) = NULL;
static void (*proc_glGetProgramResourceiv)(GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum *props, GLsizei count, GLsizei *length, GLint *params) = NULL;
static void (*proc_glGetProgramStageiv)(GLuint program, GLenum shadertype, GLenum pname, GLint *values) = NULL;
static void (*proc_glGetProgramiv)(GLuint program, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetQueryBufferObjecti64v)(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) = NULL;
static void (*proc_glGetQueryBufferObjectiv)(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) = NULL;
static void (*proc_glGetQueryBufferObjectui64v)(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) = NULL;
static void (*proc_glGetQueryBufferObjectuiv)(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) = NULL;
static void (*proc_glGetQueryIndexediv)(GLenum target, GLuint index, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetQueryObjecti64v)(GLuint id, GLenum pname, GLint64 *params) = NULL;
static void (*proc_glGetQueryObjectiv)(GLuint id, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetQueryObjectui64v)(GLuint id, GLenum pname, GLuint64 *params) = NULL;
static void (*proc_glGetQueryObjectuiv)(GLuint id, GLenum pname, GLuint *params) = NULL;
static void (*proc_glGetQueryiv)(GLenum target, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetRenderbufferParameteriv)(GLenum target, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetSamplerParameterIiv)(GLuint sampler, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetSamplerParameterIuiv)(GLuint sampler, GLenum pname, GLuint *params) = NULL;
static void (*proc_glGetSamplerParameterfv)(GLuint sampler, GLenum pname, GLfloat *params) = NULL;
static void (*proc_glGetSamplerParameteriv)(GLuint sampler, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetShaderInfoLog)(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog) = NULL;
static void (*proc_glGetShaderPrecisionFormat)(GLenum shadertype, GLenum precisiontype, GLint *range, GLint *precision) = NULL;
static void (*proc_glGetShaderSource)(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *source) = NULL;
static void (*proc_glGetShaderiv)(GLuint shader, GLenum pname, GLint *params) = NULL;
static const GLubyte *(*proc_glGetString)(GLenum name) = NULL;
static const GLubyte *(*proc_glGetStringi)(GLenum name, GLuint index) = NULL;
static GLuint (*proc_glGetSubroutineIndex)(GLuint program, GLenum shadertype, const GLchar *name) = NULL;
static GLint (*proc_glGetSubroutineUniformLocation)(GLuint program, GLenum shadertype, const GLchar *name) = NULL;
static void (*proc_glGetSynciv)(GLsync sync, GLenum pname, GLsizei count, GLsizei *length, GLint *values) = NULL;
static void (*proc_glGetTexImage)(GLenum target, GLint level, GLenum format, GLenum type, void *pixels) = NULL;
static void (*proc_glGetTexLevelParameterfv)(GLenum target, GLint level, GLenum pname, GLfloat *params) = NULL;
static void (*proc_glGetTexLevelParameteriv)(GLenum target, GLint level, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetTexParameterIiv)(GLenum target, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetTexParameterIuiv)(GLenum target, GLenum pname, GLuint *params) = NULL;
static void (*proc_glGetTexParameterfv)(GLenum target, GLenum pname, GLfloat *params) = NULL;
static void (*proc_glGetTexParameteriv)(GLenum target, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetTextureImage)(GLuint texture, GLint level, GLenum format, GLenum type, GLsizei bufSize, void *pixels) = NULL;
static void (*proc_glGetTextureLevelParameterfv)(GLuint texture, GLint level, GLenum pname, GLfloat *params) = NULL;
static void (*proc_glGetTextureLevelParameteriv)(GLuint texture, GLint level, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetTextureParameterIiv)(GLuint texture, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetTextureParameterIuiv)(GLuint texture, GLenum pname, GLuint *params) = NULL;
static void (*proc_glGetTextureParameterfv)(GLuint texture, GLenum pname, GLfloat *params) = NULL;
static void (*proc_glGetTextureParameteriv)(GLuint texture, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetTextureSubImage)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, GLsizei bufSize, void *pixels) = NULL;
static void (*proc_glGetTransformFeedbackVarying)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLsizei *size, GLenum *type, GLchar *name) = NULL;
static void (*proc_glGetTransformFeedbacki64_v)(GLuint xfb, GLenum pname, GLuint index, GLint64 *param) = NULL;
static void (*proc_glGetTransformFeedbacki_v)(GLuint xfb, GLenum pname, GLuint index, GLint *param) = NULL;
static void (*proc_glGetTransformFeedbackiv)(GLuint xfb, GLenum pname, GLint *param) = NULL;
static GLuint (*proc_glGetUniformBlockIndex)(GLuint program, const GLchar *uniformBlockName) = NULL;
static void (*proc_glGetUniformIndices)(GLuint program, GLsizei uniformCount, const GLchar *const *uniformNames, GLuint *uniformIndices) = NULL;
static GLint (*proc_glGetUniformLocation)(GLuint program, const GLchar *name) = NULL;
static void (*proc_glGetUniformSubroutineuiv)(GLenum shadertype, GLint location, GLuint *params) = NULL;
static void (*proc_glGetUniformdv)(GLuint program, GLint location, GLdouble *params) = NULL;
static void (*proc_glGetUniformfv)(GLuint program, GLint location, GLfloat *params) = NULL;
static void (*proc_glGetUniformiv)(GLuint program, GLint location, GLint *params) = NULL;
static void (*proc_glGetUniformuiv)(GLuint program, GLint location, GLuint *params) = NULL;
static void (*proc_glGetVertexArrayIndexed64iv)(GLuint vaobj, GLuint index, GLenum pname, GLint64 *param) = NULL;
static void (*proc_glGetVertexArrayIndexediv)(GLuint vaobj, GLuint index, GLenum pname, GLint *param) = NULL;
static void (*proc_glGetVertexArrayiv)(GLuint vaobj, GLenum pname, GLint *param) = NULL;
static void (*proc_glGetVertexAttribIiv)(GLuint index, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetVertexAttribIuiv)(GLuint index, GLenum pname, GLuint *params) = NULL;
static void (*proc_glGetVertexAttribLdv)(GLuint index, GLenum pname, GLdouble *params) = NULL;
static void (*proc_glGetVertexAttribPointerv)(GLuint index, GLenum pname, void **pointer) = NULL;
static void (*proc_glGetVertexAttribdv)(GLuint index, GLenum pname, GLdouble *params) = NULL;
static void (*proc_glGetVertexAttribfv)(GLuint index, GLenum pname, GLfloat *params) = NULL;
static void (*proc_glGetVertexAttribiv)(GLuint index, GLenum pname, GLint *params) = NULL;
static void (*proc_glGetnCompressedTexImage)(GLenum target, GLint lod, GLsizei bufSize, void *pixels) = NULL;
static void (*proc_glGetnTexImage)(GLenum target, GLint level, GLenum format, GLenum type, GLsizei bufSize, void *pixels) = NULL;
static void (*proc_glGetnUniformdv)(GLuint program, GLint location, GLsizei bufSize, GLdouble *params) = NULL;
static void (*proc_glGetnUniformfv)(GLuint program, GLint location, GLsizei bufSize, GLfloat *params) = NULL;
static void (*proc_glGetnUniformiv)(GLuint program, GLint location, GLsizei bufSize, GLint *params) = NULL;
static void (*proc_glGetnUniformuiv)(GLuint program, GLint location, GLsizei bufSize, GLuint *params) = NULL;
static void (*proc_glHint)(GLenum target, GLenum mode) = NULL;
static void (*proc_glInvalidateBufferData)(GLuint buffer) = NULL;
static void (*proc_glInvalidateBufferSubData)(GLuint buffer, GLintptr offset, GLsizeiptr length) = NULL;
static void (*proc_glInvalidateFramebuffer)(GLenum target, GLsizei numAttachments, const GLenum *attachments) = NULL;
static void (*proc_glInvalidateNamedFramebufferData)(GLuint framebuffer, GLsizei numAttachments, const GLenum *attachments) = NULL;
static void (*proc_glInvalidateNamedFramebufferSubData)(GLuint framebuffer, GLsizei numAttachments, const GLenum *attachments, GLint x, GLint y, GLsizei width, GLsizei height) = NULL;
static void (*proc_glInvalidateSubFramebuffer)(GLenum target, GLsizei numAttachments, const GLenum *attachments, GLint x, GLint y, GLsizei width, GLsizei height) = NULL;
static void (*proc_glInvalidateTexImage)(GLuint texture, GLint level) = NULL;
static void (*proc_glInvalidateTexSubImage)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth) = NULL;
static GLboolean (*proc_glIsBuffer)(GLuint buffer) = NULL;
static GLboolean (*proc_glIsEnabled)(GLenum cap) = NULL;
static GLboolean (*proc_glIsEnabledi)(GLenum target, GLuint index) = NULL;
static GLboolean (*proc_glIsFramebuffer)(GLuint framebuffer) = NULL;
static GLboolean (*proc_glIsProgram)(GLuint program) = NULL;
static GLboolean (*proc_glIsProgramPipeline)(GLuint pipeline) = NULL;
static GLboolean (*proc_glIsQuery)(GLuint id) = NULL;
static GLboolean (*proc_glIsRenderbuffer)(GLuint renderbuffer) = NULL;
static GLboolean (*proc_glIsSampler)(GLuint sampler) = NULL;
static GLboolean (*proc_glIsShader)(GLuint shader) = NULL;
static GLboolean (*proc_glIsSync)(GLsync sync) = NULL;
static GLboolean (*proc_glIsTexture)(GLuint texture) = NULL;
static GLboolean (*proc_glIsTransformFeedback)(GLuint id) = NULL;
static GLboolean (*proc_glIsVertexArray)(GLuint array) = NULL;
static void (*proc_glLineWidth)(GLfloat width) = NULL;
static void (*proc_glLinkProgram)(GLuint program) = NULL;
static void (*proc_glLogicOp)(GLenum opcode) = NULL;
static void *(*proc_glMapBuffer)(GLenum target, GLenum access) = NULL;
static void *(*proc_glMapBufferRange)(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access) = NULL;
static void *(*proc_glMapNamedBuffer)(GLuint buffer, GLenum access) = NULL;
static void *(*proc_glMapNamedBufferRange)(GLuint buffer, GLintptr offset, GLsizeiptr length, GLbitfield access) = NULL;
static void (*proc_glMemoryBarrier)(GLbitfield barriers) = NULL;
static void (*proc_glMemoryBarrierByRegion)(GLbitfield barriers) = NULL;
static void (*proc_glMinSampleShading)(GLfloat value) = NULL;
static void (*proc_glMultiDrawArrays)(GLenum mode, const GLint *first, const GLsizei *count, GLsizei drawcount) = NULL;
static void (*proc_glMultiDrawArraysIndirect)(GLenum mode, const void *indirect, GLsizei drawcount, GLsizei stride) = NULL;
static void (*proc_glMultiDrawElements)(GLenum mode, const GLsizei *count, GLenum type, const void *const *indices, GLsizei drawcount) = NULL;
static void (*proc_glMultiDrawElementsBaseVertex)(GLenum mode, const GLsizei *count, GLenum type, const void *const *indices, GLsizei drawcount, const GLint *basevertex) = NULL;
static void (*proc_glMultiDrawElementsIndirect)(GLenum mode, GLenum type, const void *indirect, GLsizei drawcount, GLsizei stride) = NULL;
static void (*proc_glNamedBufferData)(GLuint buffer, GLsizeiptr size, const void *data, GLenum usage) = NULL;
static void (*proc_glNamedBufferStorage)(GLuint buffer, GLsizeiptr size, const void *data, GLbitfield flags) = NULL;
static void (*proc_glNamedBufferSubData)(GLuint buffer, GLintptr offset, GLsizeiptr size, const void *data) = NULL;
static void (*proc_glNamedFramebufferDrawBuffer)(GLuint framebuffer, GLenum buf) = NULL;
static void (*proc_glNamedFramebufferDrawBuffers)(GLuint framebuffer, GLsizei n, const GLenum *bufs) = NULL;
static void (*proc_glNamedFramebufferParameteri)(GLuint framebuffer, GLenum pname, GLint param) = NULL;
static void (*proc_glNamedFramebufferReadBuffer)(GLuint framebuffer, GLenum src) = NULL;
static void (*proc_glNamedFramebufferRenderbuffer)(GLuint framebuffer, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer) = NULL;
static void (*proc_glNamedFramebufferTexture)(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level) = NULL;
static void (*proc_glNamedFramebufferTextureLayer)(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level, GLint layer) = NULL;
static void (*proc_glNamedRenderbufferStorage)(GLuint renderbuffer, GLenum internalformat, GLsizei width, GLsizei height) = NULL;
static void (*proc_glNamedRenderbufferStorageMultisample)(GLuint renderbuffer, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height) = NULL;
static void (*proc_glObjectLabel)(GLenum identifier, GLuint name, GLsizei length, const GLchar *label) = NULL;
static void (*proc_glObjectPtrLabel)(const void *ptr, GLsizei length, const GLchar *label) = NULL;
static void (*proc_glPatchParameterfv)(GLenum pname, const GLfloat *values) = NULL;
static void (*proc_glPatchParameteri)(GLenum pname, GLint value) = NULL;
static void (*proc_glPauseTransformFeedback)() = NULL;
static void (*proc_glPixelStoref)(GLenum pname, GLfloat param) = NULL;
static void (*proc_glPixelStorei)(GLenum pname, GLint param) = NULL;
static void (*proc_glPointParameterf)(GLenum pname, GLfloat param) = NULL;
static void (*proc_glPointParameterfv)(GLenum pname, const GLfloat *params) = NULL;
static void (*proc_glPointParameteri)(GLenum pname, GLint param) = NULL;
static void (*proc_glPointParameteriv)(GLenum pname, const GLint *params) = NULL;
static void (*proc_glPointSize)(GLfloat size) = NULL;
static void (*proc_glPolygonMode)(GLenum face, GLenum mode) = NULL;
static void (*proc_glPolygonOffset)(GLfloat factor, GLfloat units) = NULL;
static void (*proc_glPopDebugGroup)() = NULL;
static void (*proc_glPrimitiveRestartIndex)(GLuint index) = NULL;
static void (*proc_glProgramBinary)(GLuint program, GLenum binaryFormat, const void *binary, GLsizei length) = NULL;
static void (*proc_glProgramParameteri)(GLuint program, GLenum pname, GLint value) = NULL;
static void (*proc_glProgramUniform1d)(GLuint program, GLint location, GLdouble v0) = NULL;
static void (*proc_glProgramUniform1dv)(GLuint program, GLint location, GLsizei count, const GLdouble *value) = NULL;
static void (*proc_glProgramUniform1f)(GLuint program, GLint location, GLfloat v0) = NULL;
static void (*proc_glProgramUniform1fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value) = NULL;
static void (*proc_glProgramUniform1i)(GLuint program, GLint location, GLint v0) = NULL;
static void (*proc_glProgramUniform1iv)(GLuint program, GLint location, GLsizei count, const GLint *value) = NULL;
static void (*proc_glProgramUniform1ui)(GLuint program, GLint location, GLuint v0) = NULL;
static void (*proc_glProgramUniform1uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value) = NULL;
static void (*proc_glProgramUniform2d)(GLuint program, GLint location, GLdouble v0, GLdouble v1) = NULL;
static void (*proc_glProgramUniform2dv)(GLuint program, GLint location, GLsizei count, const GLdouble *value) = NULL;
static void (*proc_glProgramUniform2f)(GLuint program, GLint location, GLfloat v0, GLfloat v1) = NULL;
static void (*proc_glProgramUniform2fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value) = NULL;
static void (*proc_glProgramUniform2i)(GLuint program, GLint location, GLint v0, GLint v1) = NULL;
static void (*proc_glProgramUniform2iv)(GLuint program, GLint location, GLsizei count, const GLint *value) = NULL;
static void (*proc_glProgramUniform2ui)(GLuint program, GLint location, GLuint v0, GLuint v1) = NULL;
static void (*proc_glProgramUniform2uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value) = NULL;
static void (*proc_glProgramUniform3d)(GLuint program, GLint location, GLdouble v0, GLdouble v1, GLdouble v2) = NULL;
static void (*proc_glProgramUniform3dv)(GLuint program, GLint location, GLsizei count, const GLdouble *value) = NULL;
static void (*proc_glProgramUniform3f)(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2) = NULL;
static void (*proc_glProgramUniform3fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value) = NULL;
static void (*proc_glProgramUniform3i)(GLuint program, GLint location, GLint v0, GLint v1, GLint v2) = NULL;
static void (*proc_glProgramUniform3iv)(GLuint program, GLint location, GLsizei count, const GLint *value) = NULL;
static void (*proc_glProgramUniform3ui)(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2) = NULL;
static void (*proc_glProgramUniform3uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value) = NULL;
static void (*proc_glProgramUniform4d)(GLuint program, GLint location, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3) = NULL;
static void (*proc_glProgramUniform4dv)(GLuint program, GLint location, GLsizei count, const GLdouble *value) = NULL;
static void (*proc_glProgramUniform4f)(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) = NULL;
static void (*proc_glProgramUniform4fv)(GLuint program, GLint location, GLsizei count, const GLfloat *value) = NULL;
static void (*proc_glProgramUniform4i)(GLuint program, GLint location, GLint v0, GLint v1, GLint v2, GLint v3) = NULL;
static void (*proc_glProgramUniform4iv)(GLuint program, GLint location, GLsizei count, const GLint *value) = NULL;
static void (*proc_glProgramUniform4ui)(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) = NULL;
static void (*proc_glProgramUniform4uiv)(GLuint program, GLint location, GLsizei count, const GLuint *value) = NULL;
static void (*proc_glProgramUniformMatrix2dv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glProgramUniformMatrix2fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glProgramUniformMatrix2x3dv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glProgramUniformMatrix2x3fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glProgramUniformMatrix2x4dv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glProgramUniformMatrix2x4fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glProgramUniformMatrix3dv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glProgramUniformMatrix3fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glProgramUniformMatrix3x2dv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glProgramUniformMatrix3x2fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glProgramUniformMatrix3x4dv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glProgramUniformMatrix3x4fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glProgramUniformMatrix4dv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glProgramUniformMatrix4fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glProgramUniformMatrix4x2dv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glProgramUniformMatrix4x2fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glProgramUniformMatrix4x3dv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glProgramUniformMatrix4x3fv)(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glProvokingVertex)(GLenum mode) = NULL;
static void (*proc_glPushDebugGroup)(GLenum source, GLuint id, GLsizei length, const GLchar *message) = NULL;
static void (*proc_glQueryCounter)(GLuint id, GLenum target) = NULL;
static void (*proc_glReadBuffer)(GLenum src) = NULL;
static void (*proc_glReadPixels)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, void *pixels) = NULL;
static void (*proc_glReadnPixels)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data) = NULL;
static void (*proc_glReleaseShaderCompiler)() = NULL;
static void (*proc_glRenderbufferStorage)(GLenum target, GLenum internalformat, GLsizei width, GLsizei height) = NULL;
static void (*proc_glRenderbufferStorageMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height) = NULL;
static void (*proc_glResumeTransformFeedback)() = NULL;
static void (*proc_glSampleCoverage)(GLfloat value, GLboolean invert) = NULL;
static void (*proc_glSampleMaski)(GLuint maskNumber, GLbitfield mask) = NULL;
static void (*proc_glSamplerParameterIiv)(GLuint sampler, GLenum pname, const GLint *param) = NULL;
static void (*proc_glSamplerParameterIuiv)(GLuint sampler, GLenum pname, const GLuint *param) = NULL;
static void (*proc_glSamplerParameterf)(GLuint sampler, GLenum pname, GLfloat param) = NULL;
static void (*proc_glSamplerParameterfv)(GLuint sampler, GLenum pname, const GLfloat *param) = NULL;
static void (*proc_glSamplerParameteri)(GLuint sampler, GLenum pname, GLint param) = NULL;
static void (*proc_glSamplerParameteriv)(GLuint sampler, GLenum pname, const GLint *param) = NULL;
static void (*proc_glScissor)(GLint x, GLint y, GLsizei width, GLsizei height) = NULL;
static void (*proc_glScissorArrayv)(GLuint first, GLsizei count, const GLint *v) = NULL;
static void (*proc_glScissorIndexed)(GLuint index, GLint left, GLint bottom, GLsizei width, GLsizei height) = NULL;
static void (*proc_glScissorIndexedv)(GLuint index, const GLint *v) = NULL;
static void (*proc_glShaderBinary)(GLsizei count, const GLuint *shaders, GLenum binaryFormat, const void *binary, GLsizei length) = NULL;
static void (*proc_glShaderSource)(GLuint shader, GLsizei count, const GLchar *const *string, const GLint *length) = NULL;
static void (*proc_glShaderStorageBlockBinding)(GLuint program, GLuint storageBlockIndex, GLuint storageBlockBinding) = NULL;
static void (*proc_glStencilFunc)(GLenum func, GLint ref, GLuint mask) = NULL;
static void (*proc_glStencilFuncSeparate)(GLenum face, GLenum func, GLint ref, GLuint mask) = NULL;
static void (*proc_glStencilMask)(GLuint mask) = NULL;
static void (*proc_glStencilMaskSeparate)(GLenum face, GLuint mask) = NULL;
static void (*proc_glStencilOp)(GLenum fail, GLenum zfail, GLenum zpass) = NULL;
static void (*proc_glStencilOpSeparate)(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass) = NULL;
static void (*proc_glTexBuffer)(GLenum target, GLenum internalformat, GLuint buffer) = NULL;
static void (*proc_glTexBufferRange)(GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size) = NULL;
static void (*proc_glTexImage1D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type, const void *pixels) = NULL;
static void (*proc_glTexImage2D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels) = NULL;
static void (*proc_glTexImage2DMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations) = NULL;
static void (*proc_glTexImage3D)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels) = NULL;
static void (*proc_glTexImage3DMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations) = NULL;
static void (*proc_glTexParameterIiv)(GLenum target, GLenum pname, const GLint *params) = NULL;
static void (*proc_glTexParameterIuiv)(GLenum target, GLenum pname, const GLuint *params) = NULL;
static void (*proc_glTexParameterf)(GLenum target, GLenum pname, GLfloat param) = NULL;
static void (*proc_glTexParameterfv)(GLenum target, GLenum pname, const GLfloat *params) = NULL;
static void (*proc_glTexParameteri)(GLenum target, GLenum pname, GLint param) = NULL;
static void (*proc_glTexParameteriv)(GLenum target, GLenum pname, const GLint *params) = NULL;
static void (*proc_glTexStorage1D)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width) = NULL;
static void (*proc_glTexStorage2D)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height) = NULL;
static void (*proc_glTexStorage2DMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations) = NULL;
static void (*proc_glTexStorage3D)(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth) = NULL;
static void (*proc_glTexStorage3DMultisample)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations) = NULL;
static void (*proc_glTexSubImage1D)(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const void *pixels) = NULL;
static void (*proc_glTexSubImage2D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels) = NULL;
static void (*proc_glTexSubImage3D)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels) = NULL;
static void (*proc_glTextureBarrier)() = NULL;
static void (*proc_glTextureBuffer)(GLuint texture, GLenum internalformat, GLuint buffer) = NULL;
static void (*proc_glTextureBufferRange)(GLuint texture, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size) = NULL;
static void (*proc_glTextureParameterIiv)(GLuint texture, GLenum pname, const GLint *params) = NULL;
static void (*proc_glTextureParameterIuiv)(GLuint texture, GLenum pname, const GLuint *params) = NULL;
static void (*proc_glTextureParameterf)(GLuint texture, GLenum pname, GLfloat param) = NULL;
static void (*proc_glTextureParameterfv)(GLuint texture, GLenum pname, const GLfloat *param) = NULL;
static void (*proc_glTextureParameteri)(GLuint texture, GLenum pname, GLint param) = NULL;
static void (*proc_glTextureParameteriv)(GLuint texture, GLenum pname, const GLint *param) = NULL;
static void (*proc_glTextureStorage1D)(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width) = NULL;
static void (*proc_glTextureStorage2D)(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height) = NULL;
static void (*proc_glTextureStorage2DMultisample)(GLuint texture, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations) = NULL;
static void (*proc_glTextureStorage3D)(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth) = NULL;
static void (*proc_glTextureStorage3DMultisample)(GLuint texture, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations) = NULL;
static void (*proc_glTextureSubImage1D)(GLuint texture, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const void *pixels) = NULL;
static void (*proc_glTextureSubImage2D)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels) = NULL;
static void (*proc_glTextureSubImage3D)(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels) = NULL;
static void (*proc_glTextureView)(GLuint texture, GLenum target, GLuint origtexture, GLenum internalformat, GLuint minlevel, GLuint numlevels, GLuint minlayer, GLuint numlayers) = NULL;
static void (*proc_glTransformFeedbackBufferBase)(GLuint xfb, GLuint index, GLuint buffer) = NULL;
static void (*proc_glTransformFeedbackBufferRange)(GLuint xfb, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size) = NULL;
static void (*proc_glTransformFeedbackVaryings)(GLuint program, GLsizei count, const GLchar *const *varyings, GLenum bufferMode) = NULL;
static void (*proc_glUniform1d)(GLint location, GLdouble x) = NULL;
static void (*proc_glUniform1dv)(GLint location, GLsizei count, const GLdouble *value) = NULL;
static void (*proc_glUniform1f)(GLint location, GLfloat v0) = NULL;
static void (*proc_glUniform1fv)(GLint location, GLsizei count, const GLfloat *value) = NULL;
static void (*proc_glUniform1i)(GLint location, GLint v0) = NULL;
static void (*proc_glUniform1iv)(GLint location, GLsizei count, const GLint *value) = NULL;
static void (*proc_glUniform1ui)(GLint location, GLuint v0) = NULL;
static void (*proc_glUniform1uiv)(GLint location, GLsizei count, const GLuint *value) = NULL;
static void (*proc_glUniform2d)(GLint location, GLdouble x, GLdouble y) = NULL;
static void (*proc_glUniform2dv)(GLint location, GLsizei count, const GLdouble *value) = NULL;
static void (*proc_glUniform2f)(GLint location, GLfloat v0, GLfloat v1) = NULL;
static void (*proc_glUniform2fv)(GLint location, GLsizei count, const GLfloat *value) = NULL;
static void (*proc_glUniform2i)(GLint location, GLint v0, GLint v1) = NULL;
static void (*proc_glUniform2iv)(GLint location, GLsizei count, const GLint *value) = NULL;
static void (*proc_glUniform2ui)(GLint location, GLuint v0, GLuint v1) = NULL;
static void (*proc_glUniform2uiv)(GLint location, GLsizei count, const GLuint *value) = NULL;
static void (*proc_glUniform3d)(GLint location, GLdouble x, GLdouble y, GLdouble z) = NULL;
static void (*proc_glUniform3dv)(GLint location, GLsizei count, const GLdouble *value) = NULL;
static void (*proc_glUniform3f)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2) = NULL;
static void (*proc_glUniform3fv)(GLint location, GLsizei count, const GLfloat *value) = NULL;
static void (*proc_glUniform3i)(GLint location, GLint v0, GLint v1, GLint v2) = NULL;
static void (*proc_glUniform3iv)(GLint location, GLsizei count, const GLint *value) = NULL;
static void (*proc_glUniform3ui)(GLint location, GLuint v0, GLuint v1, GLuint v2) = NULL;
static void (*proc_glUniform3uiv)(GLint location, GLsizei count, const GLuint *value) = NULL;
static void (*proc_glUniform4d)(GLint location, GLdouble x, GLdouble y, GLdouble z, GLdouble w) = NULL;
static void (*proc_glUniform4dv)(GLint location, GLsizei count, const GLdouble *value) = NULL;
static void (*proc_glUniform4f)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) = NULL;
static void (*proc_glUniform4fv)(GLint location, GLsizei count, const GLfloat *value) = NULL;
static void (*proc_glUniform4i)(GLint location, GLint v0, GLint v1, GLint v2, GLint v3) = NULL;
static void (*proc_glUniform4iv)(GLint location, GLsizei count, const GLint *value) = NULL;
static void (*proc_glUniform4ui)(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) = NULL;
static void (*proc_glUniform4uiv)(GLint location, GLsizei count, const GLuint *value) = NULL;
static void (*proc_glUniformBlockBinding)(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding) = NULL;
static void (*proc_glUniformMatrix2dv)(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glUniformMatrix2fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glUniformMatrix2x3dv)(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glUniformMatrix2x3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glUniformMatrix2x4dv)(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glUniformMatrix2x4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glUniformMatrix3dv)(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glUniformMatrix3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glUniformMatrix3x2dv)(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glUniformMatrix3x2fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glUniformMatrix3x4dv)(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glUniformMatrix3x4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glUniformMatrix4dv)(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glUniformMatrix4fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glUniformMatrix4x2dv)(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glUniformMatrix4x2fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glUniformMatrix4x3dv)(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) = NULL;
static void (*proc_glUniformMatrix4x3fv)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) = NULL;
static void (*proc_glUniformSubroutinesuiv)(GLenum shadertype, GLsizei count, const GLuint *indices) = NULL;
static GLboolean (*proc_glUnmapBuffer)(GLenum target) = NULL;
static GLboolean (*proc_glUnmapNamedBuffer)(GLuint buffer) = NULL;
static void (*proc_glUseProgram)(GLuint program) = NULL;
static void (*proc_glUseProgramStages)(GLuint pipeline, GLbitfield stages, GLuint program) = NULL;
static void (*proc_glValidateProgram)(GLuint program) = NULL;
static void (*proc_glValidateProgramPipeline)(GLuint pipeline) = NULL;
static void (*proc_glVertexArrayAttribBinding)(GLuint vaobj, GLuint attribindex, GLuint bindingindex) = NULL;
static void (*proc_glVertexArrayAttribFormat)(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset) = NULL;
static void (*proc_glVertexArrayAttribIFormat)(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) = NULL;
static void (*proc_glVertexArrayAttribLFormat)(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) = NULL;
static void (*proc_glVertexArrayBindingDivisor)(GLuint vaobj, GLuint bindingindex, GLuint divisor) = NULL;
static void (*proc_glVertexArrayElementBuffer)(GLuint vaobj, GLuint buffer) = NULL;
static void (*proc_glVertexArrayVertexBuffer)(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride) = NULL;
static void (*proc_glVertexArrayVertexBuffers)(GLuint vaobj, GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizei *strides) = NULL;
static void (*proc_glVertexAttrib1d)(GLuint index, GLdouble x) = NULL;
static void (*proc_glVertexAttrib1dv)(GLuint index, const GLdouble *v) = NULL;
static void (*proc_glVertexAttrib1f)(GLuint index, GLfloat x) = NULL;
static void (*proc_glVertexAttrib1fv)(GLuint index, const GLfloat *v) = NULL;
static void (*proc_glVertexAttrib1s)(GLuint index, GLshort x) = NULL;
static void (*proc_glVertexAttrib1sv)(GLuint index, const GLshort *v) = NULL;
static void (*proc_glVertexAttrib2d)(GLuint index, GLdouble x, GLdouble y) = NULL;
static void (*proc_glVertexAttrib2dv)(GLuint index, const GLdouble *v) = NULL;
static void (*proc_glVertexAttrib2f)(GLuint index, GLfloat x, GLfloat y) = NULL;
static void (*proc_glVertexAttrib2fv)(GLuint index, const GLfloat *v) = NULL;
static void (*proc_glVertexAttrib2s)(GLuint index, GLshort x, GLshort y) = NULL;
static void (*proc_glVertexAttrib2sv)(GLuint index, const GLshort *v) = NULL;
static void (*proc_glVertexAttrib3d)(GLuint index, GLdouble x, GLdouble y, GLdouble z) = NULL;
static void (*proc_glVertexAttrib3dv)(GLuint index, const GLdouble *v) = NULL;
static void (*proc_glVertexAttrib3f)(GLuint index, GLfloat x, GLfloat y, GLfloat z) = NULL;
static void (*proc_glVertexAttrib3fv)(GLuint index, const GLfloat *v) = NULL;
static void (*proc_glVertexAttrib3s)(GLuint index, GLshort x, GLshort y, GLshort z) = NULL;
static void (*proc_glVertexAttrib3sv)(GLuint index, const GLshort *v) = NULL;
static void (*proc_glVertexAttrib4Nbv)(GLuint index, const GLbyte *v) = NULL;
static void (*proc_glVertexAttrib4Niv)(GLuint index, const GLint *v) = NULL;
static void (*proc_glVertexAttrib4Nsv)(GLuint index, const GLshort *v) = NULL;
static void (*proc_glVertexAttrib4Nub)(GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w) = NULL;
static void (*proc_glVertexAttrib4Nubv)(GLuint index, const GLubyte *v) = NULL;
static void (*proc_glVertexAttrib4Nuiv)(GLuint index, const GLuint *v) = NULL;
static void (*proc_glVertexAttrib4Nusv)(GLuint index, const GLushort *v) = NULL;
static void (*proc_glVertexAttrib4bv)(GLuint index, const GLbyte *v) = NULL;
static void (*proc_glVertexAttrib4d)(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w) = NULL;
static void (*proc_glVertexAttrib4dv)(GLuint index, const GLdouble *v) = NULL;
static void (*proc_glVertexAttrib4f)(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w) = NULL;
static void (*proc_glVertexAttrib4fv)(GLuint index, const GLfloat *v) = NULL;
static void (*proc_glVertexAttrib4iv)(GLuint index, const GLint *v) = NULL;
static void (*proc_glVertexAttrib4s)(GLuint index, GLshort x, GLshort y, GLshort z, GLshort w) = NULL;
static void (*proc_glVertexAttrib4sv)(GLuint index, const GLshort *v) = NULL;
static void (*proc_glVertexAttrib4ubv)(GLuint index, const GLubyte *v) = NULL;
static void (*proc_glVertexAttrib4uiv)(GLuint index, const GLuint *v) = NULL;
static void (*proc_glVertexAttrib4usv)(GLuint index, const GLushort *v) = NULL;
static void (*proc_glVertexAttribBinding)(GLuint attribindex, GLuint bindingindex) = NULL;
static void (*proc_glVertexAttribDivisor)(GLuint index, GLuint divisor) = NULL;
static void (*proc_glVertexAttribFormat)(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset) = NULL;
static void (*proc_glVertexAttribI1i)(GLuint index, GLint x) = NULL;
static void (*proc_glVertexAttribI1iv)(GLuint index, const GLint *v) = NULL;
static void (*proc_glVertexAttribI1ui)(GLuint index, GLuint x) = NULL;
static void (*proc_glVertexAttribI1uiv)(GLuint index, const GLuint *v) = NULL;
static void (*proc_glVertexAttribI2i)(GLuint index, GLint x, GLint y) = NULL;
static void (*proc_glVertexAttribI2iv)(GLuint index, const GLint *v) = NULL;
static void (*proc_glVertexAttribI2ui)(GLuint index, GLuint x, GLuint y) = NULL;
static void (*proc_glVertexAttribI2uiv)(GLuint index, const GLuint *v) = NULL;
static void (*proc_glVertexAttribI3i)(GLuint index, GLint x, GLint y, GLint z) = NULL;
static void (*proc_glVertexAttribI3iv)(GLuint index, const GLint *v) = NULL;
static void (*proc_glVertexAttribI3ui)(GLuint index, GLuint x, GLuint y, GLuint z) = NULL;
static void (*proc_glVertexAttribI3uiv)(GLuint index, const GLuint *v) = NULL;
static void (*proc_glVertexAttribI4bv)(GLuint index, const GLbyte *v) = NULL;
static void (*proc_glVertexAttribI4i)(GLuint index, GLint x, GLint y, GLint z, GLint w) = NULL;
static void (*proc_glVertexAttribI4iv)(GLuint index, const GLint *v) = NULL;
static void (*proc_glVertexAttribI4sv)(GLuint index, const GLshort *v) = NULL;
static void (*proc_glVertexAttribI4ubv)(GLuint index, const GLubyte *v) = NULL;
static void (*proc_glVertexAttribI4ui)(GLuint index, GLuint x, GLuint y, GLuint z, GLuint w) = NULL;
static void (*proc_glVertexAttribI4uiv)(GLuint index, const GLuint *v) = NULL;
static void (*proc_glVertexAttribI4usv)(GLuint index, const GLushort *v) = NULL;
static void (*proc_glVertexAttribIFormat)(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) = NULL;
static void (*proc_glVertexAttribIPointer)(GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer) = NULL;
static void (*proc_glVertexAttribL1d)(GLuint index, GLdouble x) = NULL;
static void (*proc_glVertexAttribL1dv)(GLuint index, const GLdouble *v) = NULL;
static void (*proc_glVertexAttribL2d)(GLuint index, GLdouble x, GLdouble y) = NULL;
static void (*proc_glVertexAttribL2dv)(GLuint index, const GLdouble *v) = NULL;
static void (*proc_glVertexAttribL3d)(GLuint index, GLdouble x, GLdouble y, GLdouble z) = NULL;
static void (*proc_glVertexAttribL3dv)(GLuint index, const GLdouble *v) = NULL;
static void (*proc_glVertexAttribL4d)(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w) = NULL;
static void (*proc_glVertexAttribL4dv)(GLuint index, const GLdouble *v) = NULL;
static void (*proc_glVertexAttribLFormat)(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) = NULL;
static void (*proc_glVertexAttribLPointer)(GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer) = NULL;
static void (*proc_glVertexAttribP1ui)(GLuint index, GLenum type, GLboolean normalized, GLuint value) = NULL;
static void (*proc_glVertexAttribP1uiv)(GLuint index, GLenum type, GLboolean normalized, const GLuint *value) = NULL;
static void (*proc_glVertexAttribP2ui)(GLuint index, GLenum type, GLboolean normalized, GLuint value) = NULL;
static void (*proc_glVertexAttribP2uiv)(GLuint index, GLenum type, GLboolean normalized, const GLuint *value) = NULL;
static void (*proc_glVertexAttribP3ui)(GLuint index, GLenum type, GLboolean normalized, GLuint value) = NULL;
static void (*proc_glVertexAttribP3uiv)(GLuint index, GLenum type, GLboolean normalized, const GLuint *value) = NULL;
static void (*proc_glVertexAttribP4ui)(GLuint index, GLenum type, GLboolean normalized, GLuint value) = NULL;
static void (*proc_glVertexAttribP4uiv)(GLuint index, GLenum type, GLboolean normalized, const GLuint *value) = NULL;
static void (*proc_glVertexAttribPointer)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer) = NULL;
static void (*proc_glVertexBindingDivisor)(GLuint bindingindex, GLuint divisor) = NULL;
static void (*proc_glViewport)(GLint x, GLint y, GLsizei width, GLsizei height) = NULL;
static void (*proc_glViewportArrayv)(GLuint first, GLsizei count, const GLfloat *v) = NULL;
static void (*proc_glViewportIndexedf)(GLuint index, GLfloat x, GLfloat y, GLfloat w, GLfloat h) = NULL;
static void (*proc_glViewportIndexedfv)(GLuint index, const GLfloat *v) = NULL;
static void (*proc_glWaitSync)(GLsync sync, GLbitfield flags, GLuint64 timeout) = NULL;

extern "C" void glActiveShaderProgram(GLuint pipeline, GLuint program) { proc_glActiveShaderProgram(pipeline, program); }
extern "C" void glActiveTexture(GLenum texture) { proc_glActiveTexture(texture); }
extern "C" void glAttachShader(GLuint program, GLuint shader) { proc_glAttachShader(program, shader); }
extern "C" void glBeginConditionalRender(GLuint id, GLenum mode) { proc_glBeginConditionalRender(id, mode); }
extern "C" void glBeginQuery(GLenum target, GLuint id) { proc_glBeginQuery(target, id); }
extern "C" void glBeginQueryIndexed(GLenum target, GLuint index, GLuint id) { proc_glBeginQueryIndexed(target, index, id); }
extern "C" void glBeginTransformFeedback(GLenum primitiveMode) { proc_glBeginTransformFeedback(primitiveMode); }
extern "C" void glBindAttribLocation(GLuint program, GLuint index, const GLchar *name) { proc_glBindAttribLocation(program, index, name); }
extern "C" void glBindBuffer(GLenum target, GLuint buffer) { proc_glBindBuffer(target, buffer); }
extern "C" void glBindBufferBase(GLenum target, GLuint index, GLuint buffer) { proc_glBindBufferBase(target, index, buffer); }
extern "C" void glBindBufferRange(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size) { proc_glBindBufferRange(target, index, buffer, offset, size); }
extern "C" void glBindBuffersBase(GLenum target, GLuint first, GLsizei count, const GLuint *buffers) { proc_glBindBuffersBase(target, first, count, buffers); }
extern "C" void glBindBuffersRange(GLenum target, GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizeiptr *sizes) { proc_glBindBuffersRange(target, first, count, buffers, offsets, sizes); }
extern "C" void glBindFragDataLocation(GLuint program, GLuint color, const GLchar *name) { proc_glBindFragDataLocation(program, color, name); }
extern "C" void glBindFragDataLocationIndexed(GLuint program, GLuint colorNumber, GLuint index, const GLchar *name) { proc_glBindFragDataLocationIndexed(program, colorNumber, index, name); }
extern "C" void glBindFramebuffer(GLenum target, GLuint framebuffer) { proc_glBindFramebuffer(target, framebuffer); }
extern "C" void glBindImageTexture(GLuint unit, GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum access, GLenum format) { proc_glBindImageTexture(unit, texture, level, layered, layer, access, format); }
extern "C" void glBindImageTextures(GLuint first, GLsizei count, const GLuint *textures) { proc_glBindImageTextures(first, count, textures); }
extern "C" void glBindProgramPipeline(GLuint pipeline) { proc_glBindProgramPipeline(pipeline); }
extern "C" void glBindRenderbuffer(GLenum target, GLuint renderbuffer) { proc_glBindRenderbuffer(target, renderbuffer); }
extern "C" void glBindSampler(GLuint unit, GLuint sampler) { proc_glBindSampler(unit, sampler); }
extern "C" void glBindSamplers(GLuint first, GLsizei count, const GLuint *samplers) { proc_glBindSamplers(first, count, samplers); }
extern "C" void glBindTexture(GLenum target, GLuint texture) { proc_glBindTexture(target, texture); }
extern "C" void glBindTextureUnit(GLuint unit, GLuint texture) { proc_glBindTextureUnit(unit, texture); }
extern "C" void glBindTextures(GLuint first, GLsizei count, const GLuint *textures) { proc_glBindTextures(first, count, textures); }
extern "C" void glBindTransformFeedback(GLenum target, GLuint id) { proc_glBindTransformFeedback(target, id); }
extern "C" void glBindVertexArray(GLuint array) { proc_glBindVertexArray(array); }
extern "C" void glBindVertexBuffer(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride) { proc_glBindVertexBuffer(bindingindex, buffer, offset, stride); }
extern "C" void glBindVertexBuffers(GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizei *strides) { proc_glBindVertexBuffers(first, count, buffers, offsets, strides); }
extern "C" void glBlendColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) { proc_glBlendColor(red, green, blue, alpha); }
extern "C" void glBlendEquation(GLenum mode) { proc_glBlendEquation(mode); }
extern "C" void glBlendEquationSeparate(GLenum modeRGB, GLenum modeAlpha) { proc_glBlendEquationSeparate(modeRGB, modeAlpha); }
extern "C" void glBlendEquationSeparatei(GLuint buf, GLenum modeRGB, GLenum modeAlpha) { proc_glBlendEquationSeparatei(buf, modeRGB, modeAlpha); }
extern "C" void glBlendEquationi(GLuint buf, GLenum mode) { proc_glBlendEquationi(buf, mode); }
extern "C" void glBlendFunc(GLenum sfactor, GLenum dfactor) { proc_glBlendFunc(sfactor, dfactor); }
extern "C" void glBlendFuncSeparate(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha) { proc_glBlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha); }
extern "C" void glBlendFuncSeparatei(GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha) { proc_glBlendFuncSeparatei(buf, srcRGB, dstRGB, srcAlpha, dstAlpha); }
extern "C" void glBlendFunci(GLuint buf, GLenum src, GLenum dst) { proc_glBlendFunci(buf, src, dst); }
extern "C" void glBlitFramebuffer(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter) { proc_glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter); }
extern "C" void glBlitNamedFramebuffer(GLuint readFramebuffer, GLuint drawFramebuffer, GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter) { proc_glBlitNamedFramebuffer(readFramebuffer, drawFramebuffer, srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter); }
extern "C" void glBufferData(GLenum target, GLsizeiptr size, const void *data, GLenum usage) { proc_glBufferData(target, size, data, usage); }
extern "C" void glBufferStorage(GLenum target, GLsizeiptr size, const void *data, GLbitfield flags) { proc_glBufferStorage(target, size, data, flags); }
extern "C" void glBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const void *data) { proc_glBufferSubData(target, offset, size, data); }
extern "C" GLenum glCheckFramebufferStatus(GLenum target) { return proc_glCheckFramebufferStatus(target); }
extern "C" GLenum glCheckNamedFramebufferStatus(GLuint framebuffer, GLenum target) { return proc_glCheckNamedFramebufferStatus(framebuffer, target); }
extern "C" void glClampColor(GLenum target, GLenum clamp) { proc_glClampColor(target, clamp); }
extern "C" void glClear(GLbitfield mask) { proc_glClear(mask); }
extern "C" void glClearBufferData(GLenum target, GLenum internalformat, GLenum format, GLenum type, const void *data) { proc_glClearBufferData(target, internalformat, format, type, data); }
extern "C" void glClearBufferSubData(GLenum target, GLenum internalformat, GLintptr offset, GLsizeiptr size, GLenum format, GLenum type, const void *data) { proc_glClearBufferSubData(target, internalformat, offset, size, format, type, data); }
extern "C" void glClearBufferfi(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil) { proc_glClearBufferfi(buffer, drawbuffer, depth, stencil); }
extern "C" void glClearBufferfv(GLenum buffer, GLint drawbuffer, const GLfloat *value) { proc_glClearBufferfv(buffer, drawbuffer, value); }
extern "C" void glClearBufferiv(GLenum buffer, GLint drawbuffer, const GLint *value) { proc_glClearBufferiv(buffer, drawbuffer, value); }
extern "C" void glClearBufferuiv(GLenum buffer, GLint drawbuffer, const GLuint *value) { proc_glClearBufferuiv(buffer, drawbuffer, value); }
extern "C" void glClearColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) { proc_glClearColor(red, green, blue, alpha); }
extern "C" void glClearDepth(GLdouble depth) { proc_glClearDepth(depth); }
extern "C" void glClearDepthf(GLfloat d) { proc_glClearDepthf(d); }
extern "C" void glClearNamedBufferData(GLuint buffer, GLenum internalformat, GLenum format, GLenum type, const void *data) { proc_glClearNamedBufferData(buffer, internalformat, format, type, data); }
extern "C" void glClearNamedBufferSubData(GLuint buffer, GLenum internalformat, GLintptr offset, GLsizeiptr size, GLenum format, GLenum type, const void *data) { proc_glClearNamedBufferSubData(buffer, internalformat, offset, size, format, type, data); }
extern "C" void glClearNamedFramebufferfi(GLuint framebuffer, GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil) { proc_glClearNamedFramebufferfi(framebuffer, buffer, drawbuffer, depth, stencil); }
extern "C" void glClearNamedFramebufferfv(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLfloat *value) { proc_glClearNamedFramebufferfv(framebuffer, buffer, drawbuffer, value); }
extern "C" void glClearNamedFramebufferiv(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLint *value) { proc_glClearNamedFramebufferiv(framebuffer, buffer, drawbuffer, value); }
extern "C" void glClearNamedFramebufferuiv(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLuint *value) { proc_glClearNamedFramebufferuiv(framebuffer, buffer, drawbuffer, value); }
extern "C" void glClearStencil(GLint s) { proc_glClearStencil(s); }
extern "C" void glClearTexImage(GLuint texture, GLint level, GLenum format, GLenum type, const void *data) { proc_glClearTexImage(texture, level, format, type, data); }
extern "C" void glClearTexSubImage(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *data) { proc_glClearTexSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data); }
extern "C" GLenum glClientWaitSync(GLsync sync, GLbitfield flags, GLuint64 timeout) { return proc_glClientWaitSync(sync, flags, timeout); }
extern "C" void glClipControl(GLenum origin, GLenum depth) { proc_glClipControl(origin, depth); }
extern "C" void glColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha) { proc_glColorMask(red, green, blue, alpha); }
extern "C" void glColorMaski(GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a) { proc_glColorMaski(index, r, g, b, a); }
extern "C" void glCompileShader(GLuint shader) { proc_glCompileShader(shader); }
extern "C" void glCompressedTexImage1D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLint border, GLsizei imageSize, const void *data) { proc_glCompressedTexImage1D(target, level, internalformat, width, border, imageSize, data); }
extern "C" void glCompressedTexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void *data) { proc_glCompressedTexImage2D(target, level, internalformat, width, height, border, imageSize, data); }
extern "C" void glCompressedTexImage3D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data) { proc_glCompressedTexImage3D(target, level, internalformat, width, height, depth, border, imageSize, data); }
extern "C" void glCompressedTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, const void *data) { proc_glCompressedTexSubImage1D(target, level, xoffset, width, format, imageSize, data); }
extern "C" void glCompressedTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const void *data) { proc_glCompressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, imageSize, data); }
extern "C" void glCompressedTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data) { proc_glCompressedTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data); }
extern "C" void glCompressedTextureSubImage1D(GLuint texture, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, const void *data) { proc_glCompressedTextureSubImage1D(texture, level, xoffset, width, format, imageSize, data); }
extern "C" void glCompressedTextureSubImage2D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const void *data) { proc_glCompressedTextureSubImage2D(texture, level, xoffset, yoffset, width, height, format, imageSize, data); }
extern "C" void glCompressedTextureSubImage3D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data) { proc_glCompressedTextureSubImage3D(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data); }
extern "C" void glCopyBufferSubData(GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size) { proc_glCopyBufferSubData(readTarget, writeTarget, readOffset, writeOffset, size); }
extern "C" void glCopyImageSubData(GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth) { proc_glCopyImageSubData(srcName, srcTarget, srcLevel, srcX, srcY, srcZ, dstName, dstTarget, dstLevel, dstX, dstY, dstZ, srcWidth, srcHeight, srcDepth); }
extern "C" void glCopyNamedBufferSubData(GLuint readBuffer, GLuint writeBuffer, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size) { proc_glCopyNamedBufferSubData(readBuffer, writeBuffer, readOffset, writeOffset, size); }
extern "C" void glCopyTexImage1D(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLint border) { proc_glCopyTexImage1D(target, level, internalformat, x, y, width, border); }
extern "C" void glCopyTexImage2D(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border) { proc_glCopyTexImage2D(target, level, internalformat, x, y, width, height, border); }
extern "C" void glCopyTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width) { proc_glCopyTexSubImage1D(target, level, xoffset, x, y, width); }
extern "C" void glCopyTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height) { proc_glCopyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height); }
extern "C" void glCopyTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height) { proc_glCopyTexSubImage3D(target, level, xoffset, yoffset, zoffset, x, y, width, height); }
extern "C" void glCopyTextureSubImage1D(GLuint texture, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width) { proc_glCopyTextureSubImage1D(texture, level, xoffset, x, y, width); }
extern "C" void glCopyTextureSubImage2D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height) { proc_glCopyTextureSubImage2D(texture, level, xoffset, yoffset, x, y, width, height); }
extern "C" void glCopyTextureSubImage3D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height) { proc_glCopyTextureSubImage3D(texture, level, xoffset, yoffset, zoffset, x, y, width, height); }
extern "C" void glCreateBuffers(GLsizei n, GLuint *buffers) { proc_glCreateBuffers(n, buffers); }
extern "C" void glCreateFramebuffers(GLsizei n, GLuint *framebuffers) { proc_glCreateFramebuffers(n, framebuffers); }
extern "C" GLuint glCreateProgram() { return proc_glCreateProgram(); }
extern "C" void glCreateProgramPipelines(GLsizei n, GLuint *pipelines) { proc_glCreateProgramPipelines(n, pipelines); }
extern "C" void glCreateQueries(GLenum target, GLsizei n, GLuint *ids) { proc_glCreateQueries(target, n, ids); }
extern "C" void glCreateRenderbuffers(GLsizei n, GLuint *renderbuffers) { proc_glCreateRenderbuffers(n, renderbuffers); }
extern "C" void glCreateSamplers(GLsizei n, GLuint *samplers) { proc_glCreateSamplers(n, samplers); }
extern "C" GLuint glCreateShader(GLenum type) { return proc_glCreateShader(type); }
extern "C" GLuint glCreateShaderProgramv(GLenum type, GLsizei count, const GLchar *const *strings) { return proc_glCreateShaderProgramv(type, count, strings); }
extern "C" void glCreateTextures(GLenum target, GLsizei n, GLuint *textures) { proc_glCreateTextures(target, n, textures); }
extern "C" void glCreateTransformFeedbacks(GLsizei n, GLuint *ids) { proc_glCreateTransformFeedbacks(n, ids); }
extern "C" void glCreateVertexArrays(GLsizei n, GLuint *arrays) { proc_glCreateVertexArrays(n, arrays); }
extern "C" void glCullFace(GLenum mode) { proc_glCullFace(mode); }
extern "C" void glDebugMessageCallback(GLDEBUGPROC callback, const void *userParam) { proc_glDebugMessageCallback(callback, userParam); }
extern "C" void glDebugMessageControl(GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled) { proc_glDebugMessageControl(source, type, severity, count, ids, enabled); }
extern "C" void glDebugMessageInsert(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *buf) { proc_glDebugMessageInsert(source, type, id, severity, length, buf); }
extern "C" void glDeleteBuffers(GLsizei n, const GLuint *buffers) { proc_glDeleteBuffers(n, buffers); }
extern "C" void glDeleteFramebuffers(GLsizei n, const GLuint *framebuffers) { proc_glDeleteFramebuffers(n, framebuffers); }
extern "C" void glDeleteProgram(GLuint program) { proc_glDeleteProgram(program); }
extern "C" void glDeleteProgramPipelines(GLsizei n, const GLuint *pipelines) { proc_glDeleteProgramPipelines(n, pipelines); }
extern "C" void glDeleteQueries(GLsizei n, const GLuint *ids) { proc_glDeleteQueries(n, ids); }
extern "C" void glDeleteRenderbuffers(GLsizei n, const GLuint *renderbuffers) { proc_glDeleteRenderbuffers(n, renderbuffers); }
extern "C" void glDeleteSamplers(GLsizei count, const GLuint *samplers) { proc_glDeleteSamplers(count, samplers); }
extern "C" void glDeleteShader(GLuint shader) { proc_glDeleteShader(shader); }
extern "C" void glDeleteSync(GLsync sync) { proc_glDeleteSync(sync); }
extern "C" void glDeleteTextures(GLsizei n, const GLuint *textures) { proc_glDeleteTextures(n, textures); }
extern "C" void glDeleteTransformFeedbacks(GLsizei n, const GLuint *ids) { proc_glDeleteTransformFeedbacks(n, ids); }
extern "C" void glDeleteVertexArrays(GLsizei n, const GLuint *arrays) { proc_glDeleteVertexArrays(n, arrays); }
extern "C" void glDepthFunc(GLenum func) { proc_glDepthFunc(func); }
extern "C" void glDepthMask(GLboolean flag) { proc_glDepthMask(flag); }
extern "C" void glDepthRange(GLdouble n, GLdouble f) { proc_glDepthRange(n, f); }
extern "C" void glDepthRangeArrayv(GLuint first, GLsizei count, const GLdouble *v) { proc_glDepthRangeArrayv(first, count, v); }
extern "C" void glDepthRangeIndexed(GLuint index, GLdouble n, GLdouble f) { proc_glDepthRangeIndexed(index, n, f); }
extern "C" void glDepthRangef(GLfloat n, GLfloat f) { proc_glDepthRangef(n, f); }
extern "C" void glDetachShader(GLuint program, GLuint shader) { proc_glDetachShader(program, shader); }
extern "C" void glDisable(GLenum cap) { proc_glDisable(cap); }
extern "C" void glDisableVertexArrayAttrib(GLuint vaobj, GLuint index) { proc_glDisableVertexArrayAttrib(vaobj, index); }
extern "C" void glDisableVertexAttribArray(GLuint index) { proc_glDisableVertexAttribArray(index); }
extern "C" void glDisablei(GLenum target, GLuint index) { proc_glDisablei(target, index); }
extern "C" void glDispatchCompute(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z) { proc_glDispatchCompute(num_groups_x, num_groups_y, num_groups_z); }
extern "C" void glDispatchComputeIndirect(GLintptr indirect) { proc_glDispatchComputeIndirect(indirect); }
extern "C" void glDrawArrays(GLenum mode, GLint first, GLsizei count) { proc_glDrawArrays(mode, first, count); }
extern "C" void glDrawArraysIndirect(GLenum mode, const void *indirect) { proc_glDrawArraysIndirect(mode, indirect); }
extern "C" void glDrawArraysInstanced(GLenum mode, GLint first, GLsizei count, GLsizei instancecount) { proc_glDrawArraysInstanced(mode, first, count, instancecount); }
extern "C" void glDrawArraysInstancedBaseInstance(GLenum mode, GLint first, GLsizei count, GLsizei instancecount, GLuint baseinstance) { proc_glDrawArraysInstancedBaseInstance(mode, first, count, instancecount, baseinstance); }
extern "C" void glDrawBuffer(GLenum buf) { proc_glDrawBuffer(buf); }
extern "C" void glDrawBuffers(GLsizei n, const GLenum *bufs) { proc_glDrawBuffers(n, bufs); }
extern "C" void glDrawElements(GLenum mode, GLsizei count, GLenum type, const void *indices) { proc_glDrawElements(mode, count, type, indices); }
extern "C" void glDrawElementsBaseVertex(GLenum mode, GLsizei count, GLenum type, const void *indices, GLint basevertex) { proc_glDrawElementsBaseVertex(mode, count, type, indices, basevertex); }
extern "C" void glDrawElementsIndirect(GLenum mode, GLenum type, const void *indirect) { proc_glDrawElementsIndirect(mode, type, indirect); }
extern "C" void glDrawElementsInstanced(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount) { proc_glDrawElementsInstanced(mode, count, type, indices, instancecount); }
extern "C" void glDrawElementsInstancedBaseInstance(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLuint baseinstance) { proc_glDrawElementsInstancedBaseInstance(mode, count, type, indices, instancecount, baseinstance); }
extern "C" void glDrawElementsInstancedBaseVertex(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex) { proc_glDrawElementsInstancedBaseVertex(mode, count, type, indices, instancecount, basevertex); }
extern "C" void glDrawElementsInstancedBaseVertexBaseInstance(GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex, GLuint baseinstance) { proc_glDrawElementsInstancedBaseVertexBaseInstance(mode, count, type, indices, instancecount, basevertex, baseinstance); }
extern "C" void glDrawRangeElements(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices) { proc_glDrawRangeElements(mode, start, end, count, type, indices); }
extern "C" void glDrawRangeElementsBaseVertex(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, GLint basevertex) { proc_glDrawRangeElementsBaseVertex(mode, start, end, count, type, indices, basevertex); }
extern "C" void glDrawTransformFeedback(GLenum mode, GLuint id) { proc_glDrawTransformFeedback(mode, id); }
extern "C" void glDrawTransformFeedbackInstanced(GLenum mode, GLuint id, GLsizei instancecount) { proc_glDrawTransformFeedbackInstanced(mode, id, instancecount); }
extern "C" void glDrawTransformFeedbackStream(GLenum mode, GLuint id, GLuint stream) { proc_glDrawTransformFeedbackStream(mode, id, stream); }
extern "C" void glDrawTransformFeedbackStreamInstanced(GLenum mode, GLuint id, GLuint stream, GLsizei instancecount) { proc_glDrawTransformFeedbackStreamInstanced(mode, id, stream, instancecount); }
extern "C" void glEnable(GLenum cap) { proc_glEnable(cap); }
extern "C" void glEnableVertexArrayAttrib(GLuint vaobj, GLuint index) { proc_glEnableVertexArrayAttrib(vaobj, index); }
extern "C" void glEnableVertexAttribArray(GLuint index) { proc_glEnableVertexAttribArray(index); }
extern "C" void glEnablei(GLenum target, GLuint index) { proc_glEnablei(target, index); }
extern "C" void glEndConditionalRender() { proc_glEndConditionalRender(); }
extern "C" void glEndQuery(GLenum target) { proc_glEndQuery(target); }
extern "C" void glEndQueryIndexed(GLenum target, GLuint index) { proc_glEndQueryIndexed(target, index); }
extern "C" void glEndTransformFeedback() { proc_glEndTransformFeedback(); }
extern "C" GLsync glFenceSync(GLenum condition, GLbitfield flags) { return proc_glFenceSync(condition, flags); }
extern "C" void glFinish() { proc_glFinish(); }
extern "C" void glFlush() { proc_glFlush(); }
extern "C" void glFlushMappedBufferRange(GLenum target, GLintptr offset, GLsizeiptr length) { proc_glFlushMappedBufferRange(target, offset, length); }
extern "C" void glFlushMappedNamedBufferRange(GLuint buffer, GLintptr offset, GLsizeiptr length) { proc_glFlushMappedNamedBufferRange(buffer, offset, length); }
extern "C" void glFramebufferParameteri(GLenum target, GLenum pname, GLint param) { proc_glFramebufferParameteri(target, pname, param); }
extern "C" void glFramebufferRenderbuffer(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer) { proc_glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer); }
extern "C" void glFramebufferTexture(GLenum target, GLenum attachment, GLuint texture, GLint level) { proc_glFramebufferTexture(target, attachment, texture, level); }
extern "C" void glFramebufferTexture1D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) { proc_glFramebufferTexture1D(target, attachment, textarget, texture, level); }
extern "C" void glFramebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level) { proc_glFramebufferTexture2D(target, attachment, textarget, texture, level); }
extern "C" void glFramebufferTexture3D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLint zoffset) { proc_glFramebufferTexture3D(target, attachment, textarget, texture, level, zoffset); }
extern "C" void glFramebufferTextureLayer(GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer) { proc_glFramebufferTextureLayer(target, attachment, texture, level, layer); }
extern "C" void glFrontFace(GLenum mode) { proc_glFrontFace(mode); }
extern "C" void glGenBuffers(GLsizei n, GLuint *buffers) { proc_glGenBuffers(n, buffers); }
extern "C" void glGenFramebuffers(GLsizei n, GLuint *framebuffers) { proc_glGenFramebuffers(n, framebuffers); }
extern "C" void glGenProgramPipelines(GLsizei n, GLuint *pipelines) { proc_glGenProgramPipelines(n, pipelines); }
extern "C" void glGenQueries(GLsizei n, GLuint *ids) { proc_glGenQueries(n, ids); }
extern "C" void glGenRenderbuffers(GLsizei n, GLuint *renderbuffers) { proc_glGenRenderbuffers(n, renderbuffers); }
extern "C" void glGenSamplers(GLsizei count, GLuint *samplers) { proc_glGenSamplers(count, samplers); }
extern "C" void glGenTextures(GLsizei n, GLuint *textures) { proc_glGenTextures(n, textures); }
extern "C" void glGenTransformFeedbacks(GLsizei n, GLuint *ids) { proc_glGenTransformFeedbacks(n, ids); }
extern "C" void glGenVertexArrays(GLsizei n, GLuint *arrays) { proc_glGenVertexArrays(n, arrays); }
extern "C" void glGenerateMipmap(GLenum target) { proc_glGenerateMipmap(target); }
extern "C" void glGenerateTextureMipmap(GLuint texture) { proc_glGenerateTextureMipmap(texture); }
extern "C" void glGetActiveAtomicCounterBufferiv(GLuint program, GLuint bufferIndex, GLenum pname, GLint *params) { proc_glGetActiveAtomicCounterBufferiv(program, bufferIndex, pname, params); }
extern "C" void glGetActiveAttrib(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) { proc_glGetActiveAttrib(program, index, bufSize, length, size, type, name); }
extern "C" void glGetActiveSubroutineName(GLuint program, GLenum shadertype, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name) { proc_glGetActiveSubroutineName(program, shadertype, index, bufSize, length, name); }
extern "C" void glGetActiveSubroutineUniformName(GLuint program, GLenum shadertype, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name) { proc_glGetActiveSubroutineUniformName(program, shadertype, index, bufSize, length, name); }
extern "C" void glGetActiveSubroutineUniformiv(GLuint program, GLenum shadertype, GLuint index, GLenum pname, GLint *values) { proc_glGetActiveSubroutineUniformiv(program, shadertype, index, pname, values); }
extern "C" void glGetActiveUniform(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) { proc_glGetActiveUniform(program, index, bufSize, length, size, type, name); }
extern "C" void glGetActiveUniformBlockName(GLuint program, GLuint uniformBlockIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformBlockName) { proc_glGetActiveUniformBlockName(program, uniformBlockIndex, bufSize, length, uniformBlockName); }
extern "C" void glGetActiveUniformBlockiv(GLuint program, GLuint uniformBlockIndex, GLenum pname, GLint *params) { proc_glGetActiveUniformBlockiv(program, uniformBlockIndex, pname, params); }
extern "C" void glGetActiveUniformName(GLuint program, GLuint uniformIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformName) { proc_glGetActiveUniformName(program, uniformIndex, bufSize, length, uniformName); }
extern "C" void glGetActiveUniformsiv(GLuint program, GLsizei uniformCount, const GLuint *uniformIndices, GLenum pname, GLint *params) { proc_glGetActiveUniformsiv(program, uniformCount, uniformIndices, pname, params); }
extern "C" void glGetAttachedShaders(GLuint program, GLsizei maxCount, GLsizei *count, GLuint *shaders) { proc_glGetAttachedShaders(program, maxCount, count, shaders); }
extern "C" GLint glGetAttribLocation(GLuint program, const GLchar *name) { return proc_glGetAttribLocation(program, name); }
extern "C" void glGetBooleani_v(GLenum target, GLuint index, GLboolean *data) { proc_glGetBooleani_v(target, index, data); }
extern "C" void glGetBooleanv(GLenum pname, GLboolean *data) { proc_glGetBooleanv(pname, data); }
extern "C" void glGetBufferParameteri64v(GLenum target, GLenum pname, GLint64 *params) { proc_glGetBufferParameteri64v(target, pname, params); }
extern "C" void glGetBufferParameteriv(GLenum target, GLenum pname, GLint *params) { proc_glGetBufferParameteriv(target, pname, params); }
extern "C" void glGetBufferPointerv(GLenum target, GLenum pname, void **params) { proc_glGetBufferPointerv(target, pname, params); }
extern "C" void glGetBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, void *data) { proc_glGetBufferSubData(target, offset, size, data); }
extern "C" void glGetCompressedTexImage(GLenum target, GLint level, void *img) { proc_glGetCompressedTexImage(target, level, img); }
extern "C" void glGetCompressedTextureImage(GLuint texture, GLint level, GLsizei bufSize, void *pixels) { proc_glGetCompressedTextureImage(texture, level, bufSize, pixels); }
extern "C" void glGetCompressedTextureSubImage(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLsizei bufSize, void *pixels) { proc_glGetCompressedTextureSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth, bufSize, pixels); }
extern "C" GLuint glGetDebugMessageLog(GLuint count, GLsizei bufSize, GLenum *sources, GLenum *types, GLuint *ids, GLenum *severities, GLsizei *lengths, GLchar *messageLog) { return proc_glGetDebugMessageLog(count, bufSize, sources, types, ids, severities, lengths, messageLog); }
extern "C" void glGetDoublei_v(GLenum target, GLuint index, GLdouble *data) { proc_glGetDoublei_v(target, index, data); }
extern "C" void glGetDoublev(GLenum pname, GLdouble *data) { proc_glGetDoublev(pname, data); }
extern "C" GLenum glGetError() { return proc_glGetError(); }
extern "C" void glGetFloati_v(GLenum target, GLuint index, GLfloat *data) { proc_glGetFloati_v(target, index, data); }
extern "C" void glGetFloatv(GLenum pname, GLfloat *data) { proc_glGetFloatv(pname, data); }
extern "C" GLint glGetFragDataIndex(GLuint program, const GLchar *name) { return proc_glGetFragDataIndex(program, name); }
extern "C" GLint glGetFragDataLocation(GLuint program, const GLchar *name) { return proc_glGetFragDataLocation(program, name); }
extern "C" void glGetFramebufferAttachmentParameteriv(GLenum target, GLenum attachment, GLenum pname, GLint *params) { proc_glGetFramebufferAttachmentParameteriv(target, attachment, pname, params); }
extern "C" void glGetFramebufferParameteriv(GLenum target, GLenum pname, GLint *params) { proc_glGetFramebufferParameteriv(target, pname, params); }
extern "C" GLenum glGetGraphicsResetStatus() { return proc_glGetGraphicsResetStatus(); }
extern "C" void glGetInteger64i_v(GLenum target, GLuint index, GLint64 *data) { proc_glGetInteger64i_v(target, index, data); }
extern "C" void glGetInteger64v(GLenum pname, GLint64 *data) { proc_glGetInteger64v(pname, data); }
extern "C" void glGetIntegeri_v(GLenum target, GLuint index, GLint *data) { proc_glGetIntegeri_v(target, index, data); }
extern "C" void glGetIntegerv(GLenum pname, GLint *data) { proc_glGetIntegerv(pname, data); }
extern "C" void glGetInternalformati64v(GLenum target, GLenum internalformat, GLenum pname, GLsizei count, GLint64 *params) { proc_glGetInternalformati64v(target, internalformat, pname, count, params); }
extern "C" void glGetInternalformativ(GLenum target, GLenum internalformat, GLenum pname, GLsizei count, GLint *params) { proc_glGetInternalformativ(target, internalformat, pname, count, params); }
extern "C" void glGetMultisamplefv(GLenum pname, GLuint index, GLfloat *val) { proc_glGetMultisamplefv(pname, index, val); }
extern "C" void glGetNamedBufferParameteri64v(GLuint buffer, GLenum pname, GLint64 *params) { proc_glGetNamedBufferParameteri64v(buffer, pname, params); }
extern "C" void glGetNamedBufferParameteriv(GLuint buffer, GLenum pname, GLint *params) { proc_glGetNamedBufferParameteriv(buffer, pname, params); }
extern "C" void glGetNamedBufferPointerv(GLuint buffer, GLenum pname, void **params) { proc_glGetNamedBufferPointerv(buffer, pname, params); }
extern "C" void glGetNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, void *data) { proc_glGetNamedBufferSubData(buffer, offset, size, data); }
extern "C" void glGetNamedFramebufferAttachmentParameteriv(GLuint framebuffer, GLenum attachment, GLenum pname, GLint *params) { proc_glGetNamedFramebufferAttachmentParameteriv(framebuffer, attachment, pname, params); }
extern "C" void glGetNamedFramebufferParameteriv(GLuint framebuffer, GLenum pname, GLint *param) { proc_glGetNamedFramebufferParameteriv(framebuffer, pname, param); }
extern "C" void glGetNamedRenderbufferParameteriv(GLuint renderbuffer, GLenum pname, GLint *params) { proc_glGetNamedRenderbufferParameteriv(renderbuffer, pname, params); }
extern "C" void glGetObjectLabel(GLenum identifier, GLuint name, GLsizei bufSize, GLsizei *length, GLchar *label) { proc_glGetObjectLabel(identifier, name, bufSize, length, label); }
extern "C" void glGetObjectPtrLabel(const void *ptr, GLsizei bufSize, GLsizei *length, GLchar *label) { proc_glGetObjectPtrLabel(ptr, bufSize, length, label); }
extern "C" void glGetPointerv(GLenum pname, void **params) { proc_glGetPointerv(pname, params); }
extern "C" void glGetProgramBinary(GLuint program, GLsizei bufSize, GLsizei *length, GLenum *binaryFormat, void *binary) { proc_glGetProgramBinary(program, bufSize, length, binaryFormat, binary); }
extern "C" void glGetProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog) { proc_glGetProgramInfoLog(program, bufSize, length, infoLog); }
extern "C" void glGetProgramInterfaceiv(GLuint program, GLenum programInterface, GLenum pname, GLint *params) { proc_glGetProgramInterfaceiv(program, programInterface, pname, params); }
extern "C" void glGetProgramPipelineInfoLog(GLuint pipeline, GLsizei bufSize, GLsizei *length, GLchar *infoLog) { proc_glGetProgramPipelineInfoLog(pipeline, bufSize, length, infoLog); }
extern "C" void glGetProgramPipelineiv(GLuint pipeline, GLenum pname, GLint *params) { proc_glGetProgramPipelineiv(pipeline, pname, params); }
extern "C" GLuint glGetProgramResourceIndex(GLuint program, GLenum programInterface, const GLchar *name) { return proc_glGetProgramResourceIndex(program, programInterface, name); }
extern "C" GLint glGetProgramResourceLocation(GLuint program, GLenum programInterface, const GLchar *name) { return proc_glGetProgramResourceLocation(program, programInterface, name); }
extern "C" GLint glGetProgramResourceLocationIndex(GLuint program, GLenum programInterface, const GLchar *name) { return proc_glGetProgramResourceLocationIndex(program, programInterface, name); }
extern "C" void glGetProgramResourceName(GLuint program, GLenum programInterface, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name) { proc_glGetProgramResourceName(program, programInterface, index, bufSize, length, name); }
extern "C" void glGetProgramResourceiv(GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum *props, GLsizei count, GLsizei *length, GLint *params) { proc_glGetProgramResourceiv(program, programInterface, index, propCount, props, count, length, params); }
extern "C" void glGetProgramStageiv(GLuint program, GLenum shadertype, GLenum pname, GLint *values) { proc_glGetProgramStageiv(program, shadertype, pname, values); }
extern "C" void glGetProgramiv(GLuint program, GLenum pname, GLint *params) { proc_glGetProgramiv(program, pname, params); }
extern "C" void glGetQueryBufferObjecti64v(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) { proc_glGetQueryBufferObjecti64v(id, buffer, pname, offset); }
extern "C" void glGetQueryBufferObjectiv(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) { proc_glGetQueryBufferObjectiv(id, buffer, pname, offset); }
extern "C" void glGetQueryBufferObjectui64v(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) { proc_glGetQueryBufferObjectui64v(id, buffer, pname, offset); }
extern "C" void glGetQueryBufferObjectuiv(GLuint id, GLuint buffer, GLenum pname, GLintptr offset) { proc_glGetQueryBufferObjectuiv(id, buffer, pname, offset); }
extern "C" void glGetQueryIndexediv(GLenum target, GLuint index, GLenum pname, GLint *params) { proc_glGetQueryIndexediv(target, index, pname, params); }
extern "C" void glGetQueryObjecti64v(GLuint id, GLenum pname, GLint64 *params) { proc_glGetQueryObjecti64v(id, pname, params); }
extern "C" void glGetQueryObjectiv(GLuint id, GLenum pname, GLint *params) { proc_glGetQueryObjectiv(id, pname, params); }
extern "C" void glGetQueryObjectui64v(GLuint id, GLenum pname, GLuint64 *params) { proc_glGetQueryObjectui64v(id, pname, params); }
extern "C" void glGetQueryObjectuiv(GLuint id, GLenum pname, GLuint *params) { proc_glGetQueryObjectuiv(id, pname, params); }
extern "C" void glGetQueryiv(GLenum target, GLenum pname, GLint *params) { proc_glGetQueryiv(target, pname, params); }
extern "C" void glGetRenderbufferParameteriv(GLenum target, GLenum pname, GLint *params) { proc_glGetRenderbufferParameteriv(target, pname, params); }
extern "C" void glGetSamplerParameterIiv(GLuint sampler, GLenum pname, GLint *params) { proc_glGetSamplerParameterIiv(sampler, pname, params); }
extern "C" void glGetSamplerParameterIuiv(GLuint sampler, GLenum pname, GLuint *params) { proc_glGetSamplerParameterIuiv(sampler, pname, params); }
extern "C" void glGetSamplerParameterfv(GLuint sampler, GLenum pname, GLfloat *params) { proc_glGetSamplerParameterfv(sampler, pname, params); }
extern "C" void glGetSamplerParameteriv(GLuint sampler, GLenum pname, GLint *params) { proc_glGetSamplerParameteriv(sampler, pname, params); }
extern "C" void glGetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog) { proc_glGetShaderInfoLog(shader, bufSize, length, infoLog); }
extern "C" void glGetShaderPrecisionFormat(GLenum shadertype, GLenum precisiontype, GLint *range, GLint *precision) { proc_glGetShaderPrecisionFormat(shadertype, precisiontype, range, precision); }
extern "C" void glGetShaderSource(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *source) { proc_glGetShaderSource(shader, bufSize, length, source); }
extern "C" void glGetShaderiv(GLuint shader, GLenum pname, GLint *params) { proc_glGetShaderiv(shader, pname, params); }
extern "C" const GLubyte *glGetString(GLenum name) { return proc_glGetString(name); }
extern "C" const GLubyte *glGetStringi(GLenum name, GLuint index) { return proc_glGetStringi(name, index); }
extern "C" GLuint glGetSubroutineIndex(GLuint program, GLenum shadertype, const GLchar *name) { return proc_glGetSubroutineIndex(program, shadertype, name); }
extern "C" GLint glGetSubroutineUniformLocation(GLuint program, GLenum shadertype, const GLchar *name) { return proc_glGetSubroutineUniformLocation(program, shadertype, name); }
extern "C" void glGetSynciv(GLsync sync, GLenum pname, GLsizei count, GLsizei *length, GLint *values) { proc_glGetSynciv(sync, pname, count, length, values); }
extern "C" void glGetTexImage(GLenum target, GLint level, GLenum format, GLenum type, void *pixels) { proc_glGetTexImage(target, level, format, type, pixels); }
extern "C" void glGetTexLevelParameterfv(GLenum target, GLint level, GLenum pname, GLfloat *params) { proc_glGetTexLevelParameterfv(target, level, pname, params); }
extern "C" void glGetTexLevelParameteriv(GLenum target, GLint level, GLenum pname, GLint *params) { proc_glGetTexLevelParameteriv(target, level, pname, params); }
extern "C" void glGetTexParameterIiv(GLenum target, GLenum pname, GLint *params) { proc_glGetTexParameterIiv(target, pname, params); }
extern "C" void glGetTexParameterIuiv(GLenum target, GLenum pname, GLuint *params) { proc_glGetTexParameterIuiv(target, pname, params); }
extern "C" void glGetTexParameterfv(GLenum target, GLenum pname, GLfloat *params) { proc_glGetTexParameterfv(target, pname, params); }
extern "C" void glGetTexParameteriv(GLenum target, GLenum pname, GLint *params) { proc_glGetTexParameteriv(target, pname, params); }
extern "C" void glGetTextureImage(GLuint texture, GLint level, GLenum format, GLenum type, GLsizei bufSize, void *pixels) { proc_glGetTextureImage(texture, level, format, type, bufSize, pixels); }
extern "C" void glGetTextureLevelParameterfv(GLuint texture, GLint level, GLenum pname, GLfloat *params) { proc_glGetTextureLevelParameterfv(texture, level, pname, params); }
extern "C" void glGetTextureLevelParameteriv(GLuint texture, GLint level, GLenum pname, GLint *params) { proc_glGetTextureLevelParameteriv(texture, level, pname, params); }
extern "C" void glGetTextureParameterIiv(GLuint texture, GLenum pname, GLint *params) { proc_glGetTextureParameterIiv(texture, pname, params); }
extern "C" void glGetTextureParameterIuiv(GLuint texture, GLenum pname, GLuint *params) { proc_glGetTextureParameterIuiv(texture, pname, params); }
extern "C" void glGetTextureParameterfv(GLuint texture, GLenum pname, GLfloat *params) { proc_glGetTextureParameterfv(texture, pname, params); }
extern "C" void glGetTextureParameteriv(GLuint texture, GLenum pname, GLint *params) { proc_glGetTextureParameteriv(texture, pname, params); }
extern "C" void glGetTextureSubImage(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, GLsizei bufSize, void *pixels) { proc_glGetTextureSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, bufSize, pixels); }
extern "C" void glGetTransformFeedbackVarying(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLsizei *size, GLenum *type, GLchar *name) { proc_glGetTransformFeedbackVarying(program, index, bufSize, length, size, type, name); }
extern "C" void glGetTransformFeedbacki64_v(GLuint xfb, GLenum pname, GLuint index, GLint64 *param) { proc_glGetTransformFeedbacki64_v(xfb, pname, index, param); }
extern "C" void glGetTransformFeedbacki_v(GLuint xfb, GLenum pname, GLuint index, GLint *param) { proc_glGetTransformFeedbacki_v(xfb, pname, index, param); }
extern "C" void glGetTransformFeedbackiv(GLuint xfb, GLenum pname, GLint *param) { proc_glGetTransformFeedbackiv(xfb, pname, param); }
extern "C" GLuint glGetUniformBlockIndex(GLuint program, const GLchar *uniformBlockName) { return proc_glGetUniformBlockIndex(program, uniformBlockName); }
extern "C" void glGetUniformIndices(GLuint program, GLsizei uniformCount, const GLchar *const *uniformNames, GLuint *uniformIndices) { proc_glGetUniformIndices(program, uniformCount, uniformNames, uniformIndices); }
extern "C" GLint glGetUniformLocation(GLuint program, const GLchar *name) { return proc_glGetUniformLocation(program, name); }
extern "C" void glGetUniformSubroutineuiv(GLenum shadertype, GLint location, GLuint *params) { proc_glGetUniformSubroutineuiv(shadertype, location, params); }
extern "C" void glGetUniformdv(GLuint program, GLint location, GLdouble *params) { proc_glGetUniformdv(program, location, params); }
extern "C" void glGetUniformfv(GLuint program, GLint location, GLfloat *params) { proc_glGetUniformfv(program, location, params); }
extern "C" void glGetUniformiv(GLuint program, GLint location, GLint *params) { proc_glGetUniformiv(program, location, params); }
extern "C" void glGetUniformuiv(GLuint program, GLint location, GLuint *params) { proc_glGetUniformuiv(program, location, params); }
extern "C" void glGetVertexArrayIndexed64iv(GLuint vaobj, GLuint index, GLenum pname, GLint64 *param) { proc_glGetVertexArrayIndexed64iv(vaobj, index, pname, param); }
extern "C" void glGetVertexArrayIndexediv(GLuint vaobj, GLuint index, GLenum pname, GLint *param) { proc_glGetVertexArrayIndexediv(vaobj, index, pname, param); }
extern "C" void glGetVertexArrayiv(GLuint vaobj, GLenum pname, GLint *param) { proc_glGetVertexArrayiv(vaobj, pname, param); }
extern "C" void glGetVertexAttribIiv(GLuint index, GLenum pname, GLint *params) { proc_glGetVertexAttribIiv(index, pname, params); }
extern "C" void glGetVertexAttribIuiv(GLuint index, GLenum pname, GLuint *params) { proc_glGetVertexAttribIuiv(index, pname, params); }
extern "C" void glGetVertexAttribLdv(GLuint index, GLenum pname, GLdouble *params) { proc_glGetVertexAttribLdv(index, pname, params); }
extern "C" void glGetVertexAttribPointerv(GLuint index, GLenum pname, void **pointer) { proc_glGetVertexAttribPointerv(index, pname, pointer); }
extern "C" void glGetVertexAttribdv(GLuint index, GLenum pname, GLdouble *params) { proc_glGetVertexAttribdv(index, pname, params); }
extern "C" void glGetVertexAttribfv(GLuint index, GLenum pname, GLfloat *params) { proc_glGetVertexAttribfv(index, pname, params); }
extern "C" void glGetVertexAttribiv(GLuint index, GLenum pname, GLint *params) { proc_glGetVertexAttribiv(index, pname, params); }
extern "C" void glGetnCompressedTexImage(GLenum target, GLint lod, GLsizei bufSize, void *pixels) { proc_glGetnCompressedTexImage(target, lod, bufSize, pixels); }
extern "C" void glGetnTexImage(GLenum target, GLint level, GLenum format, GLenum type, GLsizei bufSize, void *pixels) { proc_glGetnTexImage(target, level, format, type, bufSize, pixels); }
extern "C" void glGetnUniformdv(GLuint program, GLint location, GLsizei bufSize, GLdouble *params) { proc_glGetnUniformdv(program, location, bufSize, params); }
extern "C" void glGetnUniformfv(GLuint program, GLint location, GLsizei bufSize, GLfloat *params) { proc_glGetnUniformfv(program, location, bufSize, params); }
extern "C" void glGetnUniformiv(GLuint program, GLint location, GLsizei bufSize, GLint *params) { proc_glGetnUniformiv(program, location, bufSize, params); }
extern "C" void glGetnUniformuiv(GLuint program, GLint location, GLsizei bufSize, GLuint *params) { proc_glGetnUniformuiv(program, location, bufSize, params); }
extern "C" void glHint(GLenum target, GLenum mode) { proc_glHint(target, mode); }
extern "C" void glInvalidateBufferData(GLuint buffer) { proc_glInvalidateBufferData(buffer); }
extern "C" void glInvalidateBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr length) { proc_glInvalidateBufferSubData(buffer, offset, length); }
extern "C" void glInvalidateFramebuffer(GLenum target, GLsizei numAttachments, const GLenum *attachments) { proc_glInvalidateFramebuffer(target, numAttachments, attachments); }
extern "C" void glInvalidateNamedFramebufferData(GLuint framebuffer, GLsizei numAttachments, const GLenum *attachments) { proc_glInvalidateNamedFramebufferData(framebuffer, numAttachments, attachments); }
extern "C" void glInvalidateNamedFramebufferSubData(GLuint framebuffer, GLsizei numAttachments, const GLenum *attachments, GLint x, GLint y, GLsizei width, GLsizei height) { proc_glInvalidateNamedFramebufferSubData(framebuffer, numAttachments, attachments, x, y, width, height); }
extern "C" void glInvalidateSubFramebuffer(GLenum target, GLsizei numAttachments, const GLenum *attachments, GLint x, GLint y, GLsizei width, GLsizei height) { proc_glInvalidateSubFramebuffer(target, numAttachments, attachments, x, y, width, height); }
extern "C" void glInvalidateTexImage(GLuint texture, GLint level) { proc_glInvalidateTexImage(texture, level); }
extern "C" void glInvalidateTexSubImage(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth) { proc_glInvalidateTexSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth); }
extern "C" GLboolean glIsBuffer(GLuint buffer) { return proc_glIsBuffer(buffer); }
extern "C" GLboolean glIsEnabled(GLenum cap) { return proc_glIsEnabled(cap); }
extern "C" GLboolean glIsEnabledi(GLenum target, GLuint index) { return proc_glIsEnabledi(target, index); }
extern "C" GLboolean glIsFramebuffer(GLuint framebuffer) { return proc_glIsFramebuffer(framebuffer); }
extern "C" GLboolean glIsProgram(GLuint program) { return proc_glIsProgram(program); }
extern "C" GLboolean glIsProgramPipeline(GLuint pipeline) { return proc_glIsProgramPipeline(pipeline); }
extern "C" GLboolean glIsQuery(GLuint id) { return proc_glIsQuery(id); }
extern "C" GLboolean glIsRenderbuffer(GLuint renderbuffer) { return proc_glIsRenderbuffer(renderbuffer); }
extern "C" GLboolean glIsSampler(GLuint sampler) { return proc_glIsSampler(sampler); }
extern "C" GLboolean glIsShader(GLuint shader) { return proc_glIsShader(shader); }
extern "C" GLboolean glIsSync(GLsync sync) { return proc_glIsSync(sync); }
extern "C" GLboolean glIsTexture(GLuint texture) { return proc_glIsTexture(texture); }
extern "C" GLboolean glIsTransformFeedback(GLuint id) { return proc_glIsTransformFeedback(id); }
extern "C" GLboolean glIsVertexArray(GLuint array) { return proc_glIsVertexArray(array); }
extern "C" void glLineWidth(GLfloat width) { proc_glLineWidth(width); }
extern "C" void glLinkProgram(GLuint program) { proc_glLinkProgram(program); }
extern "C" void glLogicOp(GLenum opcode) { proc_glLogicOp(opcode); }
extern "C" void *glMapBuffer(GLenum target, GLenum access) { return proc_glMapBuffer(target, access); }
extern "C" void *glMapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access) { return proc_glMapBufferRange(target, offset, length, access); }
extern "C" void *glMapNamedBuffer(GLuint buffer, GLenum access) { return proc_glMapNamedBuffer(buffer, access); }
extern "C" void *glMapNamedBufferRange(GLuint buffer, GLintptr offset, GLsizeiptr length, GLbitfield access) { return proc_glMapNamedBufferRange(buffer, offset, length, access); }
extern "C" void glMemoryBarrier(GLbitfield barriers) { proc_glMemoryBarrier(barriers); }
extern "C" void glMemoryBarrierByRegion(GLbitfield barriers) { proc_glMemoryBarrierByRegion(barriers); }
extern "C" void glMinSampleShading(GLfloat value) { proc_glMinSampleShading(value); }
extern "C" void glMultiDrawArrays(GLenum mode, const GLint *first, const GLsizei *count, GLsizei drawcount) { proc_glMultiDrawArrays(mode, first, count, drawcount); }
extern "C" void glMultiDrawArraysIndirect(GLenum mode, const void *indirect, GLsizei drawcount, GLsizei stride) { proc_glMultiDrawArraysIndirect(mode, indirect, drawcount, stride); }
extern "C" void glMultiDrawElements(GLenum mode, const GLsizei *count, GLenum type, const void *const *indices, GLsizei drawcount) { proc_glMultiDrawElements(mode, count, type, indices, drawcount); }
extern "C" void glMultiDrawElementsBaseVertex(GLenum mode, const GLsizei *count, GLenum type, const void *const *indices, GLsizei drawcount, const GLint *basevertex) { proc_glMultiDrawElementsBaseVertex(mode, count, type, indices, drawcount, basevertex); }
extern "C" void glMultiDrawElementsIndirect(GLenum mode, GLenum type, const void *indirect, GLsizei drawcount, GLsizei stride) { proc_glMultiDrawElementsIndirect(mode, type, indirect, drawcount, stride); }
extern "C" void glNamedBufferData(GLuint buffer, GLsizeiptr size, const void *data, GLenum usage) { proc_glNamedBufferData(buffer, size, data, usage); }
extern "C" void glNamedBufferStorage(GLuint buffer, GLsizeiptr size, const void *data, GLbitfield flags) { proc_glNamedBufferStorage(buffer, size, data, flags); }
extern "C" void glNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr size, const void *data) { proc_glNamedBufferSubData(buffer, offset, size, data); }
extern "C" void glNamedFramebufferDrawBuffer(GLuint framebuffer, GLenum buf) { proc_glNamedFramebufferDrawBuffer(framebuffer, buf); }
extern "C" void glNamedFramebufferDrawBuffers(GLuint framebuffer, GLsizei n, const GLenum *bufs) { proc_glNamedFramebufferDrawBuffers(framebuffer, n, bufs); }
extern "C" void glNamedFramebufferParameteri(GLuint framebuffer, GLenum pname, GLint param) { proc_glNamedFramebufferParameteri(framebuffer, pname, param); }
extern "C" void glNamedFramebufferReadBuffer(GLuint framebuffer, GLenum src) { proc_glNamedFramebufferReadBuffer(framebuffer, src); }
extern "C" void glNamedFramebufferRenderbuffer(GLuint framebuffer, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer) { proc_glNamedFramebufferRenderbuffer(framebuffer, attachment, renderbuffertarget, renderbuffer); }
extern "C" void glNamedFramebufferTexture(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level) { proc_glNamedFramebufferTexture(framebuffer, attachment, texture, level); }
extern "C" void glNamedFramebufferTextureLayer(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level, GLint layer) { proc_glNamedFramebufferTextureLayer(framebuffer, attachment, texture, level, layer); }
extern "C" void glNamedRenderbufferStorage(GLuint renderbuffer, GLenum internalformat, GLsizei width, GLsizei height) { proc_glNamedRenderbufferStorage(renderbuffer, internalformat, width, height); }
extern "C" void glNamedRenderbufferStorageMultisample(GLuint renderbuffer, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height) { proc_glNamedRenderbufferStorageMultisample(renderbuffer, samples, internalformat, width, height); }
extern "C" void glObjectLabel(GLenum identifier, GLuint name, GLsizei length, const GLchar *label) { proc_glObjectLabel(identifier, name, length, label); }
extern "C" void glObjectPtrLabel(const void *ptr, GLsizei length, const GLchar *label) { proc_glObjectPtrLabel(ptr, length, label); }
extern "C" void glPatchParameterfv(GLenum pname, const GLfloat *values) { proc_glPatchParameterfv(pname, values); }
extern "C" void glPatchParameteri(GLenum pname, GLint value) { proc_glPatchParameteri(pname, value); }
extern "C" void glPauseTransformFeedback() { proc_glPauseTransformFeedback(); }
extern "C" void glPixelStoref(GLenum pname, GLfloat param) { proc_glPixelStoref(pname, param); }
extern "C" void glPixelStorei(GLenum pname, GLint param) { proc_glPixelStorei(pname, param); }
extern "C" void glPointParameterf(GLenum pname, GLfloat param) { proc_glPointParameterf(pname, param); }
extern "C" void glPointParameterfv(GLenum pname, const GLfloat *params) { proc_glPointParameterfv(pname, params); }
extern "C" void glPointParameteri(GLenum pname, GLint param) { proc_glPointParameteri(pname, param); }
extern "C" void glPointParameteriv(GLenum pname, const GLint *params) { proc_glPointParameteriv(pname, params); }
extern "C" void glPointSize(GLfloat size) { proc_glPointSize(size); }
extern "C" void glPolygonMode(GLenum face, GLenum mode) { proc_glPolygonMode(face, mode); }
extern "C" void glPolygonOffset(GLfloat factor, GLfloat units) { proc_glPolygonOffset(factor, units); }
extern "C" void glPopDebugGroup() { proc_glPopDebugGroup(); }
extern "C" void glPrimitiveRestartIndex(GLuint index) { proc_glPrimitiveRestartIndex(index); }
extern "C" void glProgramBinary(GLuint program, GLenum binaryFormat, const void *binary, GLsizei length) { proc_glProgramBinary(program, binaryFormat, binary, length); }
extern "C" void glProgramParameteri(GLuint program, GLenum pname, GLint value) { proc_glProgramParameteri(program, pname, value); }
extern "C" void glProgramUniform1d(GLuint program, GLint location, GLdouble v0) { proc_glProgramUniform1d(program, location, v0); }
extern "C" void glProgramUniform1dv(GLuint program, GLint location, GLsizei count, const GLdouble *value) { proc_glProgramUniform1dv(program, location, count, value); }
extern "C" void glProgramUniform1f(GLuint program, GLint location, GLfloat v0) { proc_glProgramUniform1f(program, location, v0); }
extern "C" void glProgramUniform1fv(GLuint program, GLint location, GLsizei count, const GLfloat *value) { proc_glProgramUniform1fv(program, location, count, value); }
extern "C" void glProgramUniform1i(GLuint program, GLint location, GLint v0) { proc_glProgramUniform1i(program, location, v0); }
extern "C" void glProgramUniform1iv(GLuint program, GLint location, GLsizei count, const GLint *value) { proc_glProgramUniform1iv(program, location, count, value); }
extern "C" void glProgramUniform1ui(GLuint program, GLint location, GLuint v0) { proc_glProgramUniform1ui(program, location, v0); }
extern "C" void glProgramUniform1uiv(GLuint program, GLint location, GLsizei count, const GLuint *value) { proc_glProgramUniform1uiv(program, location, count, value); }
extern "C" void glProgramUniform2d(GLuint program, GLint location, GLdouble v0, GLdouble v1) { proc_glProgramUniform2d(program, location, v0, v1); }
extern "C" void glProgramUniform2dv(GLuint program, GLint location, GLsizei count, const GLdouble *value) { proc_glProgramUniform2dv(program, location, count, value); }
extern "C" void glProgramUniform2f(GLuint program, GLint location, GLfloat v0, GLfloat v1) { proc_glProgramUniform2f(program, location, v0, v1); }
extern "C" void glProgramUniform2fv(GLuint program, GLint location, GLsizei count, const GLfloat *value) { proc_glProgramUniform2fv(program, location, count, value); }
extern "C" void glProgramUniform2i(GLuint program, GLint location, GLint v0, GLint v1) { proc_glProgramUniform2i(program, location, v0, v1); }
extern "C" void glProgramUniform2iv(GLuint program, GLint location, GLsizei count, const GLint *value) { proc_glProgramUniform2iv(program, location, count, value); }
extern "C" void glProgramUniform2ui(GLuint program, GLint location, GLuint v0, GLuint v1) { proc_glProgramUniform2ui(program, location, v0, v1); }
extern "C" void glProgramUniform2uiv(GLuint program, GLint location, GLsizei count, const GLuint *value) { proc_glProgramUniform2uiv(program, location, count, value); }
extern "C" void glProgramUniform3d(GLuint program, GLint location, GLdouble v0, GLdouble v1, GLdouble v2) { proc_glProgramUniform3d(program, location, v0, v1, v2); }
extern "C" void glProgramUniform3dv(GLuint program, GLint location, GLsizei count, const GLdouble *value) { proc_glProgramUniform3dv(program, location, count, value); }
extern "C" void glProgramUniform3f(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2) { proc_glProgramUniform3f(program, location, v0, v1, v2); }
extern "C" void glProgramUniform3fv(GLuint program, GLint location, GLsizei count, const GLfloat *value) { proc_glProgramUniform3fv(program, location, count, value); }
extern "C" void glProgramUniform3i(GLuint program, GLint location, GLint v0, GLint v1, GLint v2) { proc_glProgramUniform3i(program, location, v0, v1, v2); }
extern "C" void glProgramUniform3iv(GLuint program, GLint location, GLsizei count, const GLint *value) { proc_glProgramUniform3iv(program, location, count, value); }
extern "C" void glProgramUniform3ui(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2) { proc_glProgramUniform3ui(program, location, v0, v1, v2); }
extern "C" void glProgramUniform3uiv(GLuint program, GLint location, GLsizei count, const GLuint *value) { proc_glProgramUniform3uiv(program, location, count, value); }
extern "C" void glProgramUniform4d(GLuint program, GLint location, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3) { proc_glProgramUniform4d(program, location, v0, v1, v2, v3); }
extern "C" void glProgramUniform4dv(GLuint program, GLint location, GLsizei count, const GLdouble *value) { proc_glProgramUniform4dv(program, location, count, value); }
extern "C" void glProgramUniform4f(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) { proc_glProgramUniform4f(program, location, v0, v1, v2, v3); }
extern "C" void glProgramUniform4fv(GLuint program, GLint location, GLsizei count, const GLfloat *value) { proc_glProgramUniform4fv(program, location, count, value); }
extern "C" void glProgramUniform4i(GLuint program, GLint location, GLint v0, GLint v1, GLint v2, GLint v3) { proc_glProgramUniform4i(program, location, v0, v1, v2, v3); }
extern "C" void glProgramUniform4iv(GLuint program, GLint location, GLsizei count, const GLint *value) { proc_glProgramUniform4iv(program, location, count, value); }
extern "C" void glProgramUniform4ui(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) { proc_glProgramUniform4ui(program, location, v0, v1, v2, v3); }
extern "C" void glProgramUniform4uiv(GLuint program, GLint location, GLsizei count, const GLuint *value) { proc_glProgramUniform4uiv(program, location, count, value); }
extern "C" void glProgramUniformMatrix2dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glProgramUniformMatrix2dv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix2fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glProgramUniformMatrix2fv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix2x3dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glProgramUniformMatrix2x3dv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix2x3fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glProgramUniformMatrix2x3fv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix2x4dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glProgramUniformMatrix2x4dv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix2x4fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glProgramUniformMatrix2x4fv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix3dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glProgramUniformMatrix3dv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix3fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glProgramUniformMatrix3fv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix3x2dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glProgramUniformMatrix3x2dv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix3x2fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glProgramUniformMatrix3x2fv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix3x4dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glProgramUniformMatrix3x4dv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix3x4fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glProgramUniformMatrix3x4fv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix4dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glProgramUniformMatrix4dv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix4fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glProgramUniformMatrix4fv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix4x2dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glProgramUniformMatrix4x2dv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix4x2fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glProgramUniformMatrix4x2fv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix4x3dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glProgramUniformMatrix4x3dv(program, location, count, transpose, value); }
extern "C" void glProgramUniformMatrix4x3fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glProgramUniformMatrix4x3fv(program, location, count, transpose, value); }
extern "C" void glProvokingVertex(GLenum mode) { proc_glProvokingVertex(mode); }
extern "C" void glPushDebugGroup(GLenum source, GLuint id, GLsizei length, const GLchar *message) { proc_glPushDebugGroup(source, id, length, message); }
extern "C" void glQueryCounter(GLuint id, GLenum target) { proc_glQueryCounter(id, target); }
extern "C" void glReadBuffer(GLenum src) { proc_glReadBuffer(src); }
extern "C" void glReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, void *pixels) { proc_glReadPixels(x, y, width, height, format, type, pixels); }
extern "C" void glReadnPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data) { proc_glReadnPixels(x, y, width, height, format, type, bufSize, data); }
extern "C" void glReleaseShaderCompiler() { proc_glReleaseShaderCompiler(); }
extern "C" void glRenderbufferStorage(GLenum target, GLenum internalformat, GLsizei width, GLsizei height) { proc_glRenderbufferStorage(target, internalformat, width, height); }
extern "C" void glRenderbufferStorageMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height) { proc_glRenderbufferStorageMultisample(target, samples, internalformat, width, height); }
extern "C" void glResumeTransformFeedback() { proc_glResumeTransformFeedback(); }
extern "C" void glSampleCoverage(GLfloat value, GLboolean invert) { proc_glSampleCoverage(value, invert); }
extern "C" void glSampleMaski(GLuint maskNumber, GLbitfield mask) { proc_glSampleMaski(maskNumber, mask); }
extern "C" void glSamplerParameterIiv(GLuint sampler, GLenum pname, const GLint *param) { proc_glSamplerParameterIiv(sampler, pname, param); }
extern "C" void glSamplerParameterIuiv(GLuint sampler, GLenum pname, const GLuint *param) { proc_glSamplerParameterIuiv(sampler, pname, param); }
extern "C" void glSamplerParameterf(GLuint sampler, GLenum pname, GLfloat param) { proc_glSamplerParameterf(sampler, pname, param); }
extern "C" void glSamplerParameterfv(GLuint sampler, GLenum pname, const GLfloat *param) { proc_glSamplerParameterfv(sampler, pname, param); }
extern "C" void glSamplerParameteri(GLuint sampler, GLenum pname, GLint param) { proc_glSamplerParameteri(sampler, pname, param); }
extern "C" void glSamplerParameteriv(GLuint sampler, GLenum pname, const GLint *param) { proc_glSamplerParameteriv(sampler, pname, param); }
extern "C" void glScissor(GLint x, GLint y, GLsizei width, GLsizei height) { proc_glScissor(x, y, width, height); }
extern "C" void glScissorArrayv(GLuint first, GLsizei count, const GLint *v) { proc_glScissorArrayv(first, count, v); }
extern "C" void glScissorIndexed(GLuint index, GLint left, GLint bottom, GLsizei width, GLsizei height) { proc_glScissorIndexed(index, left, bottom, width, height); }
extern "C" void glScissorIndexedv(GLuint index, const GLint *v) { proc_glScissorIndexedv(index, v); }
extern "C" void glShaderBinary(GLsizei count, const GLuint *shaders, GLenum binaryFormat, const void *binary, GLsizei length) { proc_glShaderBinary(count, shaders, binaryFormat, binary, length); }
extern "C" void glShaderSource(GLuint shader, GLsizei count, const GLchar *const *string, const GLint *length) { proc_glShaderSource(shader, count, string, length); }
extern "C" void glShaderStorageBlockBinding(GLuint program, GLuint storageBlockIndex, GLuint storageBlockBinding) { proc_glShaderStorageBlockBinding(program, storageBlockIndex, storageBlockBinding); }
extern "C" void glStencilFunc(GLenum func, GLint ref, GLuint mask) { proc_glStencilFunc(func, ref, mask); }
extern "C" void glStencilFuncSeparate(GLenum face, GLenum func, GLint ref, GLuint mask) { proc_glStencilFuncSeparate(face, func, ref, mask); }
extern "C" void glStencilMask(GLuint mask) { proc_glStencilMask(mask); }
extern "C" void glStencilMaskSeparate(GLenum face, GLuint mask) { proc_glStencilMaskSeparate(face, mask); }
extern "C" void glStencilOp(GLenum fail, GLenum zfail, GLenum zpass) { proc_glStencilOp(fail, zfail, zpass); }
extern "C" void glStencilOpSeparate(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass) { proc_glStencilOpSeparate(face, sfail, dpfail, dppass); }
extern "C" void glTexBuffer(GLenum target, GLenum internalformat, GLuint buffer) { proc_glTexBuffer(target, internalformat, buffer); }
extern "C" void glTexBufferRange(GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size) { proc_glTexBufferRange(target, internalformat, buffer, offset, size); }
extern "C" void glTexImage1D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type, const void *pixels) { proc_glTexImage1D(target, level, internalformat, width, border, format, type, pixels); }
extern "C" void glTexImage2D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels) { proc_glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels); }
extern "C" void glTexImage2DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations) { proc_glTexImage2DMultisample(target, samples, internalformat, width, height, fixedsamplelocations); }
extern "C" void glTexImage3D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels) { proc_glTexImage3D(target, level, internalformat, width, height, depth, border, format, type, pixels); }
extern "C" void glTexImage3DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations) { proc_glTexImage3DMultisample(target, samples, internalformat, width, height, depth, fixedsamplelocations); }
extern "C" void glTexParameterIiv(GLenum target, GLenum pname, const GLint *params) { proc_glTexParameterIiv(target, pname, params); }
extern "C" void glTexParameterIuiv(GLenum target, GLenum pname, const GLuint *params) { proc_glTexParameterIuiv(target, pname, params); }
extern "C" void glTexParameterf(GLenum target, GLenum pname, GLfloat param) { proc_glTexParameterf(target, pname, param); }
extern "C" void glTexParameterfv(GLenum target, GLenum pname, const GLfloat *params) { proc_glTexParameterfv(target, pname, params); }
extern "C" void glTexParameteri(GLenum target, GLenum pname, GLint param) { proc_glTexParameteri(target, pname, param); }
extern "C" void glTexParameteriv(GLenum target, GLenum pname, const GLint *params) { proc_glTexParameteriv(target, pname, params); }
extern "C" void glTexStorage1D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width) { proc_glTexStorage1D(target, levels, internalformat, width); }
extern "C" void glTexStorage2D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height) { proc_glTexStorage2D(target, levels, internalformat, width, height); }
extern "C" void glTexStorage2DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations) { proc_glTexStorage2DMultisample(target, samples, internalformat, width, height, fixedsamplelocations); }
extern "C" void glTexStorage3D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth) { proc_glTexStorage3D(target, levels, internalformat, width, height, depth); }
extern "C" void glTexStorage3DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations) { proc_glTexStorage3DMultisample(target, samples, internalformat, width, height, depth, fixedsamplelocations); }
extern "C" void glTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const void *pixels) { proc_glTexSubImage1D(target, level, xoffset, width, format, type, pixels); }
extern "C" void glTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels) { proc_glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels); }
extern "C" void glTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels) { proc_glTexSubImage3D(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels); }
extern "C" void glTextureBarrier() { proc_glTextureBarrier(); }
extern "C" void glTextureBuffer(GLuint texture, GLenum internalformat, GLuint buffer) { proc_glTextureBuffer(texture, internalformat, buffer); }
extern "C" void glTextureBufferRange(GLuint texture, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size) { proc_glTextureBufferRange(texture, internalformat, buffer, offset, size); }
extern "C" void glTextureParameterIiv(GLuint texture, GLenum pname, const GLint *params) { proc_glTextureParameterIiv(texture, pname, params); }
extern "C" void glTextureParameterIuiv(GLuint texture, GLenum pname, const GLuint *params) { proc_glTextureParameterIuiv(texture, pname, params); }
extern "C" void glTextureParameterf(GLuint texture, GLenum pname, GLfloat param) { proc_glTextureParameterf(texture, pname, param); }
extern "C" void glTextureParameterfv(GLuint texture, GLenum pname, const GLfloat *param) { proc_glTextureParameterfv(texture, pname, param); }
extern "C" void glTextureParameteri(GLuint texture, GLenum pname, GLint param) { proc_glTextureParameteri(texture, pname, param); }
extern "C" void glTextureParameteriv(GLuint texture, GLenum pname, const GLint *param) { proc_glTextureParameteriv(texture, pname, param); }
extern "C" void glTextureStorage1D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width) { proc_glTextureStorage1D(texture, levels, internalformat, width); }
extern "C" void glTextureStorage2D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height) { proc_glTextureStorage2D(texture, levels, internalformat, width, height); }
extern "C" void glTextureStorage2DMultisample(GLuint texture, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations) { proc_glTextureStorage2DMultisample(texture, samples, internalformat, width, height, fixedsamplelocations); }
extern "C" void glTextureStorage3D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth) { proc_glTextureStorage3D(texture, levels, internalformat, width, height, depth); }
extern "C" void glTextureStorage3DMultisample(GLuint texture, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations) { proc_glTextureStorage3DMultisample(texture, samples, internalformat, width, height, depth, fixedsamplelocations); }
extern "C" void glTextureSubImage1D(GLuint texture, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const void *pixels) { proc_glTextureSubImage1D(texture, level, xoffset, width, format, type, pixels); }
extern "C" void glTextureSubImage2D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *pixels) { proc_glTextureSubImage2D(texture, level, xoffset, yoffset, width, height, format, type, pixels); }
extern "C" void glTextureSubImage3D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels) { proc_glTextureSubImage3D(texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, pixels); }
extern "C" void glTextureView(GLuint texture, GLenum target, GLuint origtexture, GLenum internalformat, GLuint minlevel, GLuint numlevels, GLuint minlayer, GLuint numlayers) { proc_glTextureView(texture, target, origtexture, internalformat, minlevel, numlevels, minlayer, numlayers); }
extern "C" void glTransformFeedbackBufferBase(GLuint xfb, GLuint index, GLuint buffer) { proc_glTransformFeedbackBufferBase(xfb, index, buffer); }
extern "C" void glTransformFeedbackBufferRange(GLuint xfb, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size) { proc_glTransformFeedbackBufferRange(xfb, index, buffer, offset, size); }
extern "C" void glTransformFeedbackVaryings(GLuint program, GLsizei count, const GLchar *const *varyings, GLenum bufferMode) { proc_glTransformFeedbackVaryings(program, count, varyings, bufferMode); }
extern "C" void glUniform1d(GLint location, GLdouble x) { proc_glUniform1d(location, x); }
extern "C" void glUniform1dv(GLint location, GLsizei count, const GLdouble *value) { proc_glUniform1dv(location, count, value); }
extern "C" void glUniform1f(GLint location, GLfloat v0) { proc_glUniform1f(location, v0); }
extern "C" void glUniform1fv(GLint location, GLsizei count, const GLfloat *value) { proc_glUniform1fv(location, count, value); }
extern "C" void glUniform1i(GLint location, GLint v0) { proc_glUniform1i(location, v0); }
extern "C" void glUniform1iv(GLint location, GLsizei count, const GLint *value) { proc_glUniform1iv(location, count, value); }
extern "C" void glUniform1ui(GLint location, GLuint v0) { proc_glUniform1ui(location, v0); }
extern "C" void glUniform1uiv(GLint location, GLsizei count, const GLuint *value) { proc_glUniform1uiv(location, count, value); }
extern "C" void glUniform2d(GLint location, GLdouble x, GLdouble y) { proc_glUniform2d(location, x, y); }
extern "C" void glUniform2dv(GLint location, GLsizei count, const GLdouble *value) { proc_glUniform2dv(location, count, value); }
extern "C" void glUniform2f(GLint location, GLfloat v0, GLfloat v1) { proc_glUniform2f(location, v0, v1); }
extern "C" void glUniform2fv(GLint location, GLsizei count, const GLfloat *value) { proc_glUniform2fv(location, count, value); }
extern "C" void glUniform2i(GLint location, GLint v0, GLint v1) { proc_glUniform2i(location, v0, v1); }
extern "C" void glUniform2iv(GLint location, GLsizei count, const GLint *value) { proc_glUniform2iv(location, count, value); }
extern "C" void glUniform2ui(GLint location, GLuint v0, GLuint v1) { proc_glUniform2ui(location, v0, v1); }
extern "C" void glUniform2uiv(GLint location, GLsizei count, const GLuint *value) { proc_glUniform2uiv(location, count, value); }
extern "C" void glUniform3d(GLint location, GLdouble x, GLdouble y, GLdouble z) { proc_glUniform3d(location, x, y, z); }
extern "C" void glUniform3dv(GLint location, GLsizei count, const GLdouble *value) { proc_glUniform3dv(location, count, value); }
extern "C" void glUniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2) { proc_glUniform3f(location, v0, v1, v2); }
extern "C" void glUniform3fv(GLint location, GLsizei count, const GLfloat *value) { proc_glUniform3fv(location, count, value); }
extern "C" void glUniform3i(GLint location, GLint v0, GLint v1, GLint v2) { proc_glUniform3i(location, v0, v1, v2); }
extern "C" void glUniform3iv(GLint location, GLsizei count, const GLint *value) { proc_glUniform3iv(location, count, value); }
extern "C" void glUniform3ui(GLint location, GLuint v0, GLuint v1, GLuint v2) { proc_glUniform3ui(location, v0, v1, v2); }
extern "C" void glUniform3uiv(GLint location, GLsizei count, const GLuint *value) { proc_glUniform3uiv(location, count, value); }
extern "C" void glUniform4d(GLint location, GLdouble x, GLdouble y, GLdouble z, GLdouble w) { proc_glUniform4d(location, x, y, z, w); }
extern "C" void glUniform4dv(GLint location, GLsizei count, const GLdouble *value) { proc_glUniform4dv(location, count, value); }
extern "C" void glUniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3) { proc_glUniform4f(location, v0, v1, v2, v3); }
extern "C" void glUniform4fv(GLint location, GLsizei count, const GLfloat *value) { proc_glUniform4fv(location, count, value); }
extern "C" void glUniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3) { proc_glUniform4i(location, v0, v1, v2, v3); }
extern "C" void glUniform4iv(GLint location, GLsizei count, const GLint *value) { proc_glUniform4iv(location, count, value); }
extern "C" void glUniform4ui(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3) { proc_glUniform4ui(location, v0, v1, v2, v3); }
extern "C" void glUniform4uiv(GLint location, GLsizei count, const GLuint *value) { proc_glUniform4uiv(location, count, value); }
extern "C" void glUniformBlockBinding(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding) { proc_glUniformBlockBinding(program, uniformBlockIndex, uniformBlockBinding); }
extern "C" void glUniformMatrix2dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glUniformMatrix2dv(location, count, transpose, value); }
extern "C" void glUniformMatrix2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glUniformMatrix2fv(location, count, transpose, value); }
extern "C" void glUniformMatrix2x3dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glUniformMatrix2x3dv(location, count, transpose, value); }
extern "C" void glUniformMatrix2x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glUniformMatrix2x3fv(location, count, transpose, value); }
extern "C" void glUniformMatrix2x4dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glUniformMatrix2x4dv(location, count, transpose, value); }
extern "C" void glUniformMatrix2x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glUniformMatrix2x4fv(location, count, transpose, value); }
extern "C" void glUniformMatrix3dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glUniformMatrix3dv(location, count, transpose, value); }
extern "C" void glUniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glUniformMatrix3fv(location, count, transpose, value); }
extern "C" void glUniformMatrix3x2dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glUniformMatrix3x2dv(location, count, transpose, value); }
extern "C" void glUniformMatrix3x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glUniformMatrix3x2fv(location, count, transpose, value); }
extern "C" void glUniformMatrix3x4dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glUniformMatrix3x4dv(location, count, transpose, value); }
extern "C" void glUniformMatrix3x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glUniformMatrix3x4fv(location, count, transpose, value); }
extern "C" void glUniformMatrix4dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glUniformMatrix4dv(location, count, transpose, value); }
extern "C" void glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glUniformMatrix4fv(location, count, transpose, value); }
extern "C" void glUniformMatrix4x2dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glUniformMatrix4x2dv(location, count, transpose, value); }
extern "C" void glUniformMatrix4x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glUniformMatrix4x2fv(location, count, transpose, value); }
extern "C" void glUniformMatrix4x3dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value) { proc_glUniformMatrix4x3dv(location, count, transpose, value); }
extern "C" void glUniformMatrix4x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value) { proc_glUniformMatrix4x3fv(location, count, transpose, value); }
extern "C" void glUniformSubroutinesuiv(GLenum shadertype, GLsizei count, const GLuint *indices) { proc_glUniformSubroutinesuiv(shadertype, count, indices); }
extern "C" GLboolean glUnmapBuffer(GLenum target) { return proc_glUnmapBuffer(target); }
extern "C" GLboolean glUnmapNamedBuffer(GLuint buffer) { return proc_glUnmapNamedBuffer(buffer); }
extern "C" void glUseProgram(GLuint program) { proc_glUseProgram(program); }
extern "C" void glUseProgramStages(GLuint pipeline, GLbitfield stages, GLuint program) { proc_glUseProgramStages(pipeline, stages, program); }
extern "C" void glValidateProgram(GLuint program) { proc_glValidateProgram(program); }
extern "C" void glValidateProgramPipeline(GLuint pipeline) { proc_glValidateProgramPipeline(pipeline); }
extern "C" void glVertexArrayAttribBinding(GLuint vaobj, GLuint attribindex, GLuint bindingindex) { proc_glVertexArrayAttribBinding(vaobj, attribindex, bindingindex); }
extern "C" void glVertexArrayAttribFormat(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset) { proc_glVertexArrayAttribFormat(vaobj, attribindex, size, type, normalized, relativeoffset); }
extern "C" void glVertexArrayAttribIFormat(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) { proc_glVertexArrayAttribIFormat(vaobj, attribindex, size, type, relativeoffset); }
extern "C" void glVertexArrayAttribLFormat(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) { proc_glVertexArrayAttribLFormat(vaobj, attribindex, size, type, relativeoffset); }
extern "C" void glVertexArrayBindingDivisor(GLuint vaobj, GLuint bindingindex, GLuint divisor) { proc_glVertexArrayBindingDivisor(vaobj, bindingindex, divisor); }
extern "C" void glVertexArrayElementBuffer(GLuint vaobj, GLuint buffer) { proc_glVertexArrayElementBuffer(vaobj, buffer); }
extern "C" void glVertexArrayVertexBuffer(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride) { proc_glVertexArrayVertexBuffer(vaobj, bindingindex, buffer, offset, stride); }
extern "C" void glVertexArrayVertexBuffers(GLuint vaobj, GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizei *strides) { proc_glVertexArrayVertexBuffers(vaobj, first, count, buffers, offsets, strides); }
extern "C" void glVertexAttrib1d(GLuint index, GLdouble x) { proc_glVertexAttrib1d(index, x); }
extern "C" void glVertexAttrib1dv(GLuint index, const GLdouble *v) { proc_glVertexAttrib1dv(index, v); }
extern "C" void glVertexAttrib1f(GLuint index, GLfloat x) { proc_glVertexAttrib1f(index, x); }
extern "C" void glVertexAttrib1fv(GLuint index, const GLfloat *v) { proc_glVertexAttrib1fv(index, v); }
extern "C" void glVertexAttrib1s(GLuint index, GLshort x) { proc_glVertexAttrib1s(index, x); }
extern "C" void glVertexAttrib1sv(GLuint index, const GLshort *v) { proc_glVertexAttrib1sv(index, v); }
extern "C" void glVertexAttrib2d(GLuint index, GLdouble x, GLdouble y) { proc_glVertexAttrib2d(index, x, y); }
extern "C" void glVertexAttrib2dv(GLuint index, const GLdouble *v) { proc_glVertexAttrib2dv(index, v); }
extern "C" void glVertexAttrib2f(GLuint index, GLfloat x, GLfloat y) { proc_glVertexAttrib2f(index, x, y); }
extern "C" void glVertexAttrib2fv(GLuint index, const GLfloat *v) { proc_glVertexAttrib2fv(index, v); }
extern "C" void glVertexAttrib2s(GLuint index, GLshort x, GLshort y) { proc_glVertexAttrib2s(index, x, y); }
extern "C" void glVertexAttrib2sv(GLuint index, const GLshort *v) { proc_glVertexAttrib2sv(index, v); }
extern "C" void glVertexAttrib3d(GLuint index, GLdouble x, GLdouble y, GLdouble z) { proc_glVertexAttrib3d(index, x, y, z); }
extern "C" void glVertexAttrib3dv(GLuint index, const GLdouble *v) { proc_glVertexAttrib3dv(index, v); }
extern "C" void glVertexAttrib3f(GLuint index, GLfloat x, GLfloat y, GLfloat z) { proc_glVertexAttrib3f(index, x, y, z); }
extern "C" void glVertexAttrib3fv(GLuint index, const GLfloat *v) { proc_glVertexAttrib3fv(index, v); }
extern "C" void glVertexAttrib3s(GLuint index, GLshort x, GLshort y, GLshort z) { proc_glVertexAttrib3s(index, x, y, z); }
extern "C" void glVertexAttrib3sv(GLuint index, const GLshort *v) { proc_glVertexAttrib3sv(index, v); }
extern "C" void glVertexAttrib4Nbv(GLuint index, const GLbyte *v) { proc_glVertexAttrib4Nbv(index, v); }
extern "C" void glVertexAttrib4Niv(GLuint index, const GLint *v) { proc_glVertexAttrib4Niv(index, v); }
extern "C" void glVertexAttrib4Nsv(GLuint index, const GLshort *v) { proc_glVertexAttrib4Nsv(index, v); }
extern "C" void glVertexAttrib4Nub(GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w) { proc_glVertexAttrib4Nub(index, x, y, z, w); }
extern "C" void glVertexAttrib4Nubv(GLuint index, const GLubyte *v) { proc_glVertexAttrib4Nubv(index, v); }
extern "C" void glVertexAttrib4Nuiv(GLuint index, const GLuint *v) { proc_glVertexAttrib4Nuiv(index, v); }
extern "C" void glVertexAttrib4Nusv(GLuint index, const GLushort *v) { proc_glVertexAttrib4Nusv(index, v); }
extern "C" void glVertexAttrib4bv(GLuint index, const GLbyte *v) { proc_glVertexAttrib4bv(index, v); }
extern "C" void glVertexAttrib4d(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w) { proc_glVertexAttrib4d(index, x, y, z, w); }
extern "C" void glVertexAttrib4dv(GLuint index, const GLdouble *v) { proc_glVertexAttrib4dv(index, v); }
extern "C" void glVertexAttrib4f(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w) { proc_glVertexAttrib4f(index, x, y, z, w); }
extern "C" void glVertexAttrib4fv(GLuint index, const GLfloat *v) { proc_glVertexAttrib4fv(index, v); }
extern "C" void glVertexAttrib4iv(GLuint index, const GLint *v) { proc_glVertexAttrib4iv(index, v); }
extern "C" void glVertexAttrib4s(GLuint index, GLshort x, GLshort y, GLshort z, GLshort w) { proc_glVertexAttrib4s(index, x, y, z, w); }
extern "C" void glVertexAttrib4sv(GLuint index, const GLshort *v) { proc_glVertexAttrib4sv(index, v); }
extern "C" void glVertexAttrib4ubv(GLuint index, const GLubyte *v) { proc_glVertexAttrib4ubv(index, v); }
extern "C" void glVertexAttrib4uiv(GLuint index, const GLuint *v) { proc_glVertexAttrib4uiv(index, v); }
extern "C" void glVertexAttrib4usv(GLuint index, const GLushort *v) { proc_glVertexAttrib4usv(index, v); }
extern "C" void glVertexAttribBinding(GLuint attribindex, GLuint bindingindex) { proc_glVertexAttribBinding(attribindex, bindingindex); }
extern "C" void glVertexAttribDivisor(GLuint index, GLuint divisor) { proc_glVertexAttribDivisor(index, divisor); }
extern "C" void glVertexAttribFormat(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset) { proc_glVertexAttribFormat(attribindex, size, type, normalized, relativeoffset); }
extern "C" void glVertexAttribI1i(GLuint index, GLint x) { proc_glVertexAttribI1i(index, x); }
extern "C" void glVertexAttribI1iv(GLuint index, const GLint *v) { proc_glVertexAttribI1iv(index, v); }
extern "C" void glVertexAttribI1ui(GLuint index, GLuint x) { proc_glVertexAttribI1ui(index, x); }
extern "C" void glVertexAttribI1uiv(GLuint index, const GLuint *v) { proc_glVertexAttribI1uiv(index, v); }
extern "C" void glVertexAttribI2i(GLuint index, GLint x, GLint y) { proc_glVertexAttribI2i(index, x, y); }
extern "C" void glVertexAttribI2iv(GLuint index, const GLint *v) { proc_glVertexAttribI2iv(index, v); }
extern "C" void glVertexAttribI2ui(GLuint index, GLuint x, GLuint y) { proc_glVertexAttribI2ui(index, x, y); }
extern "C" void glVertexAttribI2uiv(GLuint index, const GLuint *v) { proc_glVertexAttribI2uiv(index, v); }
extern "C" void glVertexAttribI3i(GLuint index, GLint x, GLint y, GLint z) { proc_glVertexAttribI3i(index, x, y, z); }
extern "C" void glVertexAttribI3iv(GLuint index, const GLint *v) { proc_glVertexAttribI3iv(index, v); }
extern "C" void glVertexAttribI3ui(GLuint index, GLuint x, GLuint y, GLuint z) { proc_glVertexAttribI3ui(index, x, y, z); }
extern "C" void glVertexAttribI3uiv(GLuint index, const GLuint *v) { proc_glVertexAttribI3uiv(index, v); }
extern "C" void glVertexAttribI4bv(GLuint index, const GLbyte *v) { proc_glVertexAttribI4bv(index, v); }
extern "C" void glVertexAttribI4i(GLuint index, GLint x, GLint y, GLint z, GLint w) { proc_glVertexAttribI4i(index, x, y, z, w); }
extern "C" void glVertexAttribI4iv(GLuint index, const GLint *v) { proc_glVertexAttribI4iv(index, v); }
extern "C" void glVertexAttribI4sv(GLuint index, const GLshort *v) { proc_glVertexAttribI4sv(index, v); }
extern "C" void glVertexAttribI4ubv(GLuint index, const GLubyte *v) { proc_glVertexAttribI4ubv(index, v); }
extern "C" void glVertexAttribI4ui(GLuint index, GLuint x, GLuint y, GLuint z, GLuint w) { proc_glVertexAttribI4ui(index, x, y, z, w); }
extern "C" void glVertexAttribI4uiv(GLuint index, const GLuint *v) { proc_glVertexAttribI4uiv(index, v); }
extern "C" void glVertexAttribI4usv(GLuint index, const GLushort *v) { proc_glVertexAttribI4usv(index, v); }
extern "C" void glVertexAttribIFormat(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) { proc_glVertexAttribIFormat(attribindex, size, type, relativeoffset); }
extern "C" void glVertexAttribIPointer(GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer) { proc_glVertexAttribIPointer(index, size, type, stride, pointer); }
extern "C" void glVertexAttribL1d(GLuint index, GLdouble x) { proc_glVertexAttribL1d(index, x); }
extern "C" void glVertexAttribL1dv(GLuint index, const GLdouble *v) { proc_glVertexAttribL1dv(index, v); }
extern "C" void glVertexAttribL2d(GLuint index, GLdouble x, GLdouble y) { proc_glVertexAttribL2d(index, x, y); }
extern "C" void glVertexAttribL2dv(GLuint index, const GLdouble *v) { proc_glVertexAttribL2dv(index, v); }
extern "C" void glVertexAttribL3d(GLuint index, GLdouble x, GLdouble y, GLdouble z) { proc_glVertexAttribL3d(index, x, y, z); }
extern "C" void glVertexAttribL3dv(GLuint index, const GLdouble *v) { proc_glVertexAttribL3dv(index, v); }
extern "C" void glVertexAttribL4d(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w) { proc_glVertexAttribL4d(index, x, y, z, w); }
extern "C" void glVertexAttribL4dv(GLuint index, const GLdouble *v) { proc_glVertexAttribL4dv(index, v); }
extern "C" void glVertexAttribLFormat(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset) { proc_glVertexAttribLFormat(attribindex, size, type, relativeoffset); }
extern "C" void glVertexAttribLPointer(GLuint index, GLint size, GLenum type, GLsizei stride, const void *pointer) { proc_glVertexAttribLPointer(index, size, type, stride, pointer); }
extern "C" void glVertexAttribP1ui(GLuint index, GLenum type, GLboolean normalized, GLuint value) { proc_glVertexAttribP1ui(index, type, normalized, value); }
extern "C" void glVertexAttribP1uiv(GLuint index, GLenum type, GLboolean normalized, const GLuint *value) { proc_glVertexAttribP1uiv(index, type, normalized, value); }
extern "C" void glVertexAttribP2ui(GLuint index, GLenum type, GLboolean normalized, GLuint value) { proc_glVertexAttribP2ui(index, type, normalized, value); }
extern "C" void glVertexAttribP2uiv(GLuint index, GLenum type, GLboolean normalized, const GLuint *value) { proc_glVertexAttribP2uiv(index, type, normalized, value); }
extern "C" void glVertexAttribP3ui(GLuint index, GLenum type, GLboolean normalized, GLuint value) { proc_glVertexAttribP3ui(index, type, normalized, value); }
extern "C" void glVertexAttribP3uiv(GLuint index, GLenum type, GLboolean normalized, const GLuint *value) { proc_glVertexAttribP3uiv(index, type, normalized, value); }
extern "C" void glVertexAttribP4ui(GLuint index, GLenum type, GLboolean normalized, GLuint value) { proc_glVertexAttribP4ui(index, type, normalized, value); }
extern "C" void glVertexAttribP4uiv(GLuint index, GLenum type, GLboolean normalized, const GLuint *value) { proc_glVertexAttribP4uiv(index, type, normalized, value); }
extern "C" void glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer) { proc_glVertexAttribPointer(index, size, type, normalized, stride, pointer); }
extern "C" void glVertexBindingDivisor(GLuint bindingindex, GLuint divisor) { proc_glVertexBindingDivisor(bindingindex, divisor); }
extern "C" void glViewport(GLint x, GLint y, GLsizei width, GLsizei height) { proc_glViewport(x, y, width, height); }
extern "C" void glViewportArrayv(GLuint first, GLsizei count, const GLfloat *v) { proc_glViewportArrayv(first, count, v); }
extern "C" void glViewportIndexedf(GLuint index, GLfloat x, GLfloat y, GLfloat w, GLfloat h) { proc_glViewportIndexedf(index, x, y, w, h); }
extern "C" void glViewportIndexedfv(GLuint index, const GLfloat *v) { proc_glViewportIndexedfv(index, v); }
extern "C" void glWaitSync(GLsync sync, GLbitfield flags, GLuint64 timeout) { proc_glWaitSync(sync, flags, timeout); }

void glLoadProcs(GLload loadFn)
{
	proc_glActiveShaderProgram = (decltype(proc_glActiveShaderProgram))loadFn("glActiveShaderProgram");
	proc_glActiveTexture = (decltype(proc_glActiveTexture))loadFn("glActiveTexture");
	proc_glAttachShader = (decltype(proc_glAttachShader))loadFn("glAttachShader");
	proc_glBeginConditionalRender = (decltype(proc_glBeginConditionalRender))loadFn("glBeginConditionalRender");
	proc_glBeginQuery = (decltype(proc_glBeginQuery))loadFn("glBeginQuery");
	proc_glBeginQueryIndexed = (decltype(proc_glBeginQueryIndexed))loadFn("glBeginQueryIndexed");
	proc_glBeginTransformFeedback = (decltype(proc_glBeginTransformFeedback))loadFn("glBeginTransformFeedback");
	proc_glBindAttribLocation = (decltype(proc_glBindAttribLocation))loadFn("glBindAttribLocation");
	proc_glBindBuffer = (decltype(proc_glBindBuffer))loadFn("glBindBuffer");
	proc_glBindBufferBase = (decltype(proc_glBindBufferBase))loadFn("glBindBufferBase");
	proc_glBindBufferRange = (decltype(proc_glBindBufferRange))loadFn("glBindBufferRange");
	proc_glBindBuffersBase = (decltype(proc_glBindBuffersBase))loadFn("glBindBuffersBase");
	proc_glBindBuffersRange = (decltype(proc_glBindBuffersRange))loadFn("glBindBuffersRange");
	proc_glBindFragDataLocation = (decltype(proc_glBindFragDataLocation))loadFn("glBindFragDataLocation");
	proc_glBindFragDataLocationIndexed = (decltype(proc_glBindFragDataLocationIndexed))loadFn("glBindFragDataLocationIndexed");
	proc_glBindFramebuffer = (decltype(proc_glBindFramebuffer))loadFn("glBindFramebuffer");
	proc_glBindImageTexture = (decltype(proc_glBindImageTexture))loadFn("glBindImageTexture");
	proc_glBindImageTextures = (decltype(proc_glBindImageTextures))loadFn("glBindImageTextures");
	proc_glBindProgramPipeline = (decltype(proc_glBindProgramPipeline))loadFn("glBindProgramPipeline");
	proc_glBindRenderbuffer = (decltype(proc_glBindRenderbuffer))loadFn("glBindRenderbuffer");
	proc_glBindSampler = (decltype(proc_glBindSampler))loadFn("glBindSampler");
	proc_glBindSamplers = (decltype(proc_glBindSamplers))loadFn("glBindSamplers");
	proc_glBindTexture = (decltype(proc_glBindTexture))loadFn("glBindTexture");
	proc_glBindTextureUnit = (decltype(proc_glBindTextureUnit))loadFn("glBindTextureUnit");
	proc_glBindTextures = (decltype(proc_glBindTextures))loadFn("glBindTextures");
	proc_glBindTransformFeedback = (decltype(proc_glBindTransformFeedback))loadFn("glBindTransformFeedback");
	proc_glBindVertexArray = (decltype(proc_glBindVertexArray))loadFn("glBindVertexArray");
	proc_glBindVertexBuffer = (decltype(proc_glBindVertexBuffer))loadFn("glBindVertexBuffer");
	proc_glBindVertexBuffers = (decltype(proc_glBindVertexBuffers))loadFn("glBindVertexBuffers");
	proc_glBlendColor = (decltype(proc_glBlendColor))loadFn("glBlendColor");
	proc_glBlendEquation = (decltype(proc_glBlendEquation))loadFn("glBlendEquation");
	proc_glBlendEquationSeparate = (decltype(proc_glBlendEquationSeparate))loadFn("glBlendEquationSeparate");
	proc_glBlendEquationSeparatei = (decltype(proc_glBlendEquationSeparatei))loadFn("glBlendEquationSeparatei");
	proc_glBlendEquationi = (decltype(proc_glBlendEquationi))loadFn("glBlendEquationi");
	proc_glBlendFunc = (decltype(proc_glBlendFunc))loadFn("glBlendFunc");
	proc_glBlendFuncSeparate = (decltype(proc_glBlendFuncSeparate))loadFn("glBlendFuncSeparate");
	proc_glBlendFuncSeparatei = (decltype(proc_glBlendFuncSeparatei))loadFn("glBlendFuncSeparatei");
	proc_glBlendFunci = (decltype(proc_glBlendFunci))loadFn("glBlendFunci");
	proc_glBlitFramebuffer = (decltype(proc_glBlitFramebuffer))loadFn("glBlitFramebuffer");
	proc_glBlitNamedFramebuffer = (decltype(proc_glBlitNamedFramebuffer))loadFn("glBlitNamedFramebuffer");
	proc_glBufferData = (decltype(proc_glBufferData))loadFn("glBufferData");
	proc_glBufferStorage = (decltype(proc_glBufferStorage))loadFn("glBufferStorage");
	proc_glBufferSubData = (decltype(proc_glBufferSubData))loadFn("glBufferSubData");
	proc_glCheckFramebufferStatus = (decltype(proc_glCheckFramebufferStatus))loadFn("glCheckFramebufferStatus");
	proc_glCheckNamedFramebufferStatus = (decltype(proc_glCheckNamedFramebufferStatus))loadFn("glCheckNamedFramebufferStatus");
	proc_glClampColor = (decltype(proc_glClampColor))loadFn("glClampColor");
	proc_glClear = (decltype(proc_glClear))loadFn("glClear");
	proc_glClearBufferData = (decltype(proc_glClearBufferData))loadFn("glClearBufferData");
	proc_glClearBufferSubData = (decltype(proc_glClearBufferSubData))loadFn("glClearBufferSubData");
	proc_glClearBufferfi = (decltype(proc_glClearBufferfi))loadFn("glClearBufferfi");
	proc_glClearBufferfv = (decltype(proc_glClearBufferfv))loadFn("glClearBufferfv");
	proc_glClearBufferiv = (decltype(proc_glClearBufferiv))loadFn("glClearBufferiv");
	proc_glClearBufferuiv = (decltype(proc_glClearBufferuiv))loadFn("glClearBufferuiv");
	proc_glClearColor = (decltype(proc_glClearColor))loadFn("glClearColor");
	proc_glClearDepth = (decltype(proc_glClearDepth))loadFn("glClearDepth");
	proc_glClearDepthf = (decltype(proc_glClearDepthf))loadFn("glClearDepthf");
	proc_glClearNamedBufferData = (decltype(proc_glClearNamedBufferData))loadFn("glClearNamedBufferData");
	proc_glClearNamedBufferSubData = (decltype(proc_glClearNamedBufferSubData))loadFn("glClearNamedBufferSubData");
	proc_glClearNamedFramebufferfi = (decltype(proc_glClearNamedFramebufferfi))loadFn("glClearNamedFramebufferfi");
	proc_glClearNamedFramebufferfv = (decltype(proc_glClearNamedFramebufferfv))loadFn("glClearNamedFramebufferfv");
	proc_glClearNamedFramebufferiv = (decltype(proc_glClearNamedFramebufferiv))loadFn("glClearNamedFramebufferiv");
	proc_glClearNamedFramebufferuiv = (decltype(proc_glClearNamedFramebufferuiv))loadFn("glClearNamedFramebufferuiv");
	proc_glClearStencil = (decltype(proc_glClearStencil))loadFn("glClearStencil");
	proc_glClearTexImage = (decltype(proc_glClearTexImage))loadFn("glClearTexImage");
	proc_glClearTexSubImage = (decltype(proc_glClearTexSubImage))loadFn("glClearTexSubImage");
	proc_glClientWaitSync = (decltype(proc_glClientWaitSync))loadFn("glClientWaitSync");
	proc_glClipControl = (decltype(proc_glClipControl))loadFn("glClipControl");
	proc_glColorMask = (decltype(proc_glColorMask))loadFn("glColorMask");
	proc_glColorMaski = (decltype(proc_glColorMaski))loadFn("glColorMaski");
	proc_glCompileShader = (decltype(proc_glCompileShader))loadFn("glCompileShader");
	proc_glCompressedTexImage1D = (decltype(proc_glCompressedTexImage1D))loadFn("glCompressedTexImage1D");
	proc_glCompressedTexImage2D = (decltype(proc_glCompressedTexImage2D))loadFn("glCompressedTexImage2D");
	proc_glCompressedTexImage3D = (decltype(proc_glCompressedTexImage3D))loadFn("glCompressedTexImage3D");
	proc_glCompressedTexSubImage1D = (decltype(proc_glCompressedTexSubImage1D))loadFn("glCompressedTexSubImage1D");
	proc_glCompressedTexSubImage2D = (decltype(proc_glCompressedTexSubImage2D))loadFn("glCompressedTexSubImage2D");
	proc_glCompressedTexSubImage3D = (decltype(proc_glCompressedTexSubImage3D))loadFn("glCompressedTexSubImage3D");
	proc_glCompressedTextureSubImage1D = (decltype(proc_glCompressedTextureSubImage1D))loadFn("glCompressedTextureSubImage1D");
	proc_glCompressedTextureSubImage2D = (decltype(proc_glCompressedTextureSubImage2D))loadFn("glCompressedTextureSubImage2D");
	proc_glCompressedTextureSubImage3D = (decltype(proc_glCompressedTextureSubImage3D))loadFn("glCompressedTextureSubImage3D");
	proc_glCopyBufferSubData = (decltype(proc_glCopyBufferSubData))loadFn("glCopyBufferSubData");
	proc_glCopyImageSubData = (decltype(proc_glCopyImageSubData))loadFn("glCopyImageSubData");
	proc_glCopyNamedBufferSubData = (decltype(proc_glCopyNamedBufferSubData))loadFn("glCopyNamedBufferSubData");
	proc_glCopyTexImage1D = (decltype(proc_glCopyTexImage1D))loadFn("glCopyTexImage1D");
	proc_glCopyTexImage2D = (decltype(proc_glCopyTexImage2D))loadFn("glCopyTexImage2D");
	proc_glCopyTexSubImage1D = (decltype(proc_glCopyTexSubImage1D))loadFn("glCopyTexSubImage1D");
	proc_glCopyTexSubImage2D = (decltype(proc_glCopyTexSubImage2D))loadFn("glCopyTexSubImage2D");
	proc_glCopyTexSubImage3D = (decltype(proc_glCopyTexSubImage3D))loadFn("glCopyTexSubImage3D");
	proc_glCopyTextureSubImage1D = (decltype(proc_glCopyTextureSubImage1D))loadFn("glCopyTextureSubImage1D");
	proc_glCopyTextureSubImage2D = (decltype(proc_glCopyTextureSubImage2D))loadFn("glCopyTextureSubImage2D");
	proc_glCopyTextureSubImage3D = (decltype(proc_glCopyTextureSubImage3D))loadFn("glCopyTextureSubImage3D");
	proc_glCreateBuffers = (decltype(proc_glCreateBuffers))loadFn("glCreateBuffers");
	proc_glCreateFramebuffers = (decltype(proc_glCreateFramebuffers))loadFn("glCreateFramebuffers");
	proc_glCreateProgram = (decltype(proc_glCreateProgram))loadFn("glCreateProgram");
	proc_glCreateProgramPipelines = (decltype(proc_glCreateProgramPipelines))loadFn("glCreateProgramPipelines");
	proc_glCreateQueries = (decltype(proc_glCreateQueries))loadFn("glCreateQueries");
	proc_glCreateRenderbuffers = (decltype(proc_glCreateRenderbuffers))loadFn("glCreateRenderbuffers");
	proc_glCreateSamplers = (decltype(proc_glCreateSamplers))loadFn("glCreateSamplers");
	proc_glCreateShader = (decltype(proc_glCreateShader))loadFn("glCreateShader");
	proc_glCreateShaderProgramv = (decltype(proc_glCreateShaderProgramv))loadFn("glCreateShaderProgramv");
	proc_glCreateTextures = (decltype(proc_glCreateTextures))loadFn("glCreateTextures");
	proc_glCreateTransformFeedbacks = (decltype(proc_glCreateTransformFeedbacks))loadFn("glCreateTransformFeedbacks");
	proc_glCreateVertexArrays = (decltype(proc_glCreateVertexArrays))loadFn("glCreateVertexArrays");
	proc_glCullFace = (decltype(proc_glCullFace))loadFn("glCullFace");
	proc_glDebugMessageCallback = (decltype(proc_glDebugMessageCallback))loadFn("glDebugMessageCallback");
	proc_glDebugMessageControl = (decltype(proc_glDebugMessageControl))loadFn("glDebugMessageControl");
	proc_glDebugMessageInsert = (decltype(proc_glDebugMessageInsert))loadFn("glDebugMessageInsert");
	proc_glDeleteBuffers = (decltype(proc_glDeleteBuffers))loadFn("glDeleteBuffers");
	proc_glDeleteFramebuffers = (decltype(proc_glDeleteFramebuffers))loadFn("glDeleteFramebuffers");
	proc_glDeleteProgram = (decltype(proc_glDeleteProgram))loadFn("glDeleteProgram");
	proc_glDeleteProgramPipelines = (decltype(proc_glDeleteProgramPipelines))loadFn("glDeleteProgramPipelines");
	proc_glDeleteQueries = (decltype(proc_glDeleteQueries))loadFn("glDeleteQueries");
	proc_glDeleteRenderbuffers = (decltype(proc_glDeleteRenderbuffers))loadFn("glDeleteRenderbuffers");
	proc_glDeleteSamplers = (decltype(proc_glDeleteSamplers))loadFn("glDeleteSamplers");
	proc_glDeleteShader = (decltype(proc_glDeleteShader))loadFn("glDeleteShader");
	proc_glDeleteSync = (decltype(proc_glDeleteSync))loadFn("glDeleteSync");
	proc_glDeleteTextures = (decltype(proc_glDeleteTextures))loadFn("glDeleteTextures");
	proc_glDeleteTransformFeedbacks = (decltype(proc_glDeleteTransformFeedbacks))loadFn("glDeleteTransformFeedbacks");
	proc_glDeleteVertexArrays = (decltype(proc_glDeleteVertexArrays))loadFn("glDeleteVertexArrays");
	proc_glDepthFunc = (decltype(proc_glDepthFunc))loadFn("glDepthFunc");
	proc_glDepthMask = (decltype(proc_glDepthMask))loadFn("glDepthMask");
	proc_glDepthRange = (decltype(proc_glDepthRange))loadFn("glDepthRange");
	proc_glDepthRangeArrayv = (decltype(proc_glDepthRangeArrayv))loadFn("glDepthRangeArrayv");
	proc_glDepthRangeIndexed = (decltype(proc_glDepthRangeIndexed))loadFn("glDepthRangeIndexed");
	proc_glDepthRangef = (decltype(proc_glDepthRangef))loadFn("glDepthRangef");
	proc_glDetachShader = (decltype(proc_glDetachShader))loadFn("glDetachShader");
	proc_glDisable = (decltype(proc_glDisable))loadFn("glDisable");
	proc_glDisableVertexArrayAttrib = (decltype(proc_glDisableVertexArrayAttrib))loadFn("glDisableVertexArrayAttrib");
	proc_glDisableVertexAttribArray = (decltype(proc_glDisableVertexAttribArray))loadFn("glDisableVertexAttribArray");
	proc_glDisablei = (decltype(proc_glDisablei))loadFn("glDisablei");
	proc_glDispatchCompute = (decltype(proc_glDispatchCompute))loadFn("glDispatchCompute");
	proc_glDispatchComputeIndirect = (decltype(proc_glDispatchComputeIndirect))loadFn("glDispatchComputeIndirect");
	proc_glDrawArrays = (decltype(proc_glDrawArrays))loadFn("glDrawArrays");
	proc_glDrawArraysIndirect = (decltype(proc_glDrawArraysIndirect))loadFn("glDrawArraysIndirect");
	proc_glDrawArraysInstanced = (decltype(proc_glDrawArraysInstanced))loadFn("glDrawArraysInstanced");
	proc_glDrawArraysInstancedBaseInstance = (decltype(proc_glDrawArraysInstancedBaseInstance))loadFn("glDrawArraysInstancedBaseInstance");
	proc_glDrawBuffer = (decltype(proc_glDrawBuffer))loadFn("glDrawBuffer");
	proc_glDrawBuffers = (decltype(proc_glDrawBuffers))loadFn("glDrawBuffers");
	proc_glDrawElements = (decltype(proc_glDrawElements))loadFn("glDrawElements");
	proc_glDrawElementsBaseVertex = (decltype(proc_glDrawElementsBaseVertex))loadFn("glDrawElementsBaseVertex");
	proc_glDrawElementsIndirect = (decltype(proc_glDrawElementsIndirect))loadFn("glDrawElementsIndirect");
	proc_glDrawElementsInstanced = (decltype(proc_glDrawElementsInstanced))loadFn("glDrawElementsInstanced");
	proc_glDrawElementsInstancedBaseInstance = (decltype(proc_glDrawElementsInstancedBaseInstance))loadFn("glDrawElementsInstancedBaseInstance");
	proc_glDrawElementsInstancedBaseVertex = (decltype(proc_glDrawElementsInstancedBaseVertex))loadFn("glDrawElementsInstancedBaseVertex");
	proc_glDrawElementsInstancedBaseVertexBaseInstance = (decltype(proc_glDrawElementsInstancedBaseVertexBaseInstance))loadFn("glDrawElementsInstancedBaseVertexBaseInstance");
	proc_glDrawRangeElements = (decltype(proc_glDrawRangeElements))loadFn("glDrawRangeElements");
	proc_glDrawRangeElementsBaseVertex = (decltype(proc_glDrawRangeElementsBaseVertex))loadFn("glDrawRangeElementsBaseVertex");
	proc_glDrawTransformFeedback = (decltype(proc_glDrawTransformFeedback))loadFn("glDrawTransformFeedback");
	proc_glDrawTransformFeedbackInstanced = (decltype(proc_glDrawTransformFeedbackInstanced))loadFn("glDrawTransformFeedbackInstanced");
	proc_glDrawTransformFeedbackStream = (decltype(proc_glDrawTransformFeedbackStream))loadFn("glDrawTransformFeedbackStream");
	proc_glDrawTransformFeedbackStreamInstanced = (decltype(proc_glDrawTransformFeedbackStreamInstanced))loadFn("glDrawTransformFeedbackStreamInstanced");
	proc_glEnable = (decltype(proc_glEnable))loadFn("glEnable");
	proc_glEnableVertexArrayAttrib = (decltype(proc_glEnableVertexArrayAttrib))loadFn("glEnableVertexArrayAttrib");
	proc_glEnableVertexAttribArray = (decltype(proc_glEnableVertexAttribArray))loadFn("glEnableVertexAttribArray");
	proc_glEnablei = (decltype(proc_glEnablei))loadFn("glEnablei");
	proc_glEndConditionalRender = (decltype(proc_glEndConditionalRender))loadFn("glEndConditionalRender");
	proc_glEndQuery = (decltype(proc_glEndQuery))loadFn("glEndQuery");
	proc_glEndQueryIndexed = (decltype(proc_glEndQueryIndexed))loadFn("glEndQueryIndexed");
	proc_glEndTransformFeedback = (decltype(proc_glEndTransformFeedback))loadFn("glEndTransformFeedback");
	proc_glFenceSync = (decltype(proc_glFenceSync))loadFn("glFenceSync");
	proc_glFinish = (decltype(proc_glFinish))loadFn("glFinish");
	proc_glFlush = (decltype(proc_glFlush))loadFn("glFlush");
	proc_glFlushMappedBufferRange = (decltype(proc_glFlushMappedBufferRange))loadFn("glFlushMappedBufferRange");
	proc_glFlushMappedNamedBufferRange = (decltype(proc_glFlushMappedNamedBufferRange))loadFn("glFlushMappedNamedBufferRange");
	proc_glFramebufferParameteri = (decltype(proc_glFramebufferParameteri))loadFn("glFramebufferParameteri");
	proc_glFramebufferRenderbuffer = (decltype(proc_glFramebufferRenderbuffer))loadFn("glFramebufferRenderbuffer");
	proc_glFramebufferTexture = (decltype(proc_glFramebufferTexture))loadFn("glFramebufferTexture");
	proc_glFramebufferTexture1D = (decltype(proc_glFramebufferTexture1D))loadFn("glFramebufferTexture1D");
	proc_glFramebufferTexture2D = (decltype(proc_glFramebufferTexture2D))loadFn("glFramebufferTexture2D");
	proc_glFramebufferTexture3D = (decltype(proc_glFramebufferTexture3D))loadFn("glFramebufferTexture3D");
	proc_glFramebufferTextureLayer = (decltype(proc_glFramebufferTextureLayer))loadFn("glFramebufferTextureLayer");
	proc_glFrontFace = (decltype(proc_glFrontFace))loadFn("glFrontFace");
	proc_glGenBuffers = (decltype(proc_glGenBuffers))loadFn("glGenBuffers");
	proc_glGenFramebuffers = (decltype(proc_glGenFramebuffers))loadFn("glGenFramebuffers");
	proc_glGenProgramPipelines = (decltype(proc_glGenProgramPipelines))loadFn("glGenProgramPipelines");
	proc_glGenQueries = (decltype(proc_glGenQueries))loadFn("glGenQueries");
	proc_glGenRenderbuffers = (decltype(proc_glGenRenderbuffers))loadFn("glGenRenderbuffers");
	proc_glGenSamplers = (decltype(proc_glGenSamplers))loadFn("glGenSamplers");
	proc_glGenTextures = (decltype(proc_glGenTextures))loadFn("glGenTextures");
	proc_glGenTransformFeedbacks = (decltype(proc_glGenTransformFeedbacks))loadFn("glGenTransformFeedbacks");
	proc_glGenVertexArrays = (decltype(proc_glGenVertexArrays))loadFn("glGenVertexArrays");
	proc_glGenerateMipmap = (decltype(proc_glGenerateMipmap))loadFn("glGenerateMipmap");
	proc_glGenerateTextureMipmap = (decltype(proc_glGenerateTextureMipmap))loadFn("glGenerateTextureMipmap");
	proc_glGetActiveAtomicCounterBufferiv = (decltype(proc_glGetActiveAtomicCounterBufferiv))loadFn("glGetActiveAtomicCounterBufferiv");
	proc_glGetActiveAttrib = (decltype(proc_glGetActiveAttrib))loadFn("glGetActiveAttrib");
	proc_glGetActiveSubroutineName = (decltype(proc_glGetActiveSubroutineName))loadFn("glGetActiveSubroutineName");
	proc_glGetActiveSubroutineUniformName = (decltype(proc_glGetActiveSubroutineUniformName))loadFn("glGetActiveSubroutineUniformName");
	proc_glGetActiveSubroutineUniformiv = (decltype(proc_glGetActiveSubroutineUniformiv))loadFn("glGetActiveSubroutineUniformiv");
	proc_glGetActiveUniform = (decltype(proc_glGetActiveUniform))loadFn("glGetActiveUniform");
	proc_glGetActiveUniformBlockName = (decltype(proc_glGetActiveUniformBlockName))loadFn("glGetActiveUniformBlockName");
	proc_glGetActiveUniformBlockiv = (decltype(proc_glGetActiveUniformBlockiv))loadFn("glGetActiveUniformBlockiv");
	proc_glGetActiveUniformName = (decltype(proc_glGetActiveUniformName))loadFn("glGetActiveUniformName");
	proc_glGetActiveUniformsiv = (decltype(proc_glGetActiveUniformsiv))loadFn("glGetActiveUniformsiv");
	proc_glGetAttachedShaders = (decltype(proc_glGetAttachedShaders))loadFn("glGetAttachedShaders");
	proc_glGetAttribLocation = (decltype(proc_glGetAttribLocation))loadFn("glGetAttribLocation");
	proc_glGetBooleani_v = (decltype(proc_glGetBooleani_v))loadFn("glGetBooleani_v");
	proc_glGetBooleanv = (decltype(proc_glGetBooleanv))loadFn("glGetBooleanv");
	proc_glGetBufferParameteri64v = (decltype(proc_glGetBufferParameteri64v))loadFn("glGetBufferParameteri64v");
	proc_glGetBufferParameteriv = (decltype(proc_glGetBufferParameteriv))loadFn("glGetBufferParameteriv");
	proc_glGetBufferPointerv = (decltype(proc_glGetBufferPointerv))loadFn("glGetBufferPointerv");
	proc_glGetBufferSubData = (decltype(proc_glGetBufferSubData))loadFn("glGetBufferSubData");
	proc_glGetCompressedTexImage = (decltype(proc_glGetCompressedTexImage))loadFn("glGetCompressedTexImage");
	proc_glGetCompressedTextureImage = (decltype(proc_glGetCompressedTextureImage))loadFn("glGetCompressedTextureImage");
	proc_glGetCompressedTextureSubImage = (decltype(proc_glGetCompressedTextureSubImage))loadFn("glGetCompressedTextureSubImage");
	proc_glGetDebugMessageLog = (decltype(proc_glGetDebugMessageLog))loadFn("glGetDebugMessageLog");
	proc_glGetDoublei_v = (decltype(proc_glGetDoublei_v))loadFn("glGetDoublei_v");
	proc_glGetDoublev = (decltype(proc_glGetDoublev))loadFn("glGetDoublev");
	proc_glGetError = (decltype(proc_glGetError))loadFn("glGetError");
	proc_glGetFloati_v = (decltype(proc_glGetFloati_v))loadFn("glGetFloati_v");
	proc_glGetFloatv = (decltype(proc_glGetFloatv))loadFn("glGetFloatv");
	proc_glGetFragDataIndex = (decltype(proc_glGetFragDataIndex))loadFn("glGetFragDataIndex");
	proc_glGetFragDataLocation = (decltype(proc_glGetFragDataLocation))loadFn("glGetFragDataLocation");
	proc_glGetFramebufferAttachmentParameteriv = (decltype(proc_glGetFramebufferAttachmentParameteriv))loadFn("glGetFramebufferAttachmentParameteriv");
	proc_glGetFramebufferParameteriv = (decltype(proc_glGetFramebufferParameteriv))loadFn("glGetFramebufferParameteriv");
	proc_glGetGraphicsResetStatus = (decltype(proc_glGetGraphicsResetStatus))loadFn("glGetGraphicsResetStatus");
	proc_glGetInteger64i_v = (decltype(proc_glGetInteger64i_v))loadFn("glGetInteger64i_v");
	proc_glGetInteger64v = (decltype(proc_glGetInteger64v))loadFn("glGetInteger64v");
	proc_glGetIntegeri_v = (decltype(proc_glGetIntegeri_v))loadFn("glGetIntegeri_v");
	proc_glGetIntegerv = (decltype(proc_glGetIntegerv))loadFn("glGetIntegerv");
	proc_glGetInternalformati64v = (decltype(proc_glGetInternalformati64v))loadFn("glGetInternalformati64v");
	proc_glGetInternalformativ = (decltype(proc_glGetInternalformativ))loadFn("glGetInternalformativ");
	proc_glGetMultisamplefv = (decltype(proc_glGetMultisamplefv))loadFn("glGetMultisamplefv");
	proc_glGetNamedBufferParameteri64v = (decltype(proc_glGetNamedBufferParameteri64v))loadFn("glGetNamedBufferParameteri64v");
	proc_glGetNamedBufferParameteriv = (decltype(proc_glGetNamedBufferParameteriv))loadFn("glGetNamedBufferParameteriv");
	proc_glGetNamedBufferPointerv = (decltype(proc_glGetNamedBufferPointerv))loadFn("glGetNamedBufferPointerv");
	proc_glGetNamedBufferSubData = (decltype(proc_glGetNamedBufferSubData))loadFn("glGetNamedBufferSubData");
	proc_glGetNamedFramebufferAttachmentParameteriv = (decltype(proc_glGetNamedFramebufferAttachmentParameteriv))loadFn("glGetNamedFramebufferAttachmentParameteriv");
	proc_glGetNamedFramebufferParameteriv = (decltype(proc_glGetNamedFramebufferParameteriv))loadFn("glGetNamedFramebufferParameteriv");
	proc_glGetNamedRenderbufferParameteriv = (decltype(proc_glGetNamedRenderbufferParameteriv))loadFn("glGetNamedRenderbufferParameteriv");
	proc_glGetObjectLabel = (decltype(proc_glGetObjectLabel))loadFn("glGetObjectLabel");
	proc_glGetObjectPtrLabel = (decltype(proc_glGetObjectPtrLabel))loadFn("glGetObjectPtrLabel");
	proc_glGetPointerv = (decltype(proc_glGetPointerv))loadFn("glGetPointerv");
	proc_glGetProgramBinary = (decltype(proc_glGetProgramBinary))loadFn("glGetProgramBinary");
	proc_glGetProgramInfoLog = (decltype(proc_glGetProgramInfoLog))loadFn("glGetProgramInfoLog");
	proc_glGetProgramInterfaceiv = (decltype(proc_glGetProgramInterfaceiv))loadFn("glGetProgramInterfaceiv");
	proc_glGetProgramPipelineInfoLog = (decltype(proc_glGetProgramPipelineInfoLog))loadFn("glGetProgramPipelineInfoLog");
	proc_glGetProgramPipelineiv = (decltype(proc_glGetProgramPipelineiv))loadFn("glGetProgramPipelineiv");
	proc_glGetProgramResourceIndex = (decltype(proc_glGetProgramResourceIndex))loadFn("glGetProgramResourceIndex");
	proc_glGetProgramResourceLocation = (decltype(proc_glGetProgramResourceLocation))loadFn("glGetProgramResourceLocation");
	proc_glGetProgramResourceLocationIndex = (decltype(proc_glGetProgramResourceLocationIndex))loadFn("glGetProgramResourceLocationIndex");
	proc_glGetProgramResourceName = (decltype(proc_glGetProgramResourceName))loadFn("glGetProgramResourceName");
	proc_glGetProgramResourceiv = (decltype(proc_glGetProgramResourceiv))loadFn("glGetProgramResourceiv");
	proc_glGetProgramStageiv = (decltype(proc_glGetProgramStageiv))loadFn("glGetProgramStageiv");
	proc_glGetProgramiv = (decltype(proc_glGetProgramiv))loadFn("glGetProgramiv");
	proc_glGetQueryBufferObjecti64v = (decltype(proc_glGetQueryBufferObjecti64v))loadFn("glGetQueryBufferObjecti64v");
	proc_glGetQueryBufferObjectiv = (decltype(proc_glGetQueryBufferObjectiv))loadFn("glGetQueryBufferObjectiv");
	proc_glGetQueryBufferObjectui64v = (decltype(proc_glGetQueryBufferObjectui64v))loadFn("glGetQueryBufferObjectui64v");
	proc_glGetQueryBufferObjectuiv = (decltype(proc_glGetQueryBufferObjectuiv))loadFn("glGetQueryBufferObjectuiv");
	proc_glGetQueryIndexediv = (decltype(proc_glGetQueryIndexediv))loadFn("glGetQueryIndexediv");
	proc_glGetQueryObjecti64v = (decltype(proc_glGetQueryObjecti64v))loadFn("glGetQueryObjecti64v");
	proc_glGetQueryObjectiv = (decltype(proc_glGetQueryObjectiv))loadFn("glGetQueryObjectiv");
	proc_glGetQueryObjectui64v = (decltype(proc_glGetQueryObjectui64v))loadFn("glGetQueryObjectui64v");
	proc_glGetQueryObjectuiv = (decltype(proc_glGetQueryObjectuiv))loadFn("glGetQueryObjectuiv");
	proc_glGetQueryiv = (decltype(proc_glGetQueryiv))loadFn("glGetQueryiv");
	proc_glGetRenderbufferParameteriv = (decltype(proc_glGetRenderbufferParameteriv))loadFn("glGetRenderbufferParameteriv");
	proc_glGetSamplerParameterIiv = (decltype(proc_glGetSamplerParameterIiv))loadFn("glGetSamplerParameterIiv");
	proc_glGetSamplerParameterIuiv = (decltype(proc_glGetSamplerParameterIuiv))loadFn("glGetSamplerParameterIuiv");
	proc_glGetSamplerParameterfv = (decltype(proc_glGetSamplerParameterfv))loadFn("glGetSamplerParameterfv");
	proc_glGetSamplerParameteriv = (decltype(proc_glGetSamplerParameteriv))loadFn("glGetSamplerParameteriv");
	proc_glGetShaderInfoLog = (decltype(proc_glGetShaderInfoLog))loadFn("glGetShaderInfoLog");
	proc_glGetShaderPrecisionFormat = (decltype(proc_glGetShaderPrecisionFormat))loadFn("glGetShaderPrecisionFormat");
	proc_glGetShaderSource = (decltype(proc_glGetShaderSource))loadFn("glGetShaderSource");
	proc_glGetShaderiv = (decltype(proc_glGetShaderiv))loadFn("glGetShaderiv");
	proc_glGetString = (decltype(proc_glGetString))loadFn("glGetString");
	proc_glGetStringi = (decltype(proc_glGetStringi))loadFn("glGetStringi");
	proc_glGetSubroutineIndex = (decltype(proc_glGetSubroutineIndex))loadFn("glGetSubroutineIndex");
	proc_glGetSubroutineUniformLocation = (decltype(proc_glGetSubroutineUniformLocation))loadFn("glGetSubroutineUniformLocation");
	proc_glGetSynciv = (decltype(proc_glGetSynciv))loadFn("glGetSynciv");
	proc_glGetTexImage = (decltype(proc_glGetTexImage))loadFn("glGetTexImage");
	proc_glGetTexLevelParameterfv = (decltype(proc_glGetTexLevelParameterfv))loadFn("glGetTexLevelParameterfv");
	proc_glGetTexLevelParameteriv = (decltype(proc_glGetTexLevelParameteriv))loadFn("glGetTexLevelParameteriv");
	proc_glGetTexParameterIiv = (decltype(proc_glGetTexParameterIiv))loadFn("glGetTexParameterIiv");
	proc_glGetTexParameterIuiv = (decltype(proc_glGetTexParameterIuiv))loadFn("glGetTexParameterIuiv");
	proc_glGetTexParameterfv = (decltype(proc_glGetTexParameterfv))loadFn("glGetTexParameterfv");
	proc_glGetTexParameteriv = (decltype(proc_glGetTexParameteriv))loadFn("glGetTexParameteriv");
	proc_glGetTextureImage = (decltype(proc_glGetTextureImage))loadFn("glGetTextureImage");
	proc_glGetTextureLevelParameterfv = (decltype(proc_glGetTextureLevelParameterfv))loadFn("glGetTextureLevelParameterfv");
	proc_glGetTextureLevelParameteriv = (decltype(proc_glGetTextureLevelParameteriv))loadFn("glGetTextureLevelParameteriv");
	proc_glGetTextureParameterIiv = (decltype(proc_glGetTextureParameterIiv))loadFn("glGetTextureParameterIiv");
	proc_glGetTextureParameterIuiv = (decltype(proc_glGetTextureParameterIuiv))loadFn("glGetTextureParameterIuiv");
	proc_glGetTextureParameterfv = (decltype(proc_glGetTextureParameterfv))loadFn("glGetTextureParameterfv");
	proc_glGetTextureParameteriv = (decltype(proc_glGetTextureParameteriv))loadFn("glGetTextureParameteriv");
	proc_glGetTextureSubImage = (decltype(proc_glGetTextureSubImage))loadFn("glGetTextureSubImage");
	proc_glGetTransformFeedbackVarying = (decltype(proc_glGetTransformFeedbackVarying))loadFn("glGetTransformFeedbackVarying");
	proc_glGetTransformFeedbacki64_v = (decltype(proc_glGetTransformFeedbacki64_v))loadFn("glGetTransformFeedbacki64_v");
	proc_glGetTransformFeedbacki_v = (decltype(proc_glGetTransformFeedbacki_v))loadFn("glGetTransformFeedbacki_v");
	proc_glGetTransformFeedbackiv = (decltype(proc_glGetTransformFeedbackiv))loadFn("glGetTransformFeedbackiv");
	proc_glGetUniformBlockIndex = (decltype(proc_glGetUniformBlockIndex))loadFn("glGetUniformBlockIndex");
	proc_glGetUniformIndices = (decltype(proc_glGetUniformIndices))loadFn("glGetUniformIndices");
	proc_glGetUniformLocation = (decltype(proc_glGetUniformLocation))loadFn("glGetUniformLocation");
	proc_glGetUniformSubroutineuiv = (decltype(proc_glGetUniformSubroutineuiv))loadFn("glGetUniformSubroutineuiv");
	proc_glGetUniformdv = (decltype(proc_glGetUniformdv))loadFn("glGetUniformdv");
	proc_glGetUniformfv = (decltype(proc_glGetUniformfv))loadFn("glGetUniformfv");
	proc_glGetUniformiv = (decltype(proc_glGetUniformiv))loadFn("glGetUniformiv");
	proc_glGetUniformuiv = (decltype(proc_glGetUniformuiv))loadFn("glGetUniformuiv");
	proc_glGetVertexArrayIndexed64iv = (decltype(proc_glGetVertexArrayIndexed64iv))loadFn("glGetVertexArrayIndexed64iv");
	proc_glGetVertexArrayIndexediv = (decltype(proc_glGetVertexArrayIndexediv))loadFn("glGetVertexArrayIndexediv");
	proc_glGetVertexArrayiv = (decltype(proc_glGetVertexArrayiv))loadFn("glGetVertexArrayiv");
	proc_glGetVertexAttribIiv = (decltype(proc_glGetVertexAttribIiv))loadFn("glGetVertexAttribIiv");
	proc_glGetVertexAttribIuiv = (decltype(proc_glGetVertexAttribIuiv))loadFn("glGetVertexAttribIuiv");
	proc_glGetVertexAttribLdv = (decltype(proc_glGetVertexAttribLdv))loadFn("glGetVertexAttribLdv");
	proc_glGetVertexAttribPointerv = (decltype(proc_glGetVertexAttribPointerv))loadFn("glGetVertexAttribPointerv");
	proc_glGetVertexAttribdv = (decltype(proc_glGetVertexAttribdv))loadFn("glGetVertexAttribdv");
	proc_glGetVertexAttribfv = (decltype(proc_glGetVertexAttribfv))loadFn("glGetVertexAttribfv");
	proc_glGetVertexAttribiv = (decltype(proc_glGetVertexAttribiv))loadFn("glGetVertexAttribiv");
	proc_glGetnCompressedTexImage = (decltype(proc_glGetnCompressedTexImage))loadFn("glGetnCompressedTexImage");
	proc_glGetnTexImage = (decltype(proc_glGetnTexImage))loadFn("glGetnTexImage");
	proc_glGetnUniformdv = (decltype(proc_glGetnUniformdv))loadFn("glGetnUniformdv");
	proc_glGetnUniformfv = (decltype(proc_glGetnUniformfv))loadFn("glGetnUniformfv");
	proc_glGetnUniformiv = (decltype(proc_glGetnUniformiv))loadFn("glGetnUniformiv");
	proc_glGetnUniformuiv = (decltype(proc_glGetnUniformuiv))loadFn("glGetnUniformuiv");
	proc_glHint = (decltype(proc_glHint))loadFn("glHint");
	proc_glInvalidateBufferData = (decltype(proc_glInvalidateBufferData))loadFn("glInvalidateBufferData");
	proc_glInvalidateBufferSubData = (decltype(proc_glInvalidateBufferSubData))loadFn("glInvalidateBufferSubData");
	proc_glInvalidateFramebuffer = (decltype(proc_glInvalidateFramebuffer))loadFn("glInvalidateFramebuffer");
	proc_glInvalidateNamedFramebufferData = (decltype(proc_glInvalidateNamedFramebufferData))loadFn("glInvalidateNamedFramebufferData");
	proc_glInvalidateNamedFramebufferSubData = (decltype(proc_glInvalidateNamedFramebufferSubData))loadFn("glInvalidateNamedFramebufferSubData");
	proc_glInvalidateSubFramebuffer = (decltype(proc_glInvalidateSubFramebuffer))loadFn("glInvalidateSubFramebuffer");
	proc_glInvalidateTexImage = (decltype(proc_glInvalidateTexImage))loadFn("glInvalidateTexImage");
	proc_glInvalidateTexSubImage = (decltype(proc_glInvalidateTexSubImage))loadFn("glInvalidateTexSubImage");
	proc_glIsBuffer = (decltype(proc_glIsBuffer))loadFn("glIsBuffer");
	proc_glIsEnabled = (decltype(proc_glIsEnabled))loadFn("glIsEnabled");
	proc_glIsEnabledi = (decltype(proc_glIsEnabledi))loadFn("glIsEnabledi");
	proc_glIsFramebuffer = (decltype(proc_glIsFramebuffer))loadFn("glIsFramebuffer");
	proc_glIsProgram = (decltype(proc_glIsProgram))loadFn("glIsProgram");
	proc_glIsProgramPipeline = (decltype(proc_glIsProgramPipeline))loadFn("glIsProgramPipeline");
	proc_glIsQuery = (decltype(proc_glIsQuery))loadFn("glIsQuery");
	proc_glIsRenderbuffer = (decltype(proc_glIsRenderbuffer))loadFn("glIsRenderbuffer");
	proc_glIsSampler = (decltype(proc_glIsSampler))loadFn("glIsSampler");
	proc_glIsShader = (decltype(proc_glIsShader))loadFn("glIsShader");
	proc_glIsSync = (decltype(proc_glIsSync))loadFn("glIsSync");
	proc_glIsTexture = (decltype(proc_glIsTexture))loadFn("glIsTexture");
	proc_glIsTransformFeedback = (decltype(proc_glIsTransformFeedback))loadFn("glIsTransformFeedback");
	proc_glIsVertexArray = (decltype(proc_glIsVertexArray))loadFn("glIsVertexArray");
	proc_glLineWidth = (decltype(proc_glLineWidth))loadFn("glLineWidth");
	proc_glLinkProgram = (decltype(proc_glLinkProgram))loadFn("glLinkProgram");
	proc_glLogicOp = (decltype(proc_glLogicOp))loadFn("glLogicOp");
	proc_glMapBuffer = (decltype(proc_glMapBuffer))loadFn("glMapBuffer");
	proc_glMapBufferRange = (decltype(proc_glMapBufferRange))loadFn("glMapBufferRange");
	proc_glMapNamedBuffer = (decltype(proc_glMapNamedBuffer))loadFn("glMapNamedBuffer");
	proc_glMapNamedBufferRange = (decltype(proc_glMapNamedBufferRange))loadFn("glMapNamedBufferRange");
	proc_glMemoryBarrier = (decltype(proc_glMemoryBarrier))loadFn("glMemoryBarrier");
	proc_glMemoryBarrierByRegion = (decltype(proc_glMemoryBarrierByRegion))loadFn("glMemoryBarrierByRegion");
	proc_glMinSampleShading = (decltype(proc_glMinSampleShading))loadFn("glMinSampleShading");
	proc_glMultiDrawArrays = (decltype(proc_glMultiDrawArrays))loadFn("glMultiDrawArrays");
	proc_glMultiDrawArraysIndirect = (decltype(proc_glMultiDrawArraysIndirect))loadFn("glMultiDrawArraysIndirect");
	proc_glMultiDrawElements = (decltype(proc_glMultiDrawElements))loadFn("glMultiDrawElements");
	proc_glMultiDrawElementsBaseVertex = (decltype(proc_glMultiDrawElementsBaseVertex))loadFn("glMultiDrawElementsBaseVertex");
	proc_glMultiDrawElementsIndirect = (decltype(proc_glMultiDrawElementsIndirect))loadFn("glMultiDrawElementsIndirect");
	proc_glNamedBufferData = (decltype(proc_glNamedBufferData))loadFn("glNamedBufferData");
	proc_glNamedBufferStorage = (decltype(proc_glNamedBufferStorage))loadFn("glNamedBufferStorage");
	proc_glNamedBufferSubData = (decltype(proc_glNamedBufferSubData))loadFn("glNamedBufferSubData");
	proc_glNamedFramebufferDrawBuffer = (decltype(proc_glNamedFramebufferDrawBuffer))loadFn("glNamedFramebufferDrawBuffer");
	proc_glNamedFramebufferDrawBuffers = (decltype(proc_glNamedFramebufferDrawBuffers))loadFn("glNamedFramebufferDrawBuffers");
	proc_glNamedFramebufferParameteri = (decltype(proc_glNamedFramebufferParameteri))loadFn("glNamedFramebufferParameteri");
	proc_glNamedFramebufferReadBuffer = (decltype(proc_glNamedFramebufferReadBuffer))loadFn("glNamedFramebufferReadBuffer");
	proc_glNamedFramebufferRenderbuffer = (decltype(proc_glNamedFramebufferRenderbuffer))loadFn("glNamedFramebufferRenderbuffer");
	proc_glNamedFramebufferTexture = (decltype(proc_glNamedFramebufferTexture))loadFn("glNamedFramebufferTexture");
	proc_glNamedFramebufferTextureLayer = (decltype(proc_glNamedFramebufferTextureLayer))loadFn("glNamedFramebufferTextureLayer");
	proc_glNamedRenderbufferStorage = (decltype(proc_glNamedRenderbufferStorage))loadFn("glNamedRenderbufferStorage");
	proc_glNamedRenderbufferStorageMultisample = (decltype(proc_glNamedRenderbufferStorageMultisample))loadFn("glNamedRenderbufferStorageMultisample");
	proc_glObjectLabel = (decltype(proc_glObjectLabel))loadFn("glObjectLabel");
	proc_glObjectPtrLabel = (decltype(proc_glObjectPtrLabel))loadFn("glObjectPtrLabel");
	proc_glPatchParameterfv = (decltype(proc_glPatchParameterfv))loadFn("glPatchParameterfv");
	proc_glPatchParameteri = (decltype(proc_glPatchParameteri))loadFn("glPatchParameteri");
	proc_glPauseTransformFeedback = (decltype(proc_glPauseTransformFeedback))loadFn("glPauseTransformFeedback");
	proc_glPixelStoref = (decltype(proc_glPixelStoref))loadFn("glPixelStoref");
	proc_glPixelStorei = (decltype(proc_glPixelStorei))loadFn("glPixelStorei");
	proc_glPointParameterf = (decltype(proc_glPointParameterf))loadFn("glPointParameterf");
	proc_glPointParameterfv = (decltype(proc_glPointParameterfv))loadFn("glPointParameterfv");
	proc_glPointParameteri = (decltype(proc_glPointParameteri))loadFn("glPointParameteri");
	proc_glPointParameteriv = (decltype(proc_glPointParameteriv))loadFn("glPointParameteriv");
	proc_glPointSize = (decltype(proc_glPointSize))loadFn("glPointSize");
	proc_glPolygonMode = (decltype(proc_glPolygonMode))loadFn("glPolygonMode");
	proc_glPolygonOffset = (decltype(proc_glPolygonOffset))loadFn("glPolygonOffset");
	proc_glPopDebugGroup = (decltype(proc_glPopDebugGroup))loadFn("glPopDebugGroup");
	proc_glPrimitiveRestartIndex = (decltype(proc_glPrimitiveRestartIndex))loadFn("glPrimitiveRestartIndex");
	proc_glProgramBinary = (decltype(proc_glProgramBinary))loadFn("glProgramBinary");
	proc_glProgramParameteri = (decltype(proc_glProgramParameteri))loadFn("glProgramParameteri");
	proc_glProgramUniform1d = (decltype(proc_glProgramUniform1d))loadFn("glProgramUniform1d");
	proc_glProgramUniform1dv = (decltype(proc_glProgramUniform1dv))loadFn("glProgramUniform1dv");
	proc_glProgramUniform1f = (decltype(proc_glProgramUniform1f))loadFn("glProgramUniform1f");
	proc_glProgramUniform1fv = (decltype(proc_glProgramUniform1fv))loadFn("glProgramUniform1fv");
	proc_glProgramUniform1i = (decltype(proc_glProgramUniform1i))loadFn("glProgramUniform1i");
	proc_glProgramUniform1iv = (decltype(proc_glProgramUniform1iv))loadFn("glProgramUniform1iv");
	proc_glProgramUniform1ui = (decltype(proc_glProgramUniform1ui))loadFn("glProgramUniform1ui");
	proc_glProgramUniform1uiv = (decltype(proc_glProgramUniform1uiv))loadFn("glProgramUniform1uiv");
	proc_glProgramUniform2d = (decltype(proc_glProgramUniform2d))loadFn("glProgramUniform2d");
	proc_glProgramUniform2dv = (decltype(proc_glProgramUniform2dv))loadFn("glProgramUniform2dv");
	proc_glProgramUniform2f = (decltype(proc_glProgramUniform2f))loadFn("glProgramUniform2f");
	proc_glProgramUniform2fv = (decltype(proc_glProgramUniform2fv))loadFn("glProgramUniform2fv");
	proc_glProgramUniform2i = (decltype(proc_glProgramUniform2i))loadFn("glProgramUniform2i");
	proc_glProgramUniform2iv = (decltype(proc_glProgramUniform2iv))loadFn("glProgramUniform2iv");
	proc_glProgramUniform2ui = (decltype(proc_glProgramUniform2ui))loadFn("glProgramUniform2ui");
	proc_glProgramUniform2uiv = (decltype(proc_glProgramUniform2uiv))loadFn("glProgramUniform2uiv");
	proc_glProgramUniform3d = (decltype(proc_glProgramUniform3d))loadFn("glProgramUniform3d");
	proc_glProgramUniform3dv = (decltype(proc_glProgramUniform3dv))loadFn("glProgramUniform3dv");
	proc_glProgramUniform3f = (decltype(proc_glProgramUniform3f))loadFn("glProgramUniform3f");
	proc_glProgramUniform3fv = (decltype(proc_glProgramUniform3fv))loadFn("glProgramUniform3fv");
	proc_glProgramUniform3i = (decltype(proc_glProgramUniform3i))loadFn("glProgramUniform3i");
	proc_glProgramUniform3iv = (decltype(proc_glProgramUniform3iv))loadFn("glProgramUniform3iv");
	proc_glProgramUniform3ui = (decltype(proc_glProgramUniform3ui))loadFn("glProgramUniform3ui");
	proc_glProgramUniform3uiv = (decltype(proc_glProgramUniform3uiv))loadFn("glProgramUniform3uiv");
	proc_glProgramUniform4d = (decltype(proc_glProgramUniform4d))loadFn("glProgramUniform4d");
	proc_glProgramUniform4dv = (decltype(proc_glProgramUniform4dv))loadFn("glProgramUniform4dv");
	proc_glProgramUniform4f = (decltype(proc_glProgramUniform4f))loadFn("glProgramUniform4f");
	proc_glProgramUniform4fv = (decltype(proc_glProgramUniform4fv))loadFn("glProgramUniform4fv");
	proc_glProgramUniform4i = (decltype(proc_glProgramUniform4i))loadFn("glProgramUniform4i");
	proc_glProgramUniform4iv = (decltype(proc_glProgramUniform4iv))loadFn("glProgramUniform4iv");
	proc_glProgramUniform4ui = (decltype(proc_glProgramUniform4ui))loadFn("glProgramUniform4ui");
	proc_glProgramUniform4uiv = (decltype(proc_glProgramUniform4uiv))loadFn("glProgramUniform4uiv");
	proc_glProgramUniformMatrix2dv = (decltype(proc_glProgramUniformMatrix2dv))loadFn("glProgramUniformMatrix2dv");
	proc_glProgramUniformMatrix2fv = (decltype(proc_glProgramUniformMatrix2fv))loadFn("glProgramUniformMatrix2fv");
	proc_glProgramUniformMatrix2x3dv = (decltype(proc_glProgramUniformMatrix2x3dv))loadFn("glProgramUniformMatrix2x3dv");
	proc_glProgramUniformMatrix2x3fv = (decltype(proc_glProgramUniformMatrix2x3fv))loadFn("glProgramUniformMatrix2x3fv");
	proc_glProgramUniformMatrix2x4dv = (decltype(proc_glProgramUniformMatrix2x4dv))loadFn("glProgramUniformMatrix2x4dv");
	proc_glProgramUniformMatrix2x4fv = (decltype(proc_glProgramUniformMatrix2x4fv))loadFn("glProgramUniformMatrix2x4fv");
	proc_glProgramUniformMatrix3dv = (decltype(proc_glProgramUniformMatrix3dv))loadFn("glProgramUniformMatrix3dv");
	proc_glProgramUniformMatrix3fv = (decltype(proc_glProgramUniformMatrix3fv))loadFn("glProgramUniformMatrix3fv");
	proc_glProgramUniformMatrix3x2dv = (decltype(proc_glProgramUniformMatrix3x2dv))loadFn("glProgramUniformMatrix3x2dv");
	proc_glProgramUniformMatrix3x2fv = (decltype(proc_glProgramUniformMatrix3x2fv))loadFn("glProgramUniformMatrix3x2fv");
	proc_glProgramUniformMatrix3x4dv = (decltype(proc_glProgramUniformMatrix3x4dv))loadFn("glProgramUniformMatrix3x4dv");
	proc_glProgramUniformMatrix3x4fv = (decltype(proc_glProgramUniformMatrix3x4fv))loadFn("glProgramUniformMatrix3x4fv");
	proc_glProgramUniformMatrix4dv = (decltype(proc_glProgramUniformMatrix4dv))loadFn("glProgramUniformMatrix4dv");
	proc_glProgramUniformMatrix4fv = (decltype(proc_glProgramUniformMatrix4fv))loadFn("glProgramUniformMatrix4fv");
	proc_glProgramUniformMatrix4x2dv = (decltype(proc_glProgramUniformMatrix4x2dv))loadFn("glProgramUniformMatrix4x2dv");
	proc_glProgramUniformMatrix4x2fv = (decltype(proc_glProgramUniformMatrix4x2fv))loadFn("glProgramUniformMatrix4x2fv");
	proc_glProgramUniformMatrix4x3dv = (decltype(proc_glProgramUniformMatrix4x3dv))loadFn("glProgramUniformMatrix4x3dv");
	proc_glProgramUniformMatrix4x3fv = (decltype(proc_glProgramUniformMatrix4x3fv))loadFn("glProgramUniformMatrix4x3fv");
	proc_glProvokingVertex = (decltype(proc_glProvokingVertex))loadFn("glProvokingVertex");
	proc_glPushDebugGroup = (decltype(proc_glPushDebugGroup))loadFn("glPushDebugGroup");
	proc_glQueryCounter = (decltype(proc_glQueryCounter))loadFn("glQueryCounter");
	proc_glReadBuffer = (decltype(proc_glReadBuffer))loadFn("glReadBuffer");
	proc_glReadPixels = (decltype(proc_glReadPixels))loadFn("glReadPixels");
	proc_glReadnPixels = (decltype(proc_glReadnPixels))loadFn("glReadnPixels");
	proc_glReleaseShaderCompiler = (decltype(proc_glReleaseShaderCompiler))loadFn("glReleaseShaderCompiler");
	proc_glRenderbufferStorage = (decltype(proc_glRenderbufferStorage))loadFn("glRenderbufferStorage");
	proc_glRenderbufferStorageMultisample = (decltype(proc_glRenderbufferStorageMultisample))loadFn("glRenderbufferStorageMultisample");
	proc_glResumeTransformFeedback = (decltype(proc_glResumeTransformFeedback))loadFn("glResumeTransformFeedback");
	proc_glSampleCoverage = (decltype(proc_glSampleCoverage))loadFn("glSampleCoverage");
	proc_glSampleMaski = (decltype(proc_glSampleMaski))loadFn("glSampleMaski");
	proc_glSamplerParameterIiv = (decltype(proc_glSamplerParameterIiv))loadFn("glSamplerParameterIiv");
	proc_glSamplerParameterIuiv = (decltype(proc_glSamplerParameterIuiv))loadFn("glSamplerParameterIuiv");
	proc_glSamplerParameterf = (decltype(proc_glSamplerParameterf))loadFn("glSamplerParameterf");
	proc_glSamplerParameterfv = (decltype(proc_glSamplerParameterfv))loadFn("glSamplerParameterfv");
	proc_glSamplerParameteri = (decltype(proc_glSamplerParameteri))loadFn("glSamplerParameteri");
	proc_glSamplerParameteriv = (decltype(proc_glSamplerParameteriv))loadFn("glSamplerParameteriv");
	proc_glScissor = (decltype(proc_glScissor))loadFn("glScissor");
	proc_glScissorArrayv = (decltype(proc_glScissorArrayv))loadFn("glScissorArrayv");
	proc_glScissorIndexed = (decltype(proc_glScissorIndexed))loadFn("glScissorIndexed");
	proc_glScissorIndexedv = (decltype(proc_glScissorIndexedv))loadFn("glScissorIndexedv");
	proc_glShaderBinary = (decltype(proc_glShaderBinary))loadFn("glShaderBinary");
	proc_glShaderSource = (decltype(proc_glShaderSource))loadFn("glShaderSource");
	proc_glShaderStorageBlockBinding = (decltype(proc_glShaderStorageBlockBinding))loadFn("glShaderStorageBlockBinding");
	proc_glStencilFunc = (decltype(proc_glStencilFunc))loadFn("glStencilFunc");
	proc_glStencilFuncSeparate = (decltype(proc_glStencilFuncSeparate))loadFn("glStencilFuncSeparate");
	proc_glStencilMask = (decltype(proc_glStencilMask))loadFn("glStencilMask");
	proc_glStencilMaskSeparate = (decltype(proc_glStencilMaskSeparate))loadFn("glStencilMaskSeparate");
	proc_glStencilOp = (decltype(proc_glStencilOp))loadFn("glStencilOp");
	proc_glStencilOpSeparate = (decltype(proc_glStencilOpSeparate))loadFn("glStencilOpSeparate");
	proc_glTexBuffer = (decltype(proc_glTexBuffer))loadFn("glTexBuffer");
	proc_glTexBufferRange = (decltype(proc_glTexBufferRange))loadFn("glTexBufferRange");
	proc_glTexImage1D = (decltype(proc_glTexImage1D))loadFn("glTexImage1D");
	proc_glTexImage2D = (decltype(proc_glTexImage2D))loadFn("glTexImage2D");
	proc_glTexImage2DMultisample = (decltype(proc_glTexImage2DMultisample))loadFn("glTexImage2DMultisample");
	proc_glTexImage3D = (decltype(proc_glTexImage3D))loadFn("glTexImage3D");
	proc_glTexImage3DMultisample = (decltype(proc_glTexImage3DMultisample))loadFn("glTexImage3DMultisample");
	proc_glTexParameterIiv = (decltype(proc_glTexParameterIiv))loadFn("glTexParameterIiv");
	proc_glTexParameterIuiv = (decltype(proc_glTexParameterIuiv))loadFn("glTexParameterIuiv");
	proc_glTexParameterf = (decltype(proc_glTexParameterf))loadFn("glTexParameterf");
	proc_glTexParameterfv = (decltype(proc_glTexParameterfv))loadFn("glTexParameterfv");
	proc_glTexParameteri = (decltype(proc_glTexParameteri))loadFn("glTexParameteri");
	proc_glTexParameteriv = (decltype(proc_glTexParameteriv))loadFn("glTexParameteriv");
	proc_glTexStorage1D = (decltype(proc_glTexStorage1D))loadFn("glTexStorage1D");
	proc_glTexStorage2D = (decltype(proc_glTexStorage2D))loadFn("glTexStorage2D");
	proc_glTexStorage2DMultisample = (decltype(proc_glTexStorage2DMultisample))loadFn("glTexStorage2DMultisample");
	proc_glTexStorage3D = (decltype(proc_glTexStorage3D))loadFn("glTexStorage3D");
	proc_glTexStorage3DMultisample = (decltype(proc_glTexStorage3DMultisample))loadFn("glTexStorage3DMultisample");
	proc_glTexSubImage1D = (decltype(proc_glTexSubImage1D))loadFn("glTexSubImage1D");
	proc_glTexSubImage2D = (decltype(proc_glTexSubImage2D))loadFn("glTexSubImage2D");
	proc_glTexSubImage3D = (decltype(proc_glTexSubImage3D))loadFn("glTexSubImage3D");
	proc_glTextureBarrier = (decltype(proc_glTextureBarrier))loadFn("glTextureBarrier");
	proc_glTextureBuffer = (decltype(proc_glTextureBuffer))loadFn("glTextureBuffer");
	proc_glTextureBufferRange = (decltype(proc_glTextureBufferRange))loadFn("glTextureBufferRange");
	proc_glTextureParameterIiv = (decltype(proc_glTextureParameterIiv))loadFn("glTextureParameterIiv");
	proc_glTextureParameterIuiv = (decltype(proc_glTextureParameterIuiv))loadFn("glTextureParameterIuiv");
	proc_glTextureParameterf = (decltype(proc_glTextureParameterf))loadFn("glTextureParameterf");
	proc_glTextureParameterfv = (decltype(proc_glTextureParameterfv))loadFn("glTextureParameterfv");
	proc_glTextureParameteri = (decltype(proc_glTextureParameteri))loadFn("glTextureParameteri");
	proc_glTextureParameteriv = (decltype(proc_glTextureParameteriv))loadFn("glTextureParameteriv");
	proc_glTextureStorage1D = (decltype(proc_glTextureStorage1D))loadFn("glTextureStorage1D");
	proc_glTextureStorage2D = (decltype(proc_glTextureStorage2D))loadFn("glTextureStorage2D");
	proc_glTextureStorage2DMultisample = (decltype(proc_glTextureStorage2DMultisample))loadFn("glTextureStorage2DMultisample");
	proc_glTextureStorage3D = (decltype(proc_glTextureStorage3D))loadFn("glTextureStorage3D");
	proc_glTextureStorage3DMultisample = (decltype(proc_glTextureStorage3DMultisample))loadFn("glTextureStorage3DMultisample");
	proc_glTextureSubImage1D = (decltype(proc_glTextureSubImage1D))loadFn("glTextureSubImage1D");
	proc_glTextureSubImage2D = (decltype(proc_glTextureSubImage2D))loadFn("glTextureSubImage2D");
	proc_glTextureSubImage3D = (decltype(proc_glTextureSubImage3D))loadFn("glTextureSubImage3D");
	proc_glTextureView = (decltype(proc_glTextureView))loadFn("glTextureView");
	proc_glTransformFeedbackBufferBase = (decltype(proc_glTransformFeedbackBufferBase))loadFn("glTransformFeedbackBufferBase");
	proc_glTransformFeedbackBufferRange = (decltype(proc_glTransformFeedbackBufferRange))loadFn("glTransformFeedbackBufferRange");
	proc_glTransformFeedbackVaryings = (decltype(proc_glTransformFeedbackVaryings))loadFn("glTransformFeedbackVaryings");
	proc_glUniform1d = (decltype(proc_glUniform1d))loadFn("glUniform1d");
	proc_glUniform1dv = (decltype(proc_glUniform1dv))loadFn("glUniform1dv");
	proc_glUniform1f = (decltype(proc_glUniform1f))loadFn("glUniform1f");
	proc_glUniform1fv = (decltype(proc_glUniform1fv))loadFn("glUniform1fv");
	proc_glUniform1i = (decltype(proc_glUniform1i))loadFn("glUniform1i");
	proc_glUniform1iv = (decltype(proc_glUniform1iv))loadFn("glUniform1iv");
	proc_glUniform1ui = (decltype(proc_glUniform1ui))loadFn("glUniform1ui");
	proc_glUniform1uiv = (decltype(proc_glUniform1uiv))loadFn("glUniform1uiv");
	proc_glUniform2d = (decltype(proc_glUniform2d))loadFn("glUniform2d");
	proc_glUniform2dv = (decltype(proc_glUniform2dv))loadFn("glUniform2dv");
	proc_glUniform2f = (decltype(proc_glUniform2f))loadFn("glUniform2f");
	proc_glUniform2fv = (decltype(proc_glUniform2fv))loadFn("glUniform2fv");
	proc_glUniform2i = (decltype(proc_glUniform2i))loadFn("glUniform2i");
	proc_glUniform2iv = (decltype(proc_glUniform2iv))loadFn("glUniform2iv");
	proc_glUniform2ui = (decltype(proc_glUniform2ui))loadFn("glUniform2ui");
	proc_glUniform2uiv = (decltype(proc_glUniform2uiv))loadFn("glUniform2uiv");
	proc_glUniform3d = (decltype(proc_glUniform3d))loadFn("glUniform3d");
	proc_glUniform3dv = (decltype(proc_glUniform3dv))loadFn("glUniform3dv");
	proc_glUniform3f = (decltype(proc_glUniform3f))loadFn("glUniform3f");
	proc_glUniform3fv = (decltype(proc_glUniform3fv))loadFn("glUniform3fv");
	proc_glUniform3i = (decltype(proc_glUniform3i))loadFn("glUniform3i");
	proc_glUniform3iv = (decltype(proc_glUniform3iv))loadFn("glUniform3iv");
	proc_glUniform3ui = (decltype(proc_glUniform3ui))loadFn("glUniform3ui");
	proc_glUniform3uiv = (decltype(proc_glUniform3uiv))loadFn("glUniform3uiv");
	proc_glUniform4d = (decltype(proc_glUniform4d))loadFn("glUniform4d");
	proc_glUniform4dv = (decltype(proc_glUniform4dv))loadFn("glUniform4dv");
	proc_glUniform4f = (decltype(proc_glUniform4f))loadFn("glUniform4f");
	proc_glUniform4fv = (decltype(proc_glUniform4fv))loadFn("glUniform4fv");
	proc_glUniform4i = (decltype(proc_glUniform4i))loadFn("glUniform4i");
	proc_glUniform4iv = (decltype(proc_glUniform4iv))loadFn("glUniform4iv");
	proc_glUniform4ui = (decltype(proc_glUniform4ui))loadFn("glUniform4ui");
	proc_glUniform4uiv = (decltype(proc_glUniform4uiv))loadFn("glUniform4uiv");
	proc_glUniformBlockBinding = (decltype(proc_glUniformBlockBinding))loadFn("glUniformBlockBinding");
	proc_glUniformMatrix2dv = (decltype(proc_glUniformMatrix2dv))loadFn("glUniformMatrix2dv");
	proc_glUniformMatrix2fv = (decltype(proc_glUniformMatrix2fv))loadFn("glUniformMatrix2fv");
	proc_glUniformMatrix2x3dv = (decltype(proc_glUniformMatrix2x3dv))loadFn("glUniformMatrix2x3dv");
	proc_glUniformMatrix2x3fv = (decltype(proc_glUniformMatrix2x3fv))loadFn("glUniformMatrix2x3fv");
	proc_glUniformMatrix2x4dv = (decltype(proc_glUniformMatrix2x4dv))loadFn("glUniformMatrix2x4dv");
	proc_glUniformMatrix2x4fv = (decltype(proc_glUniformMatrix2x4fv))loadFn("glUniformMatrix2x4fv");
	proc_glUniformMatrix3dv = (decltype(proc_glUniformMatrix3dv))loadFn("glUniformMatrix3dv");
	proc_glUniformMatrix3fv = (decltype(proc_glUniformMatrix3fv))loadFn("glUniformMatrix3fv");
	proc_glUniformMatrix3x2dv = (decltype(proc_glUniformMatrix3x2dv))loadFn("glUniformMatrix3x2dv");
	proc_glUniformMatrix3x2fv = (decltype(proc_glUniformMatrix3x2fv))loadFn("glUniformMatrix3x2fv");
	proc_glUniformMatrix3x4dv = (decltype(proc_glUniformMatrix3x4dv))loadFn("glUniformMatrix3x4dv");
	proc_glUniformMatrix3x4fv = (decltype(proc_glUniformMatrix3x4fv))loadFn("glUniformMatrix3x4fv");
	proc_glUniformMatrix4dv = (decltype(proc_glUniformMatrix4dv))loadFn("glUniformMatrix4dv");
	proc_glUniformMatrix4fv = (decltype(proc_glUniformMatrix4fv))loadFn("glUniformMatrix4fv");
	proc_glUniformMatrix4x2dv = (decltype(proc_glUniformMatrix4x2dv))loadFn("glUniformMatrix4x2dv");
	proc_glUniformMatrix4x2fv = (decltype(proc_glUniformMatrix4x2fv))loadFn("glUniformMatrix4x2fv");
	proc_glUniformMatrix4x3dv = (decltype(proc_glUniformMatrix4x3dv))loadFn("glUniformMatrix4x3dv");
	proc_glUniformMatrix4x3fv = (decltype(proc_glUniformMatrix4x3fv))loadFn("glUniformMatrix4x3fv");
	proc_glUniformSubroutinesuiv = (decltype(proc_glUniformSubroutinesuiv))loadFn("glUniformSubroutinesuiv");
	proc_glUnmapBuffer = (decltype(proc_glUnmapBuffer))loadFn("glUnmapBuffer");
	proc_glUnmapNamedBuffer = (decltype(proc_glUnmapNamedBuffer))loadFn("glUnmapNamedBuffer");
	proc_glUseProgram = (decltype(proc_glUseProgram))loadFn("glUseProgram");
	proc_glUseProgramStages = (decltype(proc_glUseProgramStages))loadFn("glUseProgramStages");
	proc_glValidateProgram = (decltype(proc_glValidateProgram))loadFn("glValidateProgram");
	proc_glValidateProgramPipeline = (decltype(proc_glValidateProgramPipeline))loadFn("glValidateProgramPipeline");
	proc_glVertexArrayAttribBinding = (decltype(proc_glVertexArrayAttribBinding))loadFn("glVertexArrayAttribBinding");
	proc_glVertexArrayAttribFormat = (decltype(proc_glVertexArrayAttribFormat))loadFn("glVertexArrayAttribFormat");
	proc_glVertexArrayAttribIFormat = (decltype(proc_glVertexArrayAttribIFormat))loadFn("glVertexArrayAttribIFormat");
	proc_glVertexArrayAttribLFormat = (decltype(proc_glVertexArrayAttribLFormat))loadFn("glVertexArrayAttribLFormat");
	proc_glVertexArrayBindingDivisor = (decltype(proc_glVertexArrayBindingDivisor))loadFn("glVertexArrayBindingDivisor");
	proc_glVertexArrayElementBuffer = (decltype(proc_glVertexArrayElementBuffer))loadFn("glVertexArrayElementBuffer");
	proc_glVertexArrayVertexBuffer = (decltype(proc_glVertexArrayVertexBuffer))loadFn("glVertexArrayVertexBuffer");
	proc_glVertexArrayVertexBuffers = (decltype(proc_glVertexArrayVertexBuffers))loadFn("glVertexArrayVertexBuffers");
	proc_glVertexAttrib1d = (decltype(proc_glVertexAttrib1d))loadFn("glVertexAttrib1d");
	proc_glVertexAttrib1dv = (decltype(proc_glVertexAttrib1dv))loadFn("glVertexAttrib1dv");
	proc_glVertexAttrib1f = (decltype(proc_glVertexAttrib1f))loadFn("glVertexAttrib1f");
	proc_glVertexAttrib1fv = (decltype(proc_glVertexAttrib1fv))loadFn("glVertexAttrib1fv");
	proc_glVertexAttrib1s = (decltype(proc_glVertexAttrib1s))loadFn("glVertexAttrib1s");
	proc_glVertexAttrib1sv = (decltype(proc_glVertexAttrib1sv))loadFn("glVertexAttrib1sv");
	proc_glVertexAttrib2d = (decltype(proc_glVertexAttrib2d))loadFn("glVertexAttrib2d");
	proc_glVertexAttrib2dv = (decltype(proc_glVertexAttrib2dv))loadFn("glVertexAttrib2dv");
	proc_glVertexAttrib2f = (decltype(proc_glVertexAttrib2f))loadFn("glVertexAttrib2f");
	proc_glVertexAttrib2fv = (decltype(proc_glVertexAttrib2fv))loadFn("glVertexAttrib2fv");
	proc_glVertexAttrib2s = (decltype(proc_glVertexAttrib2s))loadFn("glVertexAttrib2s");
	proc_glVertexAttrib2sv = (decltype(proc_glVertexAttrib2sv))loadFn("glVertexAttrib2sv");
	proc_glVertexAttrib3d = (decltype(proc_glVertexAttrib3d))loadFn("glVertexAttrib3d");
	proc_glVertexAttrib3dv = (decltype(proc_glVertexAttrib3dv))loadFn("glVertexAttrib3dv");
	proc_glVertexAttrib3f = (decltype(proc_glVertexAttrib3f))loadFn("glVertexAttrib3f");
	proc_glVertexAttrib3fv = (decltype(proc_glVertexAttrib3fv))loadFn("glVertexAttrib3fv");
	proc_glVertexAttrib3s = (decltype(proc_glVertexAttrib3s))loadFn("glVertexAttrib3s");
	proc_glVertexAttrib3sv = (decltype(proc_glVertexAttrib3sv))loadFn("glVertexAttrib3sv");
	proc_glVertexAttrib4Nbv = (decltype(proc_glVertexAttrib4Nbv))loadFn("glVertexAttrib4Nbv");
	proc_glVertexAttrib4Niv = (decltype(proc_glVertexAttrib4Niv))loadFn("glVertexAttrib4Niv");
	proc_glVertexAttrib4Nsv = (decltype(proc_glVertexAttrib4Nsv))loadFn("glVertexAttrib4Nsv");
	proc_glVertexAttrib4Nub = (decltype(proc_glVertexAttrib4Nub))loadFn("glVertexAttrib4Nub");
	proc_glVertexAttrib4Nubv = (decltype(proc_glVertexAttrib4Nubv))loadFn("glVertexAttrib4Nubv");
	proc_glVertexAttrib4Nuiv = (decltype(proc_glVertexAttrib4Nuiv))loadFn("glVertexAttrib4Nuiv");
	proc_glVertexAttrib4Nusv = (decltype(proc_glVertexAttrib4Nusv))loadFn("glVertexAttrib4Nusv");
	proc_glVertexAttrib4bv = (decltype(proc_glVertexAttrib4bv))loadFn("glVertexAttrib4bv");
	proc_glVertexAttrib4d = (decltype(proc_glVertexAttrib4d))loadFn("glVertexAttrib4d");
	proc_glVertexAttrib4dv = (decltype(proc_glVertexAttrib4dv))loadFn("glVertexAttrib4dv");
	proc_glVertexAttrib4f = (decltype(proc_glVertexAttrib4f))loadFn("glVertexAttrib4f");
	proc_glVertexAttrib4fv = (decltype(proc_glVertexAttrib4fv))loadFn("glVertexAttrib4fv");
	proc_glVertexAttrib4iv = (decltype(proc_glVertexAttrib4iv))loadFn("glVertexAttrib4iv");
	proc_glVertexAttrib4s = (decltype(proc_glVertexAttrib4s))loadFn("glVertexAttrib4s");
	proc_glVertexAttrib4sv = (decltype(proc_glVertexAttrib4sv))loadFn("glVertexAttrib4sv");
	proc_glVertexAttrib4ubv = (decltype(proc_glVertexAttrib4ubv))loadFn("glVertexAttrib4ubv");
	proc_glVertexAttrib4uiv = (decltype(proc_glVertexAttrib4uiv))loadFn("glVertexAttrib4uiv");
	proc_glVertexAttrib4usv = (decltype(proc_glVertexAttrib4usv))loadFn("glVertexAttrib4usv");
	proc_glVertexAttribBinding = (decltype(proc_glVertexAttribBinding))loadFn("glVertexAttribBinding");
	proc_glVertexAttribDivisor = (decltype(proc_glVertexAttribDivisor))loadFn("glVertexAttribDivisor");
	proc_glVertexAttribFormat = (decltype(proc_glVertexAttribFormat))loadFn("glVertexAttribFormat");
	proc_glVertexAttribI1i = (decltype(proc_glVertexAttribI1i))loadFn("glVertexAttribI1i");
	proc_glVertexAttribI1iv = (decltype(proc_glVertexAttribI1iv))loadFn("glVertexAttribI1iv");
	proc_glVertexAttribI1ui = (decltype(proc_glVertexAttribI1ui))loadFn("glVertexAttribI1ui");
	proc_glVertexAttribI1uiv = (decltype(proc_glVertexAttribI1uiv))loadFn("glVertexAttribI1uiv");
	proc_glVertexAttribI2i = (decltype(proc_glVertexAttribI2i))loadFn("glVertexAttribI2i");
	proc_glVertexAttribI2iv = (decltype(proc_glVertexAttribI2iv))loadFn("glVertexAttribI2iv");
	proc_glVertexAttribI2ui = (decltype(proc_glVertexAttribI2ui))loadFn("glVertexAttribI2ui");
	proc_glVertexAttribI2uiv = (decltype(proc_glVertexAttribI2uiv))loadFn("glVertexAttribI2uiv");
	proc_glVertexAttribI3i = (decltype(proc_glVertexAttribI3i))loadFn("glVertexAttribI3i");
	proc_glVertexAttribI3iv = (decltype(proc_glVertexAttribI3iv))loadFn("glVertexAttribI3iv");
	proc_glVertexAttribI3ui = (decltype(proc_glVertexAttribI3ui))loadFn("glVertexAttribI3ui");
	proc_glVertexAttribI3uiv = (decltype(proc_glVertexAttribI3uiv))loadFn("glVertexAttribI3uiv");
	proc_glVertexAttribI4bv = (decltype(proc_glVertexAttribI4bv))loadFn("glVertexAttribI4bv");
	proc_glVertexAttribI4i = (decltype(proc_glVertexAttribI4i))loadFn("glVertexAttribI4i");
	proc_glVertexAttribI4iv = (decltype(proc_glVertexAttribI4iv))loadFn("glVertexAttribI4iv");
	proc_glVertexAttribI4sv = (decltype(proc_glVertexAttribI4sv))loadFn("glVertexAttribI4sv");
	proc_glVertexAttribI4ubv = (decltype(proc_glVertexAttribI4ubv))loadFn("glVertexAttribI4ubv");
	proc_glVertexAttribI4ui = (decltype(proc_glVertexAttribI4ui))loadFn("glVertexAttribI4ui");
	proc_glVertexAttribI4uiv = (decltype(proc_glVertexAttribI4uiv))loadFn("glVertexAttribI4uiv");
	proc_glVertexAttribI4usv = (decltype(proc_glVertexAttribI4usv))loadFn("glVertexAttribI4usv");
	proc_glVertexAttribIFormat = (decltype(proc_glVertexAttribIFormat))loadFn("glVertexAttribIFormat");
	proc_glVertexAttribIPointer = (decltype(proc_glVertexAttribIPointer))loadFn("glVertexAttribIPointer");
	proc_glVertexAttribL1d = (decltype(proc_glVertexAttribL1d))loadFn("glVertexAttribL1d");
	proc_glVertexAttribL1dv = (decltype(proc_glVertexAttribL1dv))loadFn("glVertexAttribL1dv");
	proc_glVertexAttribL2d = (decltype(proc_glVertexAttribL2d))loadFn("glVertexAttribL2d");
	proc_glVertexAttribL2dv = (decltype(proc_glVertexAttribL2dv))loadFn("glVertexAttribL2dv");
	proc_glVertexAttribL3d = (decltype(proc_glVertexAttribL3d))loadFn("glVertexAttribL3d");
	proc_glVertexAttribL3dv = (decltype(proc_glVertexAttribL3dv))loadFn("glVertexAttribL3dv");
	proc_glVertexAttribL4d = (decltype(proc_glVertexAttribL4d))loadFn("glVertexAttribL4d");
	proc_glVertexAttribL4dv = (decltype(proc_glVertexAttribL4dv))loadFn("glVertexAttribL4dv");
	proc_glVertexAttribLFormat = (decltype(proc_glVertexAttribLFormat))loadFn("glVertexAttribLFormat");
	proc_glVertexAttribLPointer = (decltype(proc_glVertexAttribLPointer))loadFn("glVertexAttribLPointer");
	proc_glVertexAttribP1ui = (decltype(proc_glVertexAttribP1ui))loadFn("glVertexAttribP1ui");
	proc_glVertexAttribP1uiv = (decltype(proc_glVertexAttribP1uiv))loadFn("glVertexAttribP1uiv");
	proc_glVertexAttribP2ui = (decltype(proc_glVertexAttribP2ui))loadFn("glVertexAttribP2ui");
	proc_glVertexAttribP2uiv = (decltype(proc_glVertexAttribP2uiv))loadFn("glVertexAttribP2uiv");
	proc_glVertexAttribP3ui = (decltype(proc_glVertexAttribP3ui))loadFn("glVertexAttribP3ui");
	proc_glVertexAttribP3uiv = (decltype(proc_glVertexAttribP3uiv))loadFn("glVertexAttribP3uiv");
	proc_glVertexAttribP4ui = (decltype(proc_glVertexAttribP4ui))loadFn("glVertexAttribP4ui");
	proc_glVertexAttribP4uiv = (decltype(proc_glVertexAttribP4uiv))loadFn("glVertexAttribP4uiv");
	proc_glVertexAttribPointer = (decltype(proc_glVertexAttribPointer))loadFn("glVertexAttribPointer");
	proc_glVertexBindingDivisor = (decltype(proc_glVertexBindingDivisor))loadFn("glVertexBindingDivisor");
	proc_glViewport = (decltype(proc_glViewport))loadFn("glViewport");
	proc_glViewportArrayv = (decltype(proc_glViewportArrayv))loadFn("glViewportArrayv");
	proc_glViewportIndexedf = (decltype(proc_glViewportIndexedf))loadFn("glViewportIndexedf");
	proc_glViewportIndexedfv = (decltype(proc_glViewportIndexedfv))loadFn("glViewportIndexedfv");
	proc_glWaitSync = (decltype(proc_glWaitSync))loadFn("glWaitSync");
}
