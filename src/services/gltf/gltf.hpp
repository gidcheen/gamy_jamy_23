#pragma once

#include <vector>
#include <filesystem>
#include <algebra/algebra.hpp>

namespace gltf
{
	struct GltfMesh final
	{
		std::vector<algebra::Vector2> vertices;
	};

	std::vector<GltfMesh> LoadGltfMesh(std::filesystem::path path);
}
