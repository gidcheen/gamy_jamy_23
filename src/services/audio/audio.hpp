#pragma once

#include <string>
#include <filesystem>
#include <map>

struct ma_engine;
struct ma_sound;

namespace audio
{
	class Audio final
	{
	public:
		Audio();
		~Audio();

		void Oneshot(const std::string &path);
		void Play(const std::string &name);
		void Loop(const std::string &name);
		void SetVolume(const std::string &name, float volume);

	private:
		ma_engine *engine;

		ma_sound *InitSound(const std::string &path);
		void PlayOrLoop(const std::string &name, bool loop);
	};
}
