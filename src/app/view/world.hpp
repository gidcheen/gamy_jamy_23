#pragma once

#include <vector>
#include <memory>
#include <graphics/renderer.hpp>
#include <box2d/box2d.h>

#include "planet.hpp"
#include "tree.hpp"
#include "sun.hpp"

#include <state/world.hpp>
#include <audio/audio.hpp>

namespace view
{
	class WorldView final
	{
	private:
		audio::Audio &audio;
		graphics::Camera camera;
		float cameraRot = 0;
		algebra::Matrix3x3 cameraTransform = algebra::Matrix3x3::Identity();

		state::World &world;

		algebra::Vector2 mousePosScreenSpace;
		algebra::Vector3 mousePosWorldSpace;
		SunView sun;
		std::vector<std::unique_ptr<PlanetView>> planets;
		std::vector<std::unique_ptr<TreeView>> trees;

		int treeAmount = 0;
		int rootAmount = 0;

	public:
		WorldView(state::World &world, audio::Audio &audio);

		void SetScreenSize(algebra::Vector2 size) { camera.frameSize = size; }

		void Update(float dt);
		void MouseMove(algebra::Vector2 pos);
		void MoveSun(float amout, float dt);
		void MouseUp();
		void Render(graphics::Renderer &renderer);
	};
}
