#include "world.hpp"

#include <string>
#include <gltf/gltf.hpp>

#include <iostream>

namespace state
{
	World::World(int levelIndex)
		: levelIndex(levelIndex)
	{
		physicsWorld = std::make_unique<b2World>(b2Vec2{0, 0});

		std::string filePath = std::string("meshes/planets/planet0") + std::to_string(levelIndex) + ".gltf";
		std::vector<gltf::GltfMesh> gltfMesh = gltf::LoadGltfMesh(filePath);

		for (gltf::GltfMesh &mesh : gltfMesh)
		{
			planets.push_back(std::make_shared<Planet>(mesh.vertices, physicsWorld.get()));
		}
	}

	struct RayCb : public b2RayCastCallback
	{
		World *w;

		float ReportFixture(
			b2Fixture *fixture,
			const b2Vec2 &point,
			const b2Vec2 &normal, float fraction) override
		{
			Planet *collision_planet = (Planet *)fixture->GetBody()->GetUserData().pointer;

			// check if we even should spawn a new tree
			for (auto &tree : w->GetTrees())
			{
				if (!tree->is_root)
				{
					// we got an active tree. nevermind then
					return 0;
				}
			}

			for (auto &planet : w->GetPlanets())
			{
				if (planet.get() == collision_planet)
				{
					// spawn new tree at point
					w->SpawnTreeAtPoint({point.x, point.y}, {normal.x, normal.y}, planet);
				}
			}
			return 0;
		}
		RayCb(World *w) : w(w) {}
	};

	void World::SpawnTreeAtPoint(algebra::Vector2 point, algebra::Vector2 direction, std::shared_ptr<Planet> planet)
	{
		trees.push_back(std::make_shared<Tree>(planet, physicsWorld.get(), point, direction));
	}

	void World::DetectSpawnPoint(algebra::Vector2 mousePos)
	{
		RayCb raycb{this};

		float damp = 0.2;

		physicsWorld->RayCast(&raycb, {mousePos.x, mousePos.y}, {mousePos.x + damp * 0, mousePos.y + damp * 1});
		physicsWorld->RayCast(&raycb, {mousePos.x, mousePos.y}, {mousePos.x + damp * 1, mousePos.y + damp * 0});
		physicsWorld->RayCast(&raycb, {mousePos.x, mousePos.y}, {mousePos.x + damp * 0, mousePos.y + damp * -1});
		physicsWorld->RayCast(&raycb, {mousePos.x, mousePos.y}, {mousePos.x + damp * -1, mousePos.y + damp * 0});
		physicsWorld->RayCast(&raycb, {mousePos.x, mousePos.y}, {mousePos.x + damp * 1, mousePos.y + damp * 1});
		physicsWorld->RayCast(&raycb, {mousePos.x, mousePos.y}, {mousePos.x + damp * 1, mousePos.y + damp * -1});
		physicsWorld->RayCast(&raycb, {mousePos.x, mousePos.y}, {mousePos.x + damp * -1, mousePos.y + damp * 1});
		physicsWorld->RayCast(&raycb, {mousePos.x, mousePos.y}, {mousePos.x + damp * -1, mousePos.y + damp * -1});
	}

	void World::Update(float dt)
	{
		physicsWorld->Step(dt, 1, 1);

		won = std::all_of(
			planets.begin(), planets.end(), [](std::shared_ptr<Planet> &planet)
			{ return planet->ConnectedPlanets().size() > 0; });

		lost = std::all_of(
			planets.begin(), planets.end(), [](std::shared_ptr<Planet> &planet)
			{ return planet->Water() <= 0; });

		for (std::shared_ptr<Tree> &tree : trees)
		{
			tree->Grow(dt, *sun);
		}
	}

	World::~World()
	{
		planets.clear();
	}
}
