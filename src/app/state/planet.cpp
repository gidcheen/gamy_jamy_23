#include "planet.hpp"

namespace state
{
	static b2Vec2 ToVec2(algebra::Vector2 const &v)
	{
		return {v.x, v.y};
	}

	Planet::Planet(std::vector<algebra::Vector2> vertices, b2World *world)
		: vertices(std::move(vertices)),
		  world(world)
	{
		b2BodyDef def;
		def.type = b2_staticBody;
		def.userData.pointer = (uintptr_t)this;
		body = world->CreateBody(&def);
		for (size_t i = 0; i < this->vertices.size(); i += 3)
		{
			b2PolygonShape shape;
			b2Vec2 points[3]{ToVec2(this->vertices[i]), ToVec2(this->vertices[i + 1]), ToVec2(this->vertices[i + 2])};
			shape.Set(points, 3);
			body->CreateFixture(&shape, 0);
		}
	}

	Planet::~Planet()
	{
		world->DestroyBody(body);
	}
}
