#include "graphics.hpp"

#include <iostream>
#include <string>
#include <open_gl/gl.hpp>
#include <stdexcept>

namespace graphics
{
	static void DebugCallback(
		GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar *message,
		const void *userParam);

	Graphics::Graphics(Loader loader)
	{
		glLoadProcs(loader);
		std::string gl_renderer = (char *)glGetString(GL_RENDERER); // Returns a hint to the mode
		std::cout << gl_renderer << std::endl;

		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(DebugCallback, nullptr);
	}

	void Graphics::Init(Loader loader)
	{
		static Graphics graphics(loader);
	}

	static void DebugCallback(
		GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar *message,
		const void *userParam)
	{
		// ignore non-significant error/warning codes
		if (id == 131169 || id == 131185 || id == 131218 || id == 131204 || id == 131076)
		{
			return;
		}

		std::string log;
		log += "Debug message(" + std::to_string(id) + "): " + std::string(message) + "\n";

		switch (source)
		{
		case GL_DEBUG_SOURCE_API:
			log += "Source: API";
			break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
			log += "Source: Window System";
			break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
			log += "Source: Shader Compiler";
			break;
		case GL_DEBUG_SOURCE_THIRD_PARTY:
			log += "Source: Third Party";
			break;
		case GL_DEBUG_SOURCE_APPLICATION:
			log += "Source: Application";
			break;
		case GL_DEBUG_SOURCE_OTHER:
			log += "Source: Other";
			break;
		default:
			break;
		}
		log += '\n';

		switch (type)
		{
		case GL_DEBUG_TYPE_ERROR:
			log += "Type: Error";
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			log += "Type: Deprecated Behaviour";
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			log += "Type: Undefined Behaviour";
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			log += "Type: Portability";
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			log += "Type: Performance";
			break;
		case GL_DEBUG_TYPE_MARKER:
			log += "Type: Marker";
			break;
		case GL_DEBUG_TYPE_PUSH_GROUP:
			log += "Type: Push Group";
			break;
		case GL_DEBUG_TYPE_POP_GROUP:
			log += "Type: Pop Group";
			break;
		case GL_DEBUG_TYPE_OTHER:
			log += "Type: Other";
			break;
		default:
			break;
		}
		log += '\n';

		switch (severity)
		{
		case GL_DEBUG_SEVERITY_HIGH:
			log += "Severity: high";
			break;
		case GL_DEBUG_SEVERITY_MEDIUM:
			log += "Severity: medium";
			break;
		case GL_DEBUG_SEVERITY_LOW:
			log += "Severity: low";
			break;
		case GL_DEBUG_SEVERITY_NOTIFICATION:
			log += "Severity: notification";
			break;
		default:
			break;
		}

		if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
		{
			std::cout << log << std::endl;
		}
		else
		{
			std::cerr << log << std::endl;
			throw std::runtime_error(log);
		}
	}
}
