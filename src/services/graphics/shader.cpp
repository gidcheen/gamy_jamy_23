#include "shader.hpp"

#include <array>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <open_gl/gl.hpp>

#include "shader_sources.hpp"

namespace graphics
{
	static void ValidateShader(GLuint shader)
	{
		GLint result;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
		if (result == GL_FALSE)
		{
			GLint lenght;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &lenght);
			std::vector<char> error(lenght);
			glGetShaderInfoLog(shader, lenght, &lenght, error.data());
			std::cerr << "shader error" + std::string(error.data()) << std::endl;
			throw std::runtime_error("shader error" + std::string(error.data()));
		}
	}

	static void ValidateProgram(GLuint program)
	{
		GLint result;
		glGetProgramiv(program, GL_LINK_STATUS, &result);
		if (result == GL_FALSE)
		{
			GLint lenght;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &lenght);
			std::vector<char> error(lenght);
			glGetProgramInfoLog(program, lenght, &lenght, error.data());
			throw std::runtime_error("shader error" + std::string(error.data()));
		}
	}

	Shader::Shader(std::string const &vertSrc, std::string const &fragSrc)
	{
		program = glCreateProgram();

		std::array<char const *, 2>
			vertSrces{baseVertSrc.c_str(), vertSrc.empty() ? defaultVertSrc.c_str() : vertSrc.c_str()};
		GLuint vert = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vert, vertSrces.size(), vertSrces.data(), nullptr);
		glCompileShader(vert);
		ValidateShader(vert);
		glAttachShader(program, vert);

		std::array<char const *, 2> fragSrces{baseFragSrc.c_str(), fragSrc.empty() ? defaultFragSrc.c_str() : fragSrc.c_str()};
		GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(frag, fragSrces.size(), fragSrces.data(), nullptr);
		glCompileShader(frag);
		ValidateShader(frag);
		glAttachShader(program, frag);

		glLinkProgram(program);
		ValidateProgram(program);
		glValidateProgram(program);

		glDeleteShader(vert);
		glDeleteShader(frag);
	}

	Shader::~Shader()
	{
		glDeleteProgram(program);
	}
}
