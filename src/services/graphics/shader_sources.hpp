#include "shader.hpp"

#include <string>

namespace graphics
{

	// baseVertSrc
	std::string baseVertSrc = R"glsl(
#version 450

layout(location = 0) in vec2 position;

layout(location = 0) uniform float depth;
layout(location = 1) uniform mat3 transform;

layout(location = 2) uniform mat3 camera_view;
layout(location = 3) uniform mat3 camera_transform;

layout(location = 4) uniform vec2 image_size;

layout(location = 0) out vec2 uv;

vec2 get_position();

void main()
{
	uv = vec2(get_position().x, 1 - get_position().y);

	// mat3 im = mat3(      image_size.x, 0, 0, 0,       image_size.y, 0, 0, 0, 1);
	// mat3 fr = mat3(1.0 / frame_size.x, 0, 0, 0, 1.0 / frame_size.y, 0, 0, 0, 1);

	vec3 pos = camera_view * camera_transform * transform * vec3(get_position(), 1.0);
	// vec3 pos = fr * im * vec3(get_position(), 1.0);
	pos.z = depth;
    gl_Position = vec4(pos, 1.0);
}
)glsl";

	// defaultVertSrc
	std::string defaultVertSrc = R"glsl(
vec2 get_position()
{
	return position;
}
)glsl";

	// baseFragSrc
	std::string baseFragSrc = R"glsl(
#version 450

layout(location = 6) uniform vec4 color;
layout(location = 5) uniform bool has_sprite;
layout(binding = 0) uniform sampler2D sprite;

layout(location = 0) out vec4 out_color;

layout(location = 0) in vec2 uv;

vec4 get_color();

void main()
{
    out_color = get_color();
}
)glsl";

	// defaultFragSrc
	std::string defaultFragSrc = R"glsl(
vec4 get_color()
{
	if (has_sprite)
	{
		return texture(sprite, uv) * color;
	}
	else
	{
		return color;
	}
}
)glsl";

}
