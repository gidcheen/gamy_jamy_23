#pragma once

#include <functional>
#include <optional>
#include <string>
#include <tuple>
#include <variant>
#include <vector>
#include <cstdint>

namespace json
{
	class Object
	{
	private:
		using Values = std::variant<
			bool,
			int64_t,
			double,
			std::string,
			std::vector<Object>,
			std::vector<std::tuple<std::string, Object>>>;

	private:
		std::optional<Values> value;

	public:
		Object();
		Object(bool v);
		Object(int64_t v);
		Object(double v);
		Object(std::string v);

		Object(Object const &other);
		Object(Object &&other);

		Object &operator=(Object const &other);
		Object &operator=(Object &&other);

		bool IsNull() const;

		std::optional<bool> AsBool() const;
		std::optional<int64_t> AsInt() const;
		std::optional<double> AsDouble() const;
		std::optional<std::string> AsString() const;

		std::optional<size_t> GetArrayCount() const;
		std::optional<Object *> GetArrayValue(size_t index);
		std::optional<Object const *> GetArrayValue(size_t index) const;

		std::optional<size_t> GetObjectCount() const;
		std::optional<std::string const *> GetObjectName(size_t index) const;
		std::optional<Object *> GetObjectValue(size_t index);
		std::optional<Object const *> GetObjectValue(size_t index) const;
		std::optional<Object *> GetObjectValue(std::string const &name);
		std::optional<Object const *> GetObjectValue(std::string const &name) const;

		void SetValue();
		void SetValue(bool v);
		void SetValue(int64_t v);
		void SetValue(double v);
		void SetValue(std::string v);

		void AddArrayValue(Object v);
		void RemoveArrayValue(size_t index);

		void AddObjectValue(std::string const &name, Object v);
		void RemoveObjectValue(std::string const &name);

	private:
		template <typename T>
		std::optional<T *> Get();
		template <typename T>
		std::optional<T const *> Get() const;
	};

	template <typename T>
	std::optional<T *> Object::Get()
	{
		if (value.has_value())
		{
			if (auto *v = std::get_if<T>(&value.value()))
				return make_optional(v);
			else
				return std::nullopt;
		}
		else
			return std::nullopt;
	}

	template <typename T>
	std::optional<T const *> Object::Get() const
	{
		if (value.has_value())
		{
			if (auto *v = std::get_if<T>(&value.value()))
				return std::make_optional(v);
			else
				return std::nullopt;
		}
		else
			return std::nullopt;
	}
}
