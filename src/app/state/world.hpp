#pragma once

#include <vector>
#include <memory>
#include <box2d/box2d.h>
#include <algebra/algebra.hpp>

#include "planet.hpp"
#include "tree.hpp"
#include "sun.hpp"

namespace state
{
	class World final
	{
	private:
		struct Connection
		{
			Planet *planet;
		};

	private:
		int const levelIndex;
		std::unique_ptr<b2World> physicsWorld;

		std::shared_ptr<Sun> sun = std::make_shared<Sun>(9);
		std::vector<std::shared_ptr<Planet>> planets;
		std::vector<std::shared_ptr<Tree>> trees;

		bool won = false;
		bool lost = false;

	public:
		World(int levelIndex);
		World(World &) = delete;
		~World();

		void Update(float dt);

		int LevelIndex() { return levelIndex; }

		std::shared_ptr<Sun> GetSun() { return sun; }
		std::vector<std::shared_ptr<Planet>> const &GetPlanets() { return planets; }
		std::vector<std::shared_ptr<Tree>> const &GetTrees() { return trees; }
		void DetectSpawnPoint(algebra::Vector2 mousePos);
		void SpawnTreeAtPoint(algebra::Vector2 point, algebra::Vector2 direction, std::shared_ptr<Planet> planet);

		bool Won() const { return won; }
		bool Lost() const { return lost; }
	};
}
