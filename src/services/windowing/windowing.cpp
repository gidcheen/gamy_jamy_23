#include "windowing.hpp"

#include <stdexcept>

#define GLFW_INCLUDE_NONE
#include <glfw/glfw3.h>

namespace windowing
{
	Windowing::Windowing()
	{
		if (glfwInit() != GLFW_TRUE)
		{
			throw std::runtime_error("glfw failed to init");
		}
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	}

	Windowing::~Windowing()
	{
		glfwTerminate();
	}

	void Windowing::Init()
	{
		static Windowing windowing;
	}

	void Windowing::Poll()
	{
		glfwPollEvents();
	}

	float Windowing::GetTime()
	{
		return glfwGetTime();
	}

#ifdef _WIN32
#define JAMY_EXPORT __declspec(dllexport)
#else
#define JAMY_EXPORT
#endif

	extern "C"
	{
		JAMY_EXPORT unsigned long NvOptimusEnablement = 1;
		JAMY_EXPORT int AmdPowerXpressRequestHighPerformance = 1;
	}
}
