#pragma once

#include <string>
#include <cstdint>

namespace json
{
	struct Null // 0
	{
	};
	struct Bool // 1
	{
		bool value;
	};
	struct Int // 2
	{
		int64_t value;
	};
	struct Float // 3
	{
		double value;
	};
	struct String // 4
	{
		std::string value;
	};
	struct Comma // 5
	{
	};
	struct Colon // 6
	{
	};
	struct LeftBracket // 7
	{
	};
	struct RightBracket // 8
	{
	};
	struct LeftBrace // 9
	{
	};
	struct RightBrace // 10
	{
	};
}
