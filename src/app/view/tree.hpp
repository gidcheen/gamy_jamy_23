#pragma once

#include <vector>
#include <memory>

#include <state/tree.hpp>

#include <graphics/renderer.hpp>

namespace view
{
	class TreeView
	{
	private:
		std::shared_ptr<state::Tree> tree;

	public:
		TreeView(std::shared_ptr<state::Tree> tree);

		void Render(graphics::Renderer &renderer);

		std::shared_ptr<state::Tree> Tree() const { return tree; }

		bool isRoot = false;
	};
}
