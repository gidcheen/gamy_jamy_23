#include "parsing.hpp"

namespace json
{
	std::optional<std::tuple<Object, size_t>> ParseRec(std::vector<Token> const &tokens, size_t offset);

	std::optional<Object> Parse(std::vector<Token> const &tokens)
	{
		if (auto ret = ParseRec(tokens, 0))
		{
			auto [value, offset] = ret.value();
			if (offset == tokens.size())
				return std::make_optional(value);
			else
				return std::nullopt;
		}
		else
			return std::nullopt;
	}

	std::optional<std::tuple<Object, size_t>> ParseRec(std::vector<Token> const &tokens, size_t offset)
	{
		auto *token = &tokens[offset];
		if (auto n = std::get_if<Null>(token))
			return std::make_optional(std::make_tuple(Object(), offset + 1));
		else if (auto b = std::get_if<Bool>(token))
			return std::make_optional(std::make_tuple(Object(b->value), offset + 1));
		else if (auto i = std::get_if<Int>(token))
			return std::make_optional(std::make_tuple(Object(i->value), offset + 1));
		else if (auto f = std::get_if<Float>(token))
			return std::make_optional(std::make_tuple(Object(f->value), offset + 1));
		else if (auto s = std::get_if<String>(token))
			return std::make_optional(std::make_tuple(Object(s->value), offset + 1));
		else if (auto _ = std::get_if<LeftBracket>(token))
		{
			Object values;
			offset += 1;
			while (offset < tokens.size())
			{
				auto *next_token = &tokens[offset];
				if (auto _ = std::get_if<Comma>(next_token))
					offset += 1;
				else if (auto _ = std::get_if<RightBracket>(next_token))
					return std::make_optional(std::make_tuple(values, offset + 1));
				else if (auto next = ParseRec(tokens, offset))
				{
					auto [value, new_offset] = next.value();
					values.AddArrayValue(value);
					offset = new_offset;
				}
				else
					return std::nullopt;
			}
		}
		else if (auto _ = std::get_if<LeftBrace>(token))
		{
			Object values;
			offset += 1;
			while (offset < tokens.size())
			{
				auto *next_token = &tokens[offset];
				if (std::get_if<Comma>(next_token))
					offset += 1;
				else if (std::get_if<RightBrace>(next_token))
					return std::make_optional(std::make_tuple(values, offset + 1));
				else if (tokens.size() - offset > 3)
				{
					if (auto name = std::get_if<String>(&tokens[offset]))
					{
						if (std::get_if<Colon>(&tokens[offset + 1]))
						{
							if (auto next = ParseRec(tokens, offset + 2))
							{
								auto [value, new_offset] = next.value();
								values.AddObjectValue(name->value, value);
								offset = new_offset;
							}
							else
								return std::nullopt;
						}
						else
							return std::nullopt;
					}
					else
						return std::nullopt;
				}
				else
					return std::nullopt;
			}
		}
		return std::nullopt;
	}
}
