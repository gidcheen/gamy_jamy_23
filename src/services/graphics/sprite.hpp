#pragma once

#include <algebra/algebra.hpp>
#include <string>

namespace graphics
{
	class Sprite final
	{
		friend class Renderer;

	private:
		unsigned int texture = 0;
		algebra::Vector2 extent;

	public:
		Sprite(std::string const &path);
		Sprite(Sprite const &) = delete;
		~Sprite();

		algebra::Vector2 Extent() const { return extent; }
	};
}
