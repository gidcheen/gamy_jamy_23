#include "shared_assets.hpp"

namespace view
{

	SharedAssets &view::SharedAssets::Get()
	{
		static SharedAssets sharedAssets{
			.defaultShader = graphics::Shader(),
			.quadMesh = graphics::Mesh::Quad(),
			.defaultMaterial = graphics::Material{.color = {1, 1, 1, 1}},
			.planetMaterial = graphics::Material{.color = {0.6, 0.6, 0.6, 1}},
			.sunMaterial = graphics::Material{.color = {1, 1, 0, 1}},
			.branchMaterial = graphics::Material{.color = {0.3, 0.7, 0, 1}},
			.treeMaterial = graphics::Material{.color = {0.5, 0.2, 0, 1}},
		};
		return sharedAssets;
	}
}
