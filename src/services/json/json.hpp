#pragma once

#include <optional>
#include <string>

#include "object.hpp"

namespace json
{
	std::optional<Object> JsonToObject(std::string const &json);
	std::string ObjectToJson(Object const &value);
}
