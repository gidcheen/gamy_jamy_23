#pragma once

#include <string>
#include <functional>
#include <vector>
#include <memory>

#include "input.hpp"

namespace windowing
{
	class Window final
	{
	public:
		using LoaderFn = void (*)();
		using Loader = LoaderFn (*)(char const *);

	private:
		class WindowImpl *impl;
		std::unique_ptr<std::vector<InputEvent>> const events = std::make_unique<std::vector<InputEvent>>();

	public:
		Window(std::string const &title, int width, int height);
		~Window();

		void MakeCurrent() const;
		bool ShouldClose() const;
		void Close();

		void HandleEvents(std::function<void(InputEvent const &)> callback) const;
		void SwapBuffers() const;
		Loader GetLoader() const;
	};
}
