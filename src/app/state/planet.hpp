#pragma once

#include <vector>
#include <unordered_set>
#include <numeric>
#include <memory>
#include <algebra/algebra.hpp>
#include <box2d/box2d.h>

namespace state
{
	class Planet final
	{
	public:
		static constexpr int MaxWater = 5;

	private:
		std::vector<algebra::Vector2> const vertices;
		std::unordered_set<Planet *> connectedPlanets;

		float water = MaxWater;

		b2World *world;
		b2Body *body;

	public:
		Planet(std::vector<algebra::Vector2> vertices, b2World *world);
		~Planet();

		std::vector<algebra::Vector2> const &Vertices() { return vertices; }
		std::unordered_set<Planet *> const &ConnectedPlanets() { return connectedPlanets; }

		float Water() { return water; }

		bool TakeWater(float &takeWater)
		{
			std::unordered_set<Planet *> visited;
			return TakeWater(takeWater, visited);
		}

		void AddConnection(Planet *planet) { connectedPlanets.insert(planet); }

	private:
		bool TakeWater(float &takeWater, std::unordered_set<Planet *> &visited)
		{
			if (visited.find(this) == visited.end())
			{
				visited.insert(this);

				if (water > 0)
				{
					takeWater = std::min<float>(water, takeWater);
					water -= takeWater;
					return true;
				}
				else
				{
					for (auto *connected : connectedPlanets)
					{
						if (connected->TakeWater(takeWater, visited))
						{
							return true;
						}
					}
				}
			}

			takeWater = 0;
			return false;
		}
	};
}
