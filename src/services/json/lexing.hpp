#include <string>
#include <cstdint>
#include <variant>
#include <vector>

#include "tokens.hpp"

namespace json
{
	using Token = std::variant<Null, Bool, Int, Float, String, Comma, Colon, LeftBracket, RightBracket, LeftBrace, RightBrace>;

	std::vector<Token> Lex(std::string_view json);
}
