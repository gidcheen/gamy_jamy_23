#pragma once

#include <string>

namespace json
{
	std::string Escape(std::string const &s);
	std::string Unescape(std::string const &s);
}
