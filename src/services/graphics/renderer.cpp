#include "renderer.hpp"

#include <algorithm>
#include <open_gl/gl.hpp>

namespace graphics
{
	static bool Sort(Submit const &a, Submit const &b)
	{
		return a.depth < b.depth;
	}

	void Renderer::Flush()
	{
		std::sort(cameras.begin(), cameras.end(), Sort);
		std::sort(meshes.begin(), meshes.end(), Sort);

		Shader const *shader = nullptr;
		Material const *material = nullptr;

		for (CameraSubmit const &cameraSubmit : cameras)
		{
			algebra::Matrix3x3 inverseCameraTransform = cameraSubmit.transform.Inverse();
			algebra::Matrix3x3 inverseCameraView = algebra::Matrix3x3::Scale({cameraSubmit.camera->Aspect(), 1}).Inverse();

			glViewport(
				cameraSubmit.camera->frameOffset.x, cameraSubmit.camera->frameOffset.y,
				cameraSubmit.camera->frameSize.x, cameraSubmit.camera->frameSize.y);

			if (cameraSubmit.camera->clear)
			{
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			}

			for (MeshSubmit const &meshSubmit : meshes)
			{
				// todo caching
				// shader
				if (shader != meshSubmit.shader)
				{
					shader = meshSubmit.shader;
					glUseProgram(meshSubmit.shader->program);
				}

				// camera
				glProgramUniformMatrix3fv(meshSubmit.shader->program, 2, 1, GL_FALSE, &inverseCameraView.x.x);
				glProgramUniformMatrix3fv(meshSubmit.shader->program, 3, 1, GL_FALSE, &inverseCameraTransform.x.x);

				// material
				if (material != meshSubmit.material)
				{
					material = meshSubmit.material;
					if (meshSubmit.material->isTransparent)
					{
						glEnable(GL_BLEND);
						glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					}
					else
					{
						glDisable(GL_BLEND);
					}
					glProgramUniform4f(meshSubmit.shader->program, 6, meshSubmit.material->color[0], meshSubmit.material->color[1], meshSubmit.material->color[2], meshSubmit.material->color[3]);

					for (auto &[name, value] : meshSubmit.material->ints)
					{
						int location = glGetUniformLocation(meshSubmit.shader->program, name.c_str());
						glProgramUniform1i(meshSubmit.shader->program, location, value);
					}
					for (auto &[name, value] : meshSubmit.material->floats)
					{
						int location = glGetUniformLocation(meshSubmit.shader->program, name.c_str());
						glProgramUniform1f(meshSubmit.shader->program, location, value);
					}
					for (auto &[name, value] : meshSubmit.material->vec2s)
					{
						int location = glGetUniformLocation(meshSubmit.shader->program, name.c_str());
						glProgramUniform2f(meshSubmit.shader->program, location, value[0], value[1]);
					}
					for (auto &[name, value] : meshSubmit.material->vec3s)
					{
						int location = glGetUniformLocation(meshSubmit.shader->program, name.c_str());
						glProgramUniform3f(meshSubmit.shader->program, location, value[0], value[1], value[2]);
					}
					for (auto &[name, value] : meshSubmit.material->vec4s)
					{
						int location = glGetUniformLocation(meshSubmit.shader->program, name.c_str());
						glProgramUniform4f(meshSubmit.shader->program, location, value[0], value[1], value[2], value[3]);
					}
				}

				// mesh
				glBindVertexArray(meshSubmit.mesh->vertexArray);

				// sprite
				if (meshSubmit.sprite)
				{
					glProgramUniform2f(meshSubmit.shader->program, 4, meshSubmit.sprite->extent.x, meshSubmit.sprite->extent.y);
					glBindTextureUnit(0, meshSubmit.sprite->texture);
					glProgramUniform1i(meshSubmit.shader->program, 5, 1);
				}
				else
				{
					glProgramUniform1i(meshSubmit.shader->program, 5, 0);
				}

				// renderable
				glProgramUniform1f(meshSubmit.shader->program, 0, meshSubmit.depth);
				glProgramUniformMatrix3fv(meshSubmit.shader->program, 1, 1, GL_FALSE, &meshSubmit.transform.x.x);

				glDrawArrays(GL_TRIANGLES, 0, meshSubmit.mesh->VertexCount());
			}
		}

		cameras.clear();
		meshes.clear();
	}
}
