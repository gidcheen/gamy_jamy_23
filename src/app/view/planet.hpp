#pragma once

#include <vector>
#include <memory>
#include <algebra/algebra.hpp>
#include <graphics/mesh.hpp>
#include <graphics/renderer.hpp>
#include <state/planet.hpp>

namespace view
{
	class PlanetView final
	{
	private:
		std::shared_ptr<state::Planet> planet;
		graphics::Mesh mesh;
		graphics::Material material;

	public:
		PlanetView(std::shared_ptr<state::Planet>);
		PlanetView(PlanetView &) = delete;

		void Render(graphics::Renderer &renderer);
	};
}
