#include "sprite.hpp"

#include <stb/stb_image.h>
#include <open_gl/gl.hpp>

namespace graphics
{
	Sprite::Sprite(std::string const &path)
	{
		int w, h, c;
		unsigned char *pixels = stbi_load(path.c_str(), &w, &h, &c, 4);

		extent = algebra::Vector2(w, h);

		glCreateTextures(GL_TEXTURE_2D, 1, &texture);
		glTextureStorage2D(texture, 1, GL_RGBA8, w, h);
		glTextureSubImage2D(texture, 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		glTextureParameteri(texture, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glGenerateTextureMipmap(texture);

		stbi_image_free(pixels);
	}

	Sprite::~Sprite()
	{
		glDeleteTextures(1, &texture);
	}
}
