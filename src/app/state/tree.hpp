#pragma once

#include <vector>
#include <memory>
#include <box2d/box2d.h>

#include "planet.hpp"
#include "sun.hpp"

namespace state
{
	struct Transform final
	{
		algebra::Vector2 position;
		float rotation;
		algebra::Matrix3x3 transform;

		void Recalculate()
		{
			transform = algebra::Matrix3x3::TRS(position, rotation, {1, 1});
		}
	};

	class Leaf final
	{
	};

	struct Branch final
	{
		bool growing = true;
		float size = 0;
		Transform transform;
	};

	class Tree final
	{
	private:
		b2World *world;

		std::shared_ptr<Planet> planet;
		std::shared_ptr<Branch> root;
		std::vector<std::shared_ptr<Branch>> growing_branches;
		std::vector<std::shared_ptr<Branch>> full_grown_branches;

		Transform transform;

	public:
		Tree(std::shared_ptr<Planet> planet, b2World *world, algebra::Vector2 spawnPos, algebra::Vector2 direction);

		void Grow(float dt, Sun const &sun);

		std::shared_ptr<Planet> const &GetPlanet() { return planet; };
		std::shared_ptr<Branch> const &GetRoot() { return root; };
		std::vector<std::shared_ptr<Branch>> const &GrowingBranches() { return growing_branches; };
		std::vector<std::shared_ptr<Branch>> const &FullGrownBranches() { return full_grown_branches; };
		bool is_root = false;
	};
}
