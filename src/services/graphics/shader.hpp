#pragma once

#include <string>

namespace graphics
{
	class Renderer;

	class Shader final
	{
		friend class Renderer;

	private:
		unsigned int program = 0;

	public:
		Shader(std::string const &vert_src = "", std::string const &frag_src = "");
		Shader(Shader const &) = delete;
		~Shader();
	};
}
