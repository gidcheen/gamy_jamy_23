#pragma once

#include "vector.hpp"

namespace algebra
{
	// column-major
	struct Matrix3x3 final
	{
		Vector3 x, y, z;

		Matrix3x3(Vector3 x = {}, Vector3 y = {}, Vector3 z = {}) : x(x), y(y), z(z) {}

		Vector3 &operator[](size_t index)
		{
			return (&x)[index];
		}

		Vector3 const &operator[](size_t index) const
		{
			return (&x)[index];
		}

		Matrix3x3 operator*(Matrix3x3 const &other) const
		{
			return Mult(*this, other);
		}

		Vector3 operator*(Vector3 const &other) const
		{
			return VectorMult(*this, other);
		}

		float Determinant() const
		{
			return x.x * (y.y * z.z - z.y * y.z) - y.x * (x.y * z.z - z.y * x.z) + z.x * (x.y * y.z - y.y * x.z);
		}

		Matrix3x3 Transpose() const
		{
			return {{x.x, y.x, z.x}, {x.y, y.y, z.y}, {x.z, y.z, z.z}};
		}

		Matrix3x3 Cofactor() const
		{
			return {
				{
					(y.y * z.z - z.y * y.z),
					(y.x * z.z - z.x * y.z) * -1,
					(y.x * z.y - z.x * y.y),
				},
				{
					(x.y * z.z - z.y * x.z) * -1,
					(x.x * z.z - z.x * x.z),
					(x.x * z.y - z.x * x.y) * -1,
				},
				{
					(x.y * y.z - y.y * x.z),
					(x.x * y.z - y.x * x.z) * -1,
					(x.x * y.y - y.x * x.y),
				},
			};
		}

		Matrix3x3 Adjoint() const
		{
			return Cofactor().Transpose();
		}

		void ScalarMult(float scalar)
		{
			x.x *= scalar;
			x.y *= scalar;
			x.z *= scalar;

			y.x *= scalar;
			y.y *= scalar;
			y.z *= scalar;

			z.x *= scalar;
			z.y *= scalar;
			z.z *= scalar;
		}

		void Mult(Matrix3x3 const &other)
		{
			*this = Mult(*this, other);
		}

		void VectorMult(Vector3 const &other)
		{
			*this = VectorMult(*this, other);
		}

		Matrix3x3 Inverse() const
		{
			float det = Determinant();
			Matrix3x3 adj = Adjoint();

			adj.ScalarMult(1 / det);
			return adj;
		}

		static Matrix3x3 ScalarMult(Matrix3x3 a, float scalar)
		{
			a.ScalarMult(scalar);
			return a;
		}

		static Matrix3x3 Mult(Matrix3x3 const &a, Matrix3x3 const &b)
		{
			Matrix3x3 ret;

			for (size_t row = 0; row < 3; row++)
			{
				for (size_t col = 0; col < 3; col++)
				{
					for (size_t el = 0; el < 3; el++)
					{
						ret[col][row] += a[el][row] * b[col][el];
					}
				}
			}

			return ret;
		}

		static Vector3 VectorMult(Matrix3x3 const &a, Vector3 const &b)
		{
			return {
				a.x.x * b.x + a.y.x * b.y + a.z.x * b.z,
				a.x.y * b.x + a.y.y * b.y + a.z.y * b.z,
				a.x.z * b.x + a.y.z * b.y + a.z.z * b.z
			};
		}

		static Matrix3x3 Identity()
		{
			return {
				{1, 0, 0},
				{0, 1, 0},
				{0, 0, 1},
			};
		}

		static Matrix3x3 Translation(Vector2 translation)
		{
			return {
				{1, 0, 0},
				{0, 1, 0},
				{translation.x, translation.y, 1},
			};
		}

		static Matrix3x3 Rotation(float rotation)
		{
			return {
				{+cos(rotation), sin(rotation), 0},
				{-sin(rotation), cos(rotation), 0},
				{0, 0, 1},
			};
		}

		static Matrix3x3 Scale(Vector2 scale)
		{
			return {
				{scale.x, 0, 0},
				{0, scale.y, 0},
				{0, 0, 1},
			};
		}

		static Matrix3x3 TRS(Vector2 translation, float rotation, Vector2 scale)
		{
			return Translation(translation) * Rotation(rotation) * Scale(scale);
		}
	};
}
