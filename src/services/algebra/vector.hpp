#pragma once

#include <cmath>

namespace algebra
{
	struct Vector2 final
	{
		float x, y;

		Vector2(float x = 0, float y = 0) : x(x), y(y) {}

		float &operator[](size_t index)
		{
			return (&x)[index];
		}

		float const &operator[](size_t index) const
		{
			return (&x)[index];
		}

		float length() const
		{
			return std::sqrt(x * x + y * y);
		}
	};

	struct Vector3 final
	{
		float x, y, z;

		Vector3(float x = 0, float y = 0, float z = 0) : x(x), y(y), z(z) {}

		float &operator[](size_t index)
		{
			return (&x)[index];
		}

		float const &operator[](size_t index) const
		{
			return (&x)[index];
		}

		float length() const
		{
			return std::sqrt(x * x + y * y + z * z);
		}
	};
}
