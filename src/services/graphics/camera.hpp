#pragma once

#include <algebra/algebra.hpp>

namespace graphics
{
	class Camera final
	{
	public:
		bool clear = true;
		algebra::Vector2 frameOffset = {};
		algebra::Vector2 frameSize = {400, 400};

		float Aspect() const { return frameSize.x / frameSize.y; }
	};
}
