#define _USE_MATH_DEFINES
#include <cmath>

#include "tree.hpp"

#include <functional>
#include <cmath>

namespace state
{
	constexpr float PI = 3.1415926535;

	struct SunRayCb : public b2RayCastCallback
	{
		bool planetWasHit = false;
		Planet *planetHit = nullptr;

		float ReportFixture(
			b2Fixture *fixture,
			const b2Vec2 &point,
			const b2Vec2 &normal, float fraction) override
		{
			planetWasHit = true;
			planetHit = (Planet *)fixture->GetBody()->GetUserData().pointer;
			return 0;
		}
	};

	static bool DoneGrowing(std::shared_ptr<Branch> branch)
	{
		return !branch->growing;
	}

	Tree::Tree(std::shared_ptr<Planet> planet, b2World *world, algebra::Vector2 spawnPos, algebra::Vector2 direction)
		: world(world),
		  planet(planet),
		  root(std::make_shared<Branch>())
	{
		transform.position = spawnPos;
		transform.rotation = std::atan2(direction.y, direction.x) - (PI / 2);
		transform.Recalculate();

		root->transform = transform;
		growing_branches.emplace_back(root);
	}

	void Tree::Grow(float dt, Sun const &sun)
	{
		if (is_root)
			return;
			
		float waterDelta = std::min(0.6f * dt, 0.1f);
		planet->TakeWater(waterDelta);

		if (waterDelta > 0)
		{
			for (auto branch : growing_branches)
			{
				// NOTE: forking is only dependent on the branch size
				if (branch->size <= 1)
				{
					algebra::Vector3 fromPos = branch->transform.transform * algebra::Vector3{0, branch->size, 1};
					algebra::Vector3 toPos = branch->transform.transform * algebra::Vector3{0, branch->size + waterDelta, 1};
					SunRayCb cb;
					world->RayCast(&cb, {fromPos.x, fromPos.y}, {toPos.x, toPos.y});
					if (!cb.planetWasHit)
					{
						branch->size += waterDelta;
					}
					else
					{
						is_root = true;
						branch->growing = false;
						cb.planetHit->AddConnection(planet.get());
						planet->AddConnection(cb.planetHit);
						full_grown_branches.emplace_back(branch);
					}
				}
				else
				{
					// stop growing
					branch->growing = false;
					full_grown_branches.emplace_back(branch);

					// fork
					auto new_branch = std::make_shared<Branch>();
					// calculate the position of the new branch
					algebra::Vector3 offset{0, 1, 1};
					algebra::Vector3 world_pos = branch->transform.transform * offset;
					new_branch->transform.position = {world_pos.x, world_pos.y};
					new_branch->transform.rotation = branch->transform.rotation;

					new_branch->transform.Recalculate();

					algebra::Vector2 sunPos = sun.GetPosition();
					algebra::Vector2 sunDir = {
						new_branch->transform.position.x - sunPos.x,
						new_branch->transform.position.y - sunPos.y};

					// SunRayCb cb;
					// world->RayCast(&cb, {world_pos.x, world_pos.y}, {sunPos.x, sunPos.y});
					// if (!cb.planetWasHit)
					{
						float sunAngle = std::atan2(sunDir.y, sunDir.x) + M_PI * 0.5f;
						new_branch->transform.rotation = sunAngle;
						new_branch->transform.Recalculate();
					}

					growing_branches.emplace_back(new_branch);
				}
			}

			growing_branches.erase(
				std::remove_if(growing_branches.begin(), growing_branches.end(), DoneGrowing),
				growing_branches.end());
		}
		else
		{
			is_root = true;
			for (auto &b : growing_branches)
			{
				full_grown_branches.push_back(b);
			}
		}
	}
}
