#pragma once

#include <graphics/shader.hpp>
#include <graphics/mesh.hpp>
#include <graphics/material.hpp>

namespace view
{
	struct SharedAssets final
	{
		graphics::Shader const defaultShader;
		graphics::Mesh const quadMesh;
		graphics::Material const defaultMaterial;
		graphics::Material const planetMaterial;
		graphics::Material const sunMaterial;
		graphics::Material const branchMaterial;
		graphics::Material const treeMaterial;

		static SharedAssets &Get();
	};
}
