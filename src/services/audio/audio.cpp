#include "audio.hpp"

#include <iostream>

#include <miniaudio/miniaudio.h>

namespace audio
{
	static std::vector<std::filesystem::path> soundAssets{
		// "sounds/atmo_bright.wav",
		// "sounds/atmo_brightest.wav",
		"sounds/atmo_dark.wav",
		// "sounds/atmo_darkest.wav",
		"sounds/atmo_neutral.wav",
		"sounds/atmo_overtones.wav",
		"sounds/kick_one.wav",
		// "sounds/kick_two.wav",
		// "sounds/kick_three.wav",
		"sounds/success_one.wav",
		// "sounds/success_two.wav",
		// "sounds/success_three.wav",
		"sounds/roots_growing.wav",
		// "sounds/solar_wind.wav"
		};

	static std::map<std::string, ma_sound *> sounds{};

	Audio::Audio()
	{
		ma_result result;

		engine = new ma_engine;

		result = ma_engine_init(NULL, engine);
		if (result != MA_SUCCESS)
		{
			std::cerr << "miniaudio engine init: " << result << std::endl;
			throw std::exception();
		}

		// initialize all the sounds
		for (auto soundAssetPath : soundAssets)
		{
			sounds[soundAssetPath.stem().string()] = InitSound(soundAssetPath.string());
		}
	}

	Audio::~Audio()
	{
		// deinitialize all the sounds
		for (auto [name, sound] : sounds)
		{
			ma_sound_uninit(sound);
			delete sound;
		}

		ma_engine_uninit(engine);
		delete engine;
	}

	void Audio::Oneshot(const std::string &path)
	{
		ma_engine_play_sound(engine, path.c_str(), NULL);
	}

	void Audio::Play(const std::string &name)
	{
		PlayOrLoop(name, false);
	}

	void Audio::Loop(const std::string &name)
	{
		PlayOrLoop(name, true);
	}

	void Audio::SetVolume(const std::string &name, float volume)
	{
		if (sounds.find(name) != sounds.end())
		{
			ma_sound *sound = sounds[name];
			ma_sound_set_volume(sound, volume);
		}
	}

	ma_sound *Audio::InitSound(const std::string &path)
	{
		ma_sound *sound = new ma_sound;
		ma_result result;
		result = ma_sound_init_from_file(
			engine,
			path.c_str(),
			MA_SOUND_FLAG_DECODE,
			nullptr, // group
			NULL,
			sound);
		if (result != MA_SUCCESS)
		{
			std::cerr << "miniaudio sound init (" << path << "): " << result << std::endl;
			throw std::exception();
		}

		return sound;
	}

	void Audio::PlayOrLoop(const std::string &name, bool loop)
	{
		if (sounds.find(name) != sounds.end())
		{
			ma_sound *sound = sounds[name];

			if (!ma_sound_is_playing(sound))
			{
				ma_sound_seek_to_pcm_frame(sound, 0);
				ma_sound_set_looping(sound, loop ? MA_TRUE : MA_FALSE);
				ma_sound_start(sound);
			}
		}
		else
		{
			std::cerr << "miniaudio sound not found: " << name << std::endl;
			throw std::exception();
		}
	}
}
