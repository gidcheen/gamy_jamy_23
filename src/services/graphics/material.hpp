#pragma once

#include <map>
#include <string>
#include <array>

namespace graphics
{
	struct Material final
	{
		bool isTransparent = false;
		std::array<float, 4> color = {1, 1, 1, 1};
		std::map<std::string, int> ints;
		std::map<std::string, float> floats;
		std::map<std::string, std::array<float, 2>> vec2s;
		std::map<std::string, std::array<float, 3>> vec3s;
		std::map<std::string, std::array<float, 4>> vec4s;
	};
}
