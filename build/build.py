import os
import glob
import sys
import shutil
from datetime import datetime

start_time = datetime.now()

is_release = len(sys.argv) > 1 and sys.argv[1] == "release"

libs_dir = "libs/" + sys.platform + "/"
bin_dir = "bin/" + ("release/" if is_release else "debug/")
temp_dir = bin_dir + "temp/"

config = "-O2" if is_release else "-g -O0"
includes = ["-Isrc/services", "-Isrc/app"]

if sys.platform == "linux":
    compiler = "clang++"
    libs = ["-lglfw3", "-lm", "-lX11", "-lXcursor", "-ldl"]
    config += " -pthread -stdlib=libc++"
    bin_extension = ""
elif sys.platform == "win32":
    compiler = "clang++"
    libs = ["-lglfw3", "-lkernel32",
            "-luser32", "-lgdi32", "-lshell32"]
    bin_extension = ".exe"

bin_path = bin_dir + "/gamy_jamy_23" + bin_extension

c_files = [cpp_file for cpp_file in glob.glob("src/**/*.cpp", recursive=True)]
o_files = [temp_dir + c_file + ".o" for c_file in c_files]

include_cache = {}


def findheaders(file):
    file = os.path.normpath(file)
    if file in include_cache:
        return include_cache[file]

    headers = include_cache.setdefault(file, set())
    lines = []
    if os.path.exists(file):
        with open(file, "r") as opened_file:
            lines = opened_file.readlines()
    for line in lines:
        if line.startswith("#include "):
            def findnext(header):
                header = os.path.normpath(header)
                if os.path.exists(header):
                    headers.add(header)
                    for h in findheaders(header):
                        headers.add(h)
            path = line[10:-2]
            if line[9] == "<":
                header = "src/services/" + path
                findnext(header)
                header = "src/app/" + path
                findnext(header)
            else:
                reldir = os.path.dirname(file)
                header = reldir + "/" + path
                findnext(header)

    return headers


print("building:")
for (c_file, o_file) in zip(c_files, o_files):
    if os.path.exists(o_file):
        needs_compile = False
        c_time = os.path.getmtime(c_file)
        o_time = os.path.getmtime(o_file)
        needs_compile |= c_time > o_time
        for header in findheaders(c_file):
            h_time = os.path.getmtime(header)
            needs_compile |= h_time > o_time

        if not needs_compile:
            continue

    print("compiling: " + c_file + " -> " + o_file)
    os.makedirs(os.path.dirname(o_file), exist_ok=True)
    command = compiler + " {c_file} -c -o {o_file} -std=c++17 {config} {includes} -Wall".format(
        c_file=c_file,
        o_file=o_file,
        config=config,
        includes=" ".join(includes))

    os.system(command)

print("linking: " + bin_path)
os.makedirs(bin_dir, exist_ok=True)
command = compiler + " {o_file} -o {bin_path} -std=c++17  {config} {libs} -L{libs_dir} -Wall".format(
    o_file=" ".join(o_files),
    config=config,
    libs=" ".join(libs),
    libs_dir=libs_dir,
    bin_path=bin_path)

os.system(command)

end_time = datetime.now()
build_time = end_time - start_time
print("build time: " + str(build_time))

print("build done")
