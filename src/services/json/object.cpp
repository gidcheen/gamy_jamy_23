#include "object.hpp"

namespace json
{
	Object::Object() : value(std::nullopt)
	{
	}
	Object::Object(bool v) : value(std::make_optional<Values>(v))
	{
	}
	Object::Object(int64_t v) : value(std::make_optional<Values>(v))
	{
	}
	Object::Object(double v) : value(std::make_optional<Values>(v))
	{
	}
	Object::Object(std::string v) : value(std::make_optional<Values>(v))
	{
	}

	Object::Object(Object const &other) : value(other.value)
	{
	}
	Object::Object(Object &&other) : value(std::move(other.value))
	{
	}

	Object &Object::operator=(Object const &other)
	{
		value = other.value;
		return *this;
	}

	Object &Object::operator=(Object &&other)
	{
		value = std::move(other.value);
		return *this;
	}

	bool Object::IsNull() const
	{
		return !value.has_value();
	}

	std::optional<bool> Object::AsBool() const
	{
		if (auto b = Get<bool>())
			return std::make_optional((bool)*b.value());
		else if (auto i = Get<int64_t>())
			return std::make_optional((bool)*i.value());
		else if (auto d = Get<double>())
			return std::make_optional((bool)*d.value());
		else
			return std::nullopt;
	}

	std::optional<int64_t> Object::AsInt() const
	{
		if (auto b = Get<bool>())
			return std::make_optional((int64_t)*b.value());
		else if (auto i = Get<int64_t>())
			return std::make_optional((int64_t)*i.value());
		else if (auto d = Get<double>())
			return std::make_optional((int64_t)*d.value());
		else
			return std::nullopt;
	}

	std::optional<double> Object::AsDouble() const
	{
		if (auto b = Get<bool>())
			return std::make_optional((double)*b.value());
		else if (auto i = Get<int64_t>())
			return std::make_optional((double)*i.value());
		else if (auto d = Get<double>())
			return std::make_optional((double)*d.value());
		else
			return std::nullopt;
	}

	std::optional<std::string> Object::AsString() const
	{
		if (auto s = Get<std::string>())
			return std::make_optional(*s.value());
		else
			return std::nullopt;
	}

	std::optional<size_t> Object::GetArrayCount() const
	{
		if (auto a = Get<std::vector<Object>>())
			return std::make_optional(a.value()->size());
		else
			return std::nullopt;
	}

	std::optional<Object *> Object::GetArrayValue(size_t index)
	{
		if (auto a = Get<std::vector<Object>>())
		{
			if (index < a.value()->size())
				return std::make_optional(&(*a.value())[index]);
			else
				return std::nullopt;
		}
		else
			return std::nullopt;
	}

	std::optional<Object const *> Object::GetArrayValue(size_t index) const
	{
		if (auto a = Get<std::vector<Object>>())
		{
			if (index < a.value()->size())
				return std::make_optional(&(*a.value())[index]);
			else
				return std::nullopt;
		}
		else
			return std::nullopt;
	}

	std::optional<size_t> Object::GetObjectCount() const
	{
		if (auto o = Get<std::vector<std::tuple<std::string, Object>>>())
			return std::make_optional(o.value()->size());
		else
			return std::nullopt;
	}

	std::optional<std::string const *> Object::GetObjectName(size_t index) const
	{
		if (auto o = Get<std::vector<std::tuple<std::string, Object>>>())
		{
			if (index < o.value()->size())
				return std::make_optional(&std::get<0>((*o.value())[index]));
			else
				return std::nullopt;
		}
		else
			return std::nullopt;
	}

	std::optional<Object *> Object::GetObjectValue(size_t index)
	{
		if (auto o = Get<std::vector<std::tuple<std::string, Object>>>())
		{
			if (index < o.value()->size())
				return std::make_optional(&std::get<1>((*o.value())[index]));
			else
				return std::nullopt;
		}
		else
			return std::nullopt;
	}

	std::optional<Object const *> Object::GetObjectValue(size_t index) const
	{
		if (auto o = Get<std::vector<std::tuple<std::string, Object>>>())
		{
			if (index < o.value()->size())
				return std::make_optional(&std::get<1>((*o.value())[index]));
			else
				return std::nullopt;
		}
		else
			return std::nullopt;
	}

	std::optional<Object *> Object::GetObjectValue(std::string const &name)
	{
		if (auto a = Get<std::vector<std::tuple<std::string, Object>>>())
		{
			for (auto &[n, v] : *a.value())
			{
				if (n == name)
					return std::make_optional(&v);
			}
			return std::nullopt;
		}
		else
			return std::nullopt;
	}

	std::optional<Object const *> Object::GetObjectValue(std::string const &name) const
	{
		if (auto o = Get<std::vector<std::tuple<std::string, Object>>>())
		{
			for (auto &[n, v] : *o.value())
			{
				if (n == name)
					return std::make_optional(&v);
			}
			return std::nullopt;
		}
		else
			return std::nullopt;
	}

	void Object::SetValue()
	{
		value = std::nullopt;
	}

	void Object::SetValue(bool v)
	{
		value = std::make_optional<Values>(v);
	}

	void Object::SetValue(int64_t v)
	{
		value = std::make_optional<Values>(v);
	}

	void Object::SetValue(double v)
	{
		value = std::make_optional<Values>(v);
	}

	void Object::SetValue(std::string v)
	{
		value = std::make_optional<Values>(v);
	}

	void Object::AddArrayValue(Object value)
	{
		if (auto a = Get<std::vector<Object>>())
			a.value()->emplace_back(std::move(value));
		else
			this->value = std::make_optional<Values>(std::vector<Object>{value});
	}

	void Object::RemoveArrayValue(size_t index)
	{
		if (auto a = Get<std::vector<Object>>())
		{
			if (index < a.value()->size())
				a.value()->erase(a.value()->begin() + index);
		}
	}

	void Object::AddObjectValue(std::string const &name, Object value)
	{
		if (auto o = Get<std::vector<std::tuple<std::string, Object>>>())
		{
			for (auto &[n, v] : *o.value())
			{
				if (n == name)
					v = std::move(value);
			}
			o.value()->emplace_back(name, value);
		}
		else
		{
			this->value = std::make_optional<Values>(std::vector<std::tuple<std::string, Object>>{
				{name, value},
			});
		}
	}

	void Object::RemoveObjectValue(std::string const &name)
	{
		if (auto o = Get<std::vector<std::tuple<std::string, Object>>>())
		{
			size_t i = 0;
			for (auto &[n, v] : *o.value())
			{
				if (n == name)
				{
					o.value()->erase(o.value()->begin() + i);
					break;
				}
				i += 1;
			}
		}
	}
}
