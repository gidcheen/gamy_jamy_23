#pragma once

#include <string>
#include <vector>

namespace windowing
{
	enum class InputAction
	{
		Down,
		Up,
		Repeat,
	};

	enum class InputModifier
	{
		Shift = 1 << 0,
		Control = 1 << 1,
		Alt = 1 << 2,
		Super = 1 << 3,
		CapsLock = 1 << 4,
		NumLock = 1 << 5,
	};

	enum class MouseButton
	{
		Left,
		Right,
		Middle,

		Button4,
		Button5,
		Button6,
		Button7,
		Button8,
	};

	enum class KeyboardKey
	{
		Key1,
		Key2,
		Key3,
		Key4,
		Key5,
		Key6,
		Key7,
		Key8,
		Key9,
		Key0,

		KeyA,
		KeyB,
		KeyC,
		KeyD,
		KeyE,
		KeyF,
		KeyG,
		KeyH,
		KeyI,
		KeyJ,
		KeyK,
		KeyL,
		KeyM,
		KeyN,
		KeyO,
		KeyP,
		KeyQ,
		KeyR,
		KeyS,
		KeyT,
		KeyU,
		KeyV,
		KeyW,
		KeyX,
		KeyY,
		KeyZ,

		Space,
		GraveAccent,
		Minus,
		Equal,
		LeftBracket,
		RightBracket,
		Semicolon,
		Apostrophe,
		BackSlash,
		Comma,
		Period,
		Slash,

		LeftShift,
		LeftControl,
		LeftAlt,
		LeftSuper,

		RightShift,
		RightControl,
		RightAlt,
		RightSuper,

		Print,
		ScrollLock,
		Pause,

		Tab,
		CapsLock,
		BackSpace,
		Enter,

		Escape,

		F1,
		F2,
		F3,
		F4,
		F5,
		F6,
		F7,
		F8,
		F9,
		F10,
		F11,
		F12,

		Insert,
		Delete,
		Home,
		End,
		PageUp,
		PageDown,

		Left,
		Right,
		Up,
		Down,

		NumLock,
		Num0,
		Num1,
		Num2,
		Num3,
		Num4,
		Num5,
		Num6,
		Num7,
		Num8,
		Num9,
		NumDivide,
		NumMultiply,
		NumSubtract,
		NumAdd,
		NumDecimal,
		NumEnter,
	};

	enum class InputEventType
	{
		Windowpos,
		Windowsize,
		Windowfocus,
		Windowrefresh,
		Windowclose,
		Windowcontentscale,
		Windowiconify,
		Windowmaximize,
		Cursorpos,
		Cursorenter,
		Mousebutton,
		Scroll,
		Key,
		Text,
		Drop,
	};

	struct InputEvent
	{
		InputEventType type;

		struct Windowpos
		{
			int xpos;
			int ypos;
		} windowpos;
		struct Windowsize
		{
			int width;
			int height;
		} windowsize;
		struct Windowfocus
		{
			bool focused;
		} windowfocus;
		struct Windowrefresh
		{
		} windowrefresh;
		struct Windowclose
		{
		} windowclose;
		struct Windowcontentscale
		{
			float xscale;
			float yscale;
		} windowcontentscale;
		struct Windowiconify
		{
			bool iconified;
		} windowiconify;
		struct Windowmaximize
		{
			bool maximized;
		} windowmaximize;
		struct Cursorpos
		{
			double xpos;
			double ypos;
		} cursorpos;
		struct Cursorenter
		{
			bool entered;
		} cursorenter;
		struct Mousebutton
		{
			MouseButton button;
			InputAction action;
			InputModifier mods;
		} mousebutton;
		struct Scroll
		{
			double xoffset;
			double yoffset;
		} scroll;
		struct Key
		{
			KeyboardKey key;
			InputAction action;
			InputModifier mods;
		} key;
		struct Text
		{
			std::string text;
		} text;
		struct Drop
		{
			std::vector<std::string> paths;
		} drop;
	};
}
